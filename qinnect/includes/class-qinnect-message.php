<?php
/**
 * Message class file.
 *
 * @package Qinnect/Message
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Message class.
 */
class Qinnect_Message {

	/**
	 * The single instance of Qinnect_Message.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin message.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available message for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $message = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		// $this->base = 'wpt_';
	}

	function load_messages( $server_slug, $channel_slug ) {
		global $wpdb;
		
		// load
		// $sql = 'SELECT *
		// FROM '.$wpdb->prefix.'booking_calendars AS booking_calendars
		// INNER JOIN '. $wpdb->prefix.'booking_reservation AS booking_reservations
		// ON booking_calendars.id =  booking_reservations.calendar_id
		// WHERE status LIKE "pending"';
		// $wpdb->get_results( $sql );

	}

	function save_file_to_temp_dir() {

	}

	function check_if_s3_file_exists($path_to_file) {
		
		// 
		// Remote file url
		$remoteFile = AWS_S3_URL.'/' . $path_to_file;

		// Initialize cURL
		$ch = curl_init($remoteFile);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_exec($ch);
		$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// Check the response code
		if ($responseCode == 200) {
			return true;
		} else {
			return false;
		}

	}

	function create($data) {
		global $wpdb;
		extract($data);

		$user_id = get_current_user_id();
		if (!$user_id) {
			return $this->parent->response->error(ERR_USER_NOT_LOGGED_IN,'ERR_USER_NOT_LOGGED_IN');
		}

		$file_res = '';
		$total_files = 0;
		$upload_res = '';
		if ($has_attachment) {

			// we need to get the file first from base64 and place it somewhere temporarily

			// after temporarily saving it, we need to fetch it and hash it

			// then we need to check if it exists


			// $path_to_file = __DIR__ . '/' . $filename;

			// $filename;
			$hash_filename = md5_file($file);
			$hash_filename = base64_encode($hash_filename);
			
			if ( !check_if_s3_file_exists('chanels/test/'.$hash_filename) ) {
				$path_to_hash_file = __DIR__ . '/'. $hash_filename;
				file_put_contents($path_to_hash_file, file_get_contents($file) );
				// upload to storage
				$upload_res = $this->parent->file_uploader->upload($path_to_hash_file, 'channels', 'test');
				// remove from temp directory once uploaded
				unlink($path_to_hash_file);
			}



			// // we need to get the file
			// $total = count($_FILES['file']['name']);
			// $total_files = $total;
			// for( $i=0 ; $i < $total ; $i++ ) {

			// 	// get local file for upload testing
			// 	$filename = basename($_FILES['file']['name'][$i]);
			// 	$filepath =  $_FILES['file']['tmp_name'][$i];

			// 	$file_res = file_put_contents( __DIR__ , $filepath);

			// 	// $finfo = new finfo();
			// 	// $fileinfo_type = $finfo->file($filepath, FILEINFO_MIME);

			// 	// hash
			// 	// we need to save the name
	
			// 	// we need to turn the name into hash
	
			// 	// we need to save hash
	
			// 	// we need to check if file exists on S3, 
			// 		// if it does, it means it's the sam
			// }
		}

		$data['upload_res'] = $upload_res;

		// public key = $id;
		// private key = generate
		// user_id = $user or get_current_user_id
		// message_category_id ( the channel ID) = 
		// message_category_type_id = 1;
		// message_type_id ( 1 = text, 2 = photo )
		// title = ""
		// message = $text
		// reference = ""
		// user_id
		// parent_message_id = $parent
		$message_category = $this->parent->helper->get_id_by_key($channel_key, 'channel');
		$today = date('Y-m-d H:i:s');
		
		$wpdb->query(
			$wpdb->prepare(
				"
				INSERT INTO " . $wpdb->prefix . TBL_MESSAGES . "
				( 
					public_key,
					private_key,
					message,
					reference,
					user_id,
					message_category_id,
					message_category_type_id,
					message_type_id,
					parent_message_id,
					created_at,
					updated_at
				)
				VALUES ( %s, %s, %s, %s, %d, %d, %d, %d, %d, %s, %s )
				",
				$key,
				$this->parent->helper->generate_key(),
				$message,
				$reference,
				$user_id,
				$message_category,
				$message_category_type,
				$message_type,
				$parent,
				$today,
				$today
			)
		);

		return $this->parent->response->success($data, MSG_MESSAGE_CREATE_SUCCESS);

	}

	function update_message() {

	}

	function delete_message() {

	}


	/**
	 * Main Qinnect_Message Instance
	 *
	 * Ensures only one instance of Qinnect_Message is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Message instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
