<?php
/**
 * Server class file.
 *
 * @package Qinnect/Server
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Server class.
 */
class Qinnect_Server {

	/**
	 * The single instance of Qinnect_Server.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin server.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available server for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $server = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = $this->parent->_token;

		$this->register_post_type();
		$this->register_category_taxonomy();
		add_action( 'add_meta_boxes', array($this,'register_metabox') );

	}

	function get_servers_by_category($slug) {
		return $this->get_servers('cat', $slug);
		// $servers = get_posts(
		// 	array(
		// 		'post_type' => POST_TYPE_SERVER,
		// 		'numberposts' => -1,
		// 		'tax_query' => array(
		// 			array(
		// 				'taxonomy' => TAXONOMY_SERVER_CAT,
		// 				'field' => 'term_slug', 
		// 				'terms' => $slug, 
		// 				'include_children' => false
		// 			)
		// 		)
		// 	)
		// );

		// $server_arr = array();

		// foreach( $servers as $server ) {
		// 	$server_arr[] = array(
		// 		'slug' => $server->post_name,
		// 		'title' => $server->post_title,
		// 		'content' => $server->post_content,
		// 		'server_key' => get_post_meta( $server->ID, 'public_key', true ),
		// 	);
		// }

		// $arr = array(
		// 	'servers' => $server_arr,
		// 	"server_count" => count($servers)
		// );

		// return $this->parent->response->success($arr, MSG_SERVERS_GET_SUCCESS);

		
	}

	function get_all() {
		return $this->get_servers('all');
	}

	function get_servers($type = 'all', $slug = null) {
		// The Query
		if ($type == 'all') {
			$args = array(
				'post_type' => POST_TYPE_SERVER,
				'orderby' => 'title',
				'order' => 'ASC'
			);
		} else {
			$args =	array(
				'post_type' => POST_TYPE_SERVER,
				'numberposts' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => TAXONOMY_SERVER_CAT,
						'field' => 'slug', 
						'terms' => $slug, 
						'include_children' => false
					)
				)
			);
		}

		$the_query = new WP_Query( $args );
		
		$servers = array();

		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				global $post;
				$id = get_the_ID();
				$thumb = get_the_post_thumbnail_url(null, 'medium_large' );
				if (!$thumb) $thumb = ASSETS_URL . 'img/qinnect-default-server.jpg';
				$servers['servers'][] = array(
					'server_key' => get_post_meta( $id, 'public_key', true ),
					'title' => get_the_title(),
					'content' => get_the_content(),
					'slug' => $post->post_name,
					'image' => $thumb
				);
			}
			$servers['server_count'] = count($servers['servers']);
			$servers['title'] = $this->get_server_title($type, $slug);
			if ($slug) $servers['category'] = $slug;
		} else {
			// no servers found
			// return $this->parent->response->error(ERR_SERVERS_NONE,'ERR_SERVERS_NONE');
			$servers['title'] = $this->get_server_title($type, $slug);
			return $this->parent->response->success($servers,ERR_SERVERS_NONE);
		}
		/* Restore original Post Data */
		wp_reset_postdata();

		// 	$servers
		return $this->parent->response->success($servers,MSG_SERVERS_GET_SUCCESS);
	}

	function get_server_title($type, $slug) {
		if ($type == 'cat') {
			$term = get_term_by( 'slug', $slug, TAXONOMY_SERVER_CAT);
			$title = $term->name;
		} else {
			$title = COMPANY_NAME;
		}
		return $title;
	}

	// function get($server_key = null) {
		
	// 	if (!$server_key) {
	// 		return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
	// 	}

	// 	$server_id = $this->get_server_id( $server_key );

	// 	if (!$server_id) {	
	// 		return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
	// 	}

	// 	// check if current user is owner can update
	// 	$server = get_post($server_id);

	// 	if (!$server) {
	// 		return $this->parent->response->error(ERR_SERVER_UPDATE,'ERR_SERVER_UPDATE');
	// 	}

	// 	$arr = array(
	// 		'title' => $server->post_title,
	// 		'content' => $server->post_content,
	// 		'server_key' => get_post_meta($server_id, 'public_key',true)
	// 	);

	// 	return $this->parent->response->success($arr,MSG_SERVER_GET_SUCCESS);
	// }

	function get($slug = null) {
		
		if (!$slug) {
			return $this->parent->response->error( 'err....'.$slug,'ERR_CHANNEL_NO_SERVER_KEY');
		}

		$server_id = $this->get_server_id_by_slug( $slug );

		if (!$server_id) {	
			return $this->parent->response->error('err....'.$slug,'ERR_CHANNEL_NO_SERVER_KEY');
		}
		
		// check if current user is owner can update

		$server = get_post($server_id);
		if (!$server) {
			return $this->parent->response->error(ERR_SERVER,'ERR_SERVER');
		}

		// $server = get_post($server_id);
		$arr = array(
			'title' => $server->post_title,
			'content' => $server->post_content,
			'server_key' => get_post_meta($server_id, 'public_key',true),
			'slug' => $slug
		);

		return $this->parent->response->success($arr,MSG_SERVER_GET_SUCCESS);
	}

	function get_server_id_by_key($value) {
		return $this->parent->helper->get_id_by_meta_key('server_key', $value);
	}

	function get_server_id_by_slug($slug) {
		if ( $post = get_page_by_path( $slug, OBJECT, POST_TYPE_SERVER ) ) {
			return $post->ID;
		}
		return false;
	}

	function create() {
		global $wpdb;
		$db_prefix = $wpdb->prefix;
	
		$user_id = get_current_user_id();

		$title = isset($_POST['title']) ? $_POST['title'] : '' ;
		$content = isset($_POST['content']) ? $_POST['content'] : '';
		$visibility = isset($_POST['visibility']) ? $_POST['visibility'] : SERVER_VISIBILITY_PUBLIC;
		$server_type = isset($_POST['type']) ? $_POST['type'] : SERVER_TYPE_TOPIC;
		$server_category_slug = isset($_POST['category']) ? $_POST['category'] : '';

		// check if title is empty + content

		// check if a server already exists with the same name
		$query = $wpdb->prepare(
			"SELECT ID FROM " . $wpdb->posts . "
			WHERE post_title = %s
			AND post_type = '" . POST_TYPE_SERVER . "'
			LIMIT 1",
			$title
		);
		$wpdb->query( $query );
	
		if ( $wpdb->num_rows ) {
			return $this->parent->response->error(ERR_SERVER_CREATE,'ERR_SERVER_CREATE');
		}

		$args = array(
			'post_type' => POST_TYPE_SERVER,
			'post_author' => $user_id,
			'post_title' => $title,
			'post_content' => $content,
			'post_status' => 'publish'
		);

		$server_id = wp_insert_post($args);
		$server = get_post($server_id);

		wp_set_object_terms( $server_id, $server_category_slug, TAXONOMY_SERVER_CAT );

		// create public and private key
		$keys = $this->parent->helper->generate_public_private_keys( $server_id, 'server' );
		$server_key = $keys['public_key'];

		// is_private
		// add_post_meta($server_id, 'is_private', $is_private );
		add_post_meta($server_id, 'visibility', $visibility );

		// server type
		add_post_meta($server_id, 'server_type', $server_type );

		// also insert into DB server/user relationship
		$table = $db_prefix . TBL_SERVER_USERS;
		$data = array(
			'server_id' => $server_id, 
			'user_id' => $user_id, 
			'role_id' => ROLE_OWNER_ID
		);
		$format = array('%d','%d','%d');
		$wpdb->insert($table,$data,$format);
		$wpdb->insert_id;

		$arr = array(
			'server_key' => $server_key,
			'title' => $title,
			'slug' => $server->post_name
		);

		return $this->parent->response->success($arr,MSG_SERVER_CREATED_SUCCESS);

	}


	function update( $server_key = null) {
		
		if (!$server_key) {
			return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
		}

		$server_id = $this->get_server_id( $server_key );

		if (!$server_id) {	
			return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
		}

		// check if current user is owner can update
		$server = get_post($server_id);

		$user_id = get_current_user_id();

		if ( $user_id != $server->post_author) {
			// do not have permission
			return $this->parent->response->error(ERR_NO_USER_PERMISSION,'ERR_NO_USER_PERMISSION');
		}

		$title = isset($_POST['title']) ? $_POST['title'] : '' ;
		$content = isset($_POST['content']) ? $_POST['content'] : '';

		$server_arr = array(
			'ID' => $server_id,
			'post_title' => $title,
			'post_content' => $content
		);
		$server = wp_update_post( $server_arr );

		if (!$server) {
			return $this->parent->response->error(ERR_SERVER_UPDATE,'ERR_SERVER_UPDATE');
		}

		$arr = array(
			'title' => $title,
			'content' => $content,
			'server_key' => $server_key
		);

		return $this->parent->response->success($arr,MSG_SERVER_UPDATED_SUCCESS);

	}


	function delete($server_key = null) {

		if (!$server_key) {
			return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
		}

		$server_id = $this->get_server_id( $server_key );

		if (!$server_id) {	
			return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
		}


		// check if current user is owner can update
		$server = get_post($server_id);

		$user_id = get_current_user_id();
		
		if ( $user_id != $server->post_author) {
			// do not have permission
			return $this->parent->response->error(ERR_NO_USER_PERMISSION,'ERR_NO_USER_PERMISSION');
		}

		$response = wp_delete_post($server_id);
		if (!$response) {
			return $this->parent->response->error($response,ERROR_SERVER_DELETE);
		}


		// delete channels
		$this->parent->channel->delete_all($server_id);
		// TODO: delete chats 
		// TODO: delete messages

		return $this->parent->response->success($response,MSG_SERVER_DELETED_SUCCESS);
		
	}
	

	function get_categories() {
		$terms = get_terms( array(
			'taxonomy' => TAXONOMY_SERVER_CAT,
			'hide_empty' => false,
		));
		$categories = array();
		foreach($terms as $term) {
			$categories[] = array(
				'title' => $term->name,
				'slug' => $term->slug,
				'content' => $term->description,
				'server_count' => $term->count
			);
		}

		$arr = array(
			'categories' => $categories,	
			'categories_count' => count($categories),	
		);
		return $this->parent->response->success($arr,MSG_SERVER_CATEGORIES_GET_SUCCESS);
	}

	function register_post_type() {
		$options = array(
			'hierarchical' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => false,
			'supports' => array( 
				'title', 
				'editor',
				'thumbnail',
				'author',
				'page-attributes' 
			),
		);
		$post_type = new Qinnect_Post_Type( 
			POST_TYPE_SERVER, 
			POST_TYPE_SERVER_PLURAL, 
			POST_TYPE_SERVER_PLURAL, 
			POST_TYPE_SERVER_PLURAL. ' available', 
			$options 
		);
		return $post_type;
	}
	function register_category_taxonomy() {
		$options = array(
			'hierarchical' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => false,
		);
		$taxonomy = new Qinnect_Taxonomy( 
			TAXONOMY_SERVER_CAT, 
			POST_TYPE_SERVER_SINGLE . ' Categories', 
			POST_TYPE_SERVER_SINGLE . ' Category', 
			array(POST_TYPE_SERVER), 
			$options 
		);

		return $taxonomy;
	}

	function register_metabox() {
		add_meta_box(
			'server_channel_metabox',
			'Server Channels',
			array($this,'server_channel_metabox'),
			POST_TYPE_SERVER,
			'advanced',
			'default'
		);
		// add_meta_box( 'server-channels-metabox', 'Channels', array($this, 'server_channel_metabox'), null, 'advanced', 'default');
	}

	function server_channel_metabox() {
		global $post;
		$server_id = $post->ID;

		$channels = $this->parent->channel->get_channels($server_id);

		if (count($channels) == 0) echo ERR_CHANNELS_NONE;

		foreach($channels as $channel) {
			echo '<div>#' . $channel->title . '</div>';
		}
	}


	function join($request) {
		global $wpdb;
		$slug = $request['slug'];
		$server_id = $this->get_server_id_by_slug($slug); 
		$user_id = get_current_user_id();

		// is private?

		$role_id = ROLE_MEMBER_ID;

		$has_joined = $this->has_joined( array('slug' => $slug) );

		if ( $has_joined ) {
			// if private, check if pending
			return $this->parent->response->error(ERR_USER_REG,'ERR_USER_REG');
		}

		// add to database;
		$wpdb->query(
			$wpdb->prepare(
			"INSERT INTO " . $wpdb->prefix . TBL_SERVER_USERS . "
			( server_id, user_id, role_id )
			VALUES ( %d, %d, %d )
			",
			array( $server_id, $user_id, $role_id )
			)
		);

		return $this->parent->response->success('Joined server ' . $server_id, MSG_REG_SUCCESS);

	}
	
	function leave($request) {
		global $wpdb;
		$slug = $request['slug'];
		$server_id = $this->get_server_id_by_slug($slug); 
		$user_id = get_current_user_id();

		$has_joined = $this->has_joined( array('slug' => $slug) );

		if ( !$has_joined ) {
			return $this->parent->response->error(ERR_USER_REG,'ERR_USER_REG');
		}

		$wpdb->query(
			$wpdb->prepare(
			"DELETE FROM " . $wpdb->prefix . TBL_SERVER_USERS . "
			WHERE server_id=$server_id AND user_id=$user_id"
			)
		);

		return $this->parent->response->success('Removed server ' . $server_id, MSG_REG_SUCCESS);
		
	}
	
	function check_if_has_joined($request) {
		$has_joined = $this->has_joined($request);
		
		if ($has_joined) {
			return $this->parent->response->success(true, MSG_SERVER_USER_HAS_JOINED);
		} 
		return $this->parent->response->success(false, MSG_SERVER_USER_HAS_NOT_JOINED);
		
	}
	
	function has_joined($request) {
		global $wpdb;
		$slug = $request['slug'];
		$server_id = $this->get_server_id_by_slug($slug); 
		$user_id = get_current_user_id();

		// is server private...?
		$server_visibility = get_post_meta( $server_id, 'visibility', true);
		// if server is private, we need to add status to tbl_server_users
		
		$query = $wpdb->prepare(
			"SELECT * FROM " . $wpdb->prefix . TBL_SERVER_USERS . "
			WHERE server_id = '%d'
			AND user_id = '%d' 
			LIMIT 1",
			$server_id,
			$user_id
		);

		$wpdb->query( $query );

		if ( $wpdb->num_rows ) {
			return true;
		}
		return false;

	}



	/**
	 * Main Qinnect_Server Instance
	 *
	 * Ensures only one instance of Qinnect_Server is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Server instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
