<?php
/**
 * Response class file.
 *
 * @package Qinnect/Response
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Response class.
 */
class Qinnect_Response {

	/**
	 * The single instance of Qinnect_Response.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin response.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available response for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $response = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		// $this->base = 'wpt_';
	}
	
	function error($msg = null, $code = null) {
		if (!$msg) $msg = ERR_DEFAULT;

		
		$error = array(
			'status' => STATUS_ERROR,
			'message' => $msg,
		);
		
		$error['error'] = $code ? strtolower( $code ) : 'error' ;

		return $error;
	}

	function success($data = null, $msg = null) {
		$results = array('status' => STATUS_SUCCESS);
		if ($data) $results['data'] = $data;
		if ($msg) $results['message'] = $msg;
		return $results;
	}


	/**
	 * Main Qinnect_Response Instance
	 *
	 * Ensures only one instance of Qinnect_Response is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Response instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
