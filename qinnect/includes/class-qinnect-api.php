<?php

/**
 * Api class file.
 *
 * @package Qinnect/Api
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Api class.
 */
class Qinnect_Api {

	/**
	 * The single instance of Qinnect_Api.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin api.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available api for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $api = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		// $this->base = 'wpt_';

		add_action( 'init', array( $this,'handle_allowed_origins') );
		add_filter( 'rest_url_prefix', array( $this, 'set_api_base' ) ); 
		add_action('rest_api_init', array( $this, 'register_api_routes' ) );

		add_filter('api_bearer_auth_unauthenticated_urls', array( $this, 'api_bearer_auth_unauthenticated_urls_filter' ), 10, 2);

	}

	function api_bearer_auth_unauthenticated_urls_filter($custom_urls, $request_method) {
    
		$base = '/api/' . API_PREFIX . '/';
	
		switch ($request_method) {
			case 'POST':
				// $custom_urls[] = '/api/api-bearer-auth/v1/login';
				$custom_urls[] = $base . ROUTE_LOGIN;
				$custom_urls[] = $base . ROUTE_LOGOUT;
				$custom_urls[] = $base . ROUTE_USER_REGISTER;
				$custom_urls[] = $base . ROUTE_LOGIN_RESET;
				$custom_urls[] = $base . ROUTE_CHANNEL_CREATE;
			break;
			case 'GET':
				$custom_urls[] = $base . ROUTE_SERVERS_GET;
				$custom_urls[] = $base . ROUTE_SERVER_CATEGORIES_GET;
				$custom_urls[] = $base . ROUTE_SERVERS_BY_CATEGORY_GET;
			break;
		}
		return $custom_urls;
	}

	function set_api_base() { 
		return API_BASE_PREFIX; 
	}

	function permissions_check() {
		$user_id = get_current_user_id();
		if (!$user_id) {
			return $this->parent->response->error(ERR_USER_NOT_LOGGED_IN,'ERR_USER_NOT_LOGGED_IN' );
		}
		return true;
	}

	function register_api_routes() {
		// method => route
		$public_routes = array(
			'user_login' => ROUTE_LOGIN,
			'user_login_reset' => ROUTE_LOGIN_RESET,
			'user_logout' => ROUTE_LOGOUT,
			'user_register' => ROUTE_USER_REGISTER,
			'server_categories_get' => ROUTE_SERVER_CATEGORIES_GET,
			'servers_get' => ROUTE_SERVERS_GET,

			'channel_create' => ROUTE_CHANNEL_CREATE,
			'servers_by_category_get' => ROUTE_SERVERS_BY_CATEGORY_GET,
		);
		foreach( $public_routes as $method => $route ) {
			register_rest_route( API_PREFIX, $route, array(
				array(
					'methods' => WP_REST_Server::ALLMETHODS,
					'callback' => array( $this, $method )
				),
			));
		}

		$private_routes = array(
			'user_login' => ROUTE_LOGIN,
			'user_logout' => ROUTE_LOGOUT,
			'user_get' => ROUTE_USER_GET,
			'user_update_profile' => ROUTE_USER_UPDATE_PROFILE,
			'user_update_email' => ROUTE_USER_UPDATE_EMAIL,
			'user_update_password' => ROUTE_USER_UPDATE_PASSWORD,
			'user_delete' => ROUTE_USER_DELETE,
			'user_register' => ROUTE_USER_REGISTER,
			'servers_get' => ROUTE_SERVERS_GET,
			'server_create' => ROUTE_SERVER_CREATE,
			'server_update' => ROUTE_SERVER_UPDATE,
			'server_delete' => ROUTE_SERVER_DELETE,
			'server_get' => ROUTE_SERVER_GET,
			'channels_get' => ROUTE_CHANNELS_GET,
			'channel_get' => ROUTE_CHANNEL_GET,
			'server_user_join' => ROUTE_SERVER_JOIN,
			'server_user_leave' => ROUTE_SERVER_LEAVE,
			'server_user_has_joined' => ROUTE_SERVER_HAS_JOINED,
			// 'channel_user_has_access' => '',
			'message_create' => ROUTE_MESSAGE_CREATE
		);
		foreach( $private_routes as $method => $route ) {
			register_rest_route( API_PREFIX, $route, array(
				array(
					'methods' => WP_REST_Server::ALLMETHODS,
					'callback' => array( $this, $method ),
					'permission_callback' => array($this,'permissions_check')
				),
			));
		}
	}
	

	function send($arr = null) {

		if (!$arr) return;
		extract($arr);

		if (!$status) $status = STATUS_NOT_FOUND;

		// prepare the array to send back to user
		$arr = array();
		if ( isset($data) ) $arr['results'] = $data;
		if ( isset($message) ) $arr['message'] = $message;

		$arr = rest_ensure_response($arr);
		
		header('Content-type:application/json;charset=utf-8');
		
		if ( $status == STATUS_SUCCESS ) {
			$response = new WP_REST_Response($arr, $status);
			return $response;
		} else {
			if ( !isset($code) ) $code = 'error';
			return new WP_Error( $code, __($message), array( 'status' => $status ) );
		}
		
	}

	// function check_if_user_logged_in() {
	// 	$user_id = get_current_user_id();
	// 	if (!$user_id) {
	// 		return $this->send( $this->parent->response->error(ERR_USER_NOT_LOGGED_IN,'ERR_USER_NOT_LOGGED_IN') );
	// 	}
	// 	return $this->send( $this->parent->response->success(array('user' => $user_id), 'user logged in') );
	// }

	// *********************************************************
	// USER
	// *********************************************************
	function user_get() { 
		return $this->send( $this->parent->user->get() );
	}
	function user_register(WP_REST_Request $request) { 
		$_POST =  $request;
		return $this->send( $this->parent->user->register() );
	}
	function user_login(WP_REST_Request $request) { 
		$_POST = $request;
		return $this->send( $this->parent->user->login() );
	}
	function user_logout() { 
		return $this->send( $this->parent->user->logout() );
	}
	
	function user_login_reset(WP_REST_Request $request) { 
		$_POST =  $request;
		return $this->send( $this->parent->user->send_password_reset() );
	}
	
	function user_refresh_token() {

	}

	function user_auth() { }
	function user_activate() { }
	function user_deactivate() { }
	function user_status() { }

	function user_update_email(WP_REST_Request $request) { 
		$_POST =  $request;
		return $this->send( $this->parent->user->update_user_email($request) );
		
	}
	function user_update_password(WP_REST_Request $request) { 
		$_POST =  $request;
		return $this->send( $this->parent->user->update_user_password($request) );
	}
	function user_update_profile(WP_REST_Request $request) { 
		$_POST =  $request;
		return $this->send( $this->parent->user->update_user($request) );
	}
	function user_delete() { 
		return $this->send( $this->parent->user->delete_user() );
	}

	// *********************************************************
	// SERVERS
	// *********************************************************
	function servers_get() { 
		return $this->send( $this->parent->server->get_all() );
	}
	function servers_by_category_get(WP_REST_Request $request) { 
		$slug = $request['slug'];
		return $this->send( $this->parent->server->get_servers_by_category($slug) );
	}
	function servers_get_category() { }
	
	function server_categories_get(WP_REST_Request $request) {
		$slug = $request['slug'];
		return $this->send( $this->parent->server->get_categories($slug) );
	}

	function server_get(WP_REST_Request $request ) { 
		$slug = $request['slug'];
		return $this->send( $this->parent->server->get($slug) );
	}

	// function server_get(WP_REST_Request $request ) { 
	// 	$server_key = $request['server_key'];
	// 	return $this->send( $this->parent->server->get($server_key) );
	// }

	function server_create(WP_REST_Request $request) { 
		$_POST = $request;
		return $this->send( $this->parent->server->create() );
	}
	
	function server_update(WP_REST_Request $request) { 
		$server_key = $request['server_key'];
		return $this->send( $this->parent->server->update($server_key) );
	}

	function server_delete(WP_REST_Request $request) { 
		$server_key = $request['server_key'];
		return $this->send( $this->parent->server->delete($server_key) );
	}

	function server_user_join(WP_REST_Request $request) { 
		return $this->send( $this->parent->server->join($request) );
	}

	function server_user_leave(WP_REST_Request $request) { 
		return $this->send( $this->parent->server->leave($request) );
	}

	function server_user_has_joined(WP_REST_Request $request) { 
		return $this->send( $this->parent->server->check_if_has_joined($request) );
	}

	// *********************************************************
	// CHANNELS
	// *********************************************************
	function channels_get(WP_REST_Request $request) {
		$slug = $request['slug'];
		return $this->send( $this->parent->channel->get_all($slug) );
	}

	function channel_messages_get(WP_REST_Request $request) { 
		$server_slug = $request['server_slug'];
		$channel_slug = $request['channel_slug'];
		return $this->send( $this->parent->message->load_messages($server_slug, $channel_slug ) );
	}

	function channel_get( WP_REST_Request $request ) { 
		$server_slug = $request['server_slug'];
		$channel_slug = $request['channel_slug'];
		return $this->send( $this->parent->channel->get($server_slug, $channel_slug) );
	}

	function channel_create( WP_REST_Request $request ) { 
		$server_slug = $request['server_slug'];
		$_POST = $request;
		return $this->send( $this->parent->channel->create($server_slug) );
	}

	function channel_update() { }

	function channel_delete() { }

	// *********************************************************
	// CHAT
	// *********************************************************
	function chats_get() { }
	function chat_get() { }
	function chat_messages_get() { }

	// *********************************************************
	// MESSAGES 
	// *********************************************************
	function message_create( WP_REST_Request $request ) { 
		return $this->send( $this->parent->message->create( $request['data'] ) );
	}
	function message_update() { }
	function message_delete() { }

	// *********************************************************
	// NOTIFICATIONS 
	// *********************************************************
	function notifications_get() { }
	function notification_add() { }
	function notification_remove() { }

	// *********************************************************
	// CORS STUFF
	// *********************************************************
	function handle_allowed_origins() {

		// $allowedOrigins = [
		//     'capacitor://localhost',
		//     'ionic://localhost',
		//     'http://localhost',
		//     'http://localhost:8080',
		//     'http://localhost:8100'
		// ];
	
		// if ( ! in_array( $_SERVER[ 'HTTP_ORIGIN' ] , $allowedOrigins ) ) return $headers;
		// $headers['Access-Control-Allow-Origin'] = $_SERVER[ 'HTTP_ORIGIN' ];
		
		header('Access-Control-Allow-Origin: *' );
		header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS");
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, Pragma, cache-control, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
		header('Content-Type: application/json');
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			//header('Access-Control-Allow-Origin: *');
			//header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
			header("HTTP/1.1 200 OK");
			die();
		}
	}


	/**
	 * Main Qinnect_Api Instance
	 *
	 * Ensures only one instance of Qinnect_Api is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Api instance
	 */
	
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
