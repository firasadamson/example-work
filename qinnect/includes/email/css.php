<?php
return '
<style>
	/* -------------------------------------
		GLOBAL RESETS
	------------------------------------- */

	/*All the styling goes here*/

	.email-img {
		border: none;
		-ms-interpolation-mode: bicubic;
		max-width: 100%;
	}

	.email-body-container {
		background-color: #f6f6f6;
		font-family: sans-serif;
		-webkit-font-smoothing: antialiased;
		font-size: 14px;
		line-height: 1.4;
		margin: 0;
		padding: 0;
		-ms-text-size-adjust: 100%;
		-webkit-text-size-adjust: 100%;
	}

	.email-body-container table {
		border-collapse: separate;
		mso-table-lspace: 0pt;
		mso-table-rspace: 0pt;
		width: 100%;
	}

	.email-body-container table td {
		font-family: sans-serif;
		font-size: 14px;
		vertical-align: top;
	}

	/* -------------------------------------
		BODY & CONTAINER
	------------------------------------- */

	.email-body {
		background-color: #f6f6f6;
		width: 100%;
	}

	/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
	.email-container {
		display: block;
		margin: 0 auto !important;
		/* makes it centered */
		max-width: 580px;
		padding: 10px;
		width: 580px;
	}

	/* This should also be a block element, so that it will fill 100% of the .email-container */
	.email-content {
		box-sizing: border-box;
		display: block;
		margin: 0 auto;
		max-width: 580px;
		padding: 10px;
	}

	/* -------------------------------------
		HEADER, FOOTER, MAIN
	------------------------------------- */
	.email-main {
		background: #ffffff;
		border-radius: 3px;
		width: 100%;
	}

	.email-body-container .email-wrapper {
		box-sizing: border-box;
		padding: 20px;
	}

	.email-body-container .content-block {
		padding-bottom: 10px;
		padding-top: 10px;
	}

	.email-body-container .email-footer {
		clear: both;
		margin-top: 10px;
		text-align: center;
		width: 100%;
	}

	.email-body-container .email-footer td,
	.email-body-container .email-footer p,
	.email-body-container .email-footer span,
	.email-body-container .email-footer a {
		color: #999999;
		font-size: 12px;
		text-align: center;
	}

	/* -------------------------------------
		TYPOGRAPHY
	------------------------------------- */
	.email-body-container h1,
	.email-body-container h2,
	.email-body-container h3,
	.email-body-container h4 {
		color: #000000;
		font-family: sans-serif;
		font-weight: 400;
		line-height: 1.4;
		margin: 0;
		margin-bottom: 30px;
	}

	.email-body-container h1 {
		font-size: 35px;
		font-weight: 300;
		text-align: center;
		text-transform: capitalize;
	}

	.email-body-container p,
	.email-body-container ul,
	.email-body-container ol {
		font-family: sans-serif;
		font-size: 14px;
		font-weight: normal;
		margin: 0;
		margin-bottom: 15px;
	}

	.email-body-container p li,
	.email-body-container ul li,
	.email-body-container ol li {
		list-style-position: inside;
		margin-left: 5px;
	}

	.email-body-container a {
		color: #3498db;
		text-decoration: underline;
	}

	/* -------------------------------------
		BUTTONS
	------------------------------------- */
	.email-body-container .btn {
		box-sizing: border-box;
		width: 100%;
	}

	.email-body-container .btn>tbody>tr>td {
		padding-bottom: 15px;
	}

	.email-body-container .btn table {
		width: auto;
	}

	.email-body-container .btn table td {
		background-color: #ffffff;
		border-radius: 5px;
		text-align: center;
	}

	.email-body-container .btn a {
		background-color: #ffffff;
		border: solid 1px #3498db;
		border-radius: 5px;
		box-sizing: border-box;
		color: #3498db;
		cursor: pointer;
		display: inline-block;
		font-size: 14px;
		font-weight: bold;
		margin: 0;
		padding: 12px 25px;
		text-decoration: none;
		text-transform: capitalize;
	}

	.email-body-container .btn-primary table td {
		background-color: #3498db;
	}

	.email-body-container .btn-primary a {
		background-color: #3498db;
		border-color: #3498db;
		color: #ffffff;
	}

	/* -------------------------------------
		OTHER STYLES THAT MIGHT BE USEFUL
	------------------------------------- */
	.last {
		margin-bottom: 0;
	}

	.first {
		margin-top: 0;
	}

	.align-center {
		text-align: center;
	}

	.align-right {
		text-align: right;
	}

	.align-left {
		text-align: left;
	}

	.clear {
		clear: both;
	}

	.mt0 {
		margin-top: 0;
	}

	.mb0 {
		margin-bottom: 0;
	}

	.email-body-container .email-preheader {
		color: transparent;
		display: none;
		height: 0;
		max-height: 0;
		max-width: 0;
		opacity: 0;
		overflow: hidden;
		mso-hide: all;
		visibility: hidden;
		width: 0;
	}

	.email-body-container .powered-by a {
		text-decoration: none;
	}

	.email-body-container hr {
		border: 0;
		border-bottom: 1px solid #f6f6f6;
		margin: 20px 0;
	}

	/* -------------------------------------
		RESPONSIVE AND MOBILE FRIENDLY STYLES
	------------------------------------- */
	@media only screen and (max-width: 620px) {
		table[class=email-body] h1 {
			font-size: 28px !important;
			margin-bottom: 10px !important;
		}

		table[class=email-body] p,
		table[class=email-body] ul,
		table[class=email-body] ol,
		table[class=email-body] td,
		table[class=email-body] span,
		table[class=email-body] a {
			font-size: 16px !important;
		}
		table[class=email-body] span.email-footer-text,
		table[class=email-body] span.email-footer-text a {
			font-size: 12px !important;
		}

		table[class=email-body] .email-wrapper,
		table[class=email-body] .article {
			padding: 10px !important;
		}

		table[class=email-body] .email-content {
			padding: 0 !important;
		}

		table[class=email-body] .email-container {
			padding: 0 !important;
			width: 100% !important;
		}

		table[class=email-body] .email-main {
			border-left-width: 0 !important;
			border-radius: 0 !important;
			border-right-width: 0 !important;
		}

		table[class=email-body] .btn table {
			width: 100% !important;
		}

		table[class=email-body] .btn a {
			width: 100% !important;
		}

		table[class=email-body] .img-responsive {
			height: auto !important;
			max-width: 100% !important;
			width: auto !important;
		}

	}

	/* -------------------------------------
		PRESERVE THESE STYLES IN THE HEAD
	------------------------------------- */
	@media all {
		.ExternalClass {
			width: 100%;
		}

		.ExternalClass,
		.ExternalClass p,
		.ExternalClass span,
		.ExternalClass font,
		.ExternalClass td,
		.ExternalClass div {
			line-height: 100%;
		}

		.apple-link a {
			color: inherit !important;
			font-family: inherit !important;
			font-size: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
			text-decoration: none !important;
		}

		#MessageViewBody a {
			color: inherit;
			text-decoration: none;
			font-size: inherit;
			font-family: inherit;
			font-weight: inherit;
			line-height: inherit;
		}

		.btn-primary table td:hover {
			background-color: #34495e !important;
		}

		.btn-primary a:hover {
			background-color: #34495e !important;
			border-color: #34495e !important;
		}
	}

	/* overrides */
	.email-body-container td,
	.email-body td {
		border-bottom: 0 !important;
	}


</style>
';