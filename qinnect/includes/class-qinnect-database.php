<?php
/**
 * Database class file.
 *
 * @package Qinnect/Database
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Database class.
 */
class Qinnect_Database {

	/**
	 * The single instance of Qinnect_Database.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin database.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available database for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $database = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = $this->parent->_token;

		$this->install_tables();
	}

	private function install_tables() {
		global $wpdb;
		$db_prefix = $wpdb->prefix;
		$charset_collate = $wpdb->get_charset_collate();
		// *************************************************
		// SERVER/USER Relationship
		// *************************************************
		$table_name = $db_prefix . TBL_SERVER_USERS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			server_id INT NOT NULL,
			user_id INT NOT NULL,
			role_id INT NOT NULL,
			access_status INT NOT NULL DEFAULT 0,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// ACCESS STATUS
		// *************************************************
		$table_name = $db_prefix . TBL_ACCESS_STATUSES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL,
			title VARCHAR(10) NOT NULL,
			type VARCHAR(10) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		$rows = array(
			array( 'id' => -1, 'title' => 'Banned', 'type' => 'banned' ),
			array( 'id' => 0, 'title' => 'No Access', 'type' => 'no-access' ),
			array( 'id' => 1, 'title' => 'Pending', 'type' => 'pending' ),
			array( 'id' => 2, 'title' => 'Approved', 'type' => 'approved' )
		);

		$q = "INSERT IGNORE INTO $table_name 
		(id, title, type) 
		VALUES ";
		foreach ( $rows as $row ) {
			$q .= $wpdb->prepare(
				"(%d, %s, %s),",
				$row['id'], $row['title'], $row['type']
			);
		}
		$q = rtrim( $q, ',' ) . ';';
		$wpdb->query( $q );

		// *************************************************
		// SERVER ROLE TYPES
		// *************************************************
		$table_name = $db_prefix . TBL_ROLE_TYPES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL,
			title VARCHAR(10) NOT NULL,
			type VARCHAR(10) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		$rows = array(
			array( 'id' => 1, 'title' => 'Member', 'type' => 'member' ),
			array( 'id' => 2, 'title' => 'Admin', 'type' => 'admin' ),
			array( 'id' => 3, 'title' => 'Owner', 'type' => 'owner' ),
		);

		$q = "INSERT IGNORE INTO $table_name 
		(id, title, type) 
		VALUES ";
		foreach ( $rows as $row ) {
			$q .= $wpdb->prepare(
				"(%d, %s, %s),",
				$row['id'], $row['title'], $row['type']
			);
		}
		$q = rtrim( $q, ',' ) . ';';
		$wpdb->query( $q );

		// *************************************************
		// VISIBILITY TYPES
		// *************************************************
		$table_name = $db_prefix . TBL_VISIBILITY_TYPES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL,
			title VARCHAR(10) NOT NULL,
			type VARCHAR(10) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		$rows = array(
			array( 'id' => -2, 'title' => 'suspended', 'type' => 'suspended' ),
			array( 'id' => -1, 'title' => 'private', 'type' => 'private' ),
			array( 'id' => 1, 'title' => 'Public', 'type' => 'public' ),
			array( 'id' => 2, 'title' => 'Unlisted', 'type' => 'unlisted' ),
		);

		$q = "INSERT IGNORE INTO $table_name 
		(id, title, type) 
		VALUES ";
		foreach ( $rows as $row ) {
			$q .= $wpdb->prepare(
				"(%d, %s, %s),",
				$row['id'], $row['title'], $row['type']
			);
		}
		$q = rtrim( $q, ',' ) . ';';
		$wpdb->query( $q );

		// *************************************************
		// CHANNELS
		// *************************************************
		$table_name = $db_prefix . TBL_CHANNELS;
		$visibility = VISIBILITY_PUBLIC;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			public_key VARCHAR(255) NOT NULL,
			private_key VARCHAR(255) NOT NULL,
			server_id INT NOT NULL,
			user_id INT NOT NULL,
			title VARCHAR(255) NOT NULL,
			content VARCHAR(255) NOT NULL,
			slug VARCHAR(255) NOT NULL,
			visibility INT(1) NOT NULL DEFAULT $visibility,
			pin INT(1) NOT NULL DEFAULT 0,
			channel_category_id INT NOT NULL DEFAULT 2,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// CHANNEL CATEGORIES
		// *************************************************
		$table_name = $db_prefix . TBL_CHANNEL_CATEGORIES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL,
			title VARCHAR(50) NOT NULL,
			type VARCHAR(50) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		$rows = array(
			array( 'id' => 1, 'title' => 'Information', 'type' => 'information' ),
			array( 'id' => 2, 'title' => 'Text Channel', 'type' => 'text-channel' ),
			array( 'id' => 3, 'title' => 'Voice Channel', 'type' => 'voice-channel' ),
			array( 'id' => 4, 'title' => 'Video Channel', 'type' => 'video-channel' ),
		);

		$q = "INSERT IGNORE INTO $table_name 
		(id, title, type) 
		VALUES ";
		foreach ( $rows as $row ) {
			$q .= $wpdb->prepare(
				"(%d, %s, %s),",
				$row['id'], $row['title'], $row['type']
			);
		}
		$q = rtrim( $q, ',' ) . ';';
		$wpdb->query( $q );

		// *************************************************
		// SERVER/USER Relationship
		// *************************************************
		$table_name = $db_prefix . TBL_CHANNEL_USERS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			channel_id INT NOT NULL,
			user_id INT NOT NULL,
			access_status INT NOT NULL DEFAULT 0,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY  (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// CHATS
		// *************************************************
		$table_name = $db_prefix . TBL_CHATS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			public_key VARCHAR(255) NOT NULL,
			private_key VARCHAR(255) NOT NULL,
			user_id INT NOT NULL,
			chat_type_id INT NOT NULL DEFAULT 1,
			title VARCHAR(100) NOT NULL,
			profile_image VARCHAR(100) NOT NULL,
			deleted INT NOT NULL DEFAULT 0,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// CHAT USERS
		// *************************************************
		$table_name = $db_prefix . TBL_CHAT_USERS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			user_id INT NOT NULL,
			read_status INT(1) NOT NULL DEFAULT 0,
			last_message_read_at DATETIME,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// CHAT TYPE
		// *************************************************
		$table_name = $db_prefix . TBL_CHAT_TYPES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL,
			title VARCHAR(10) NOT NULL,
			type VARCHAR(10) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		$rows = array(
			array( 'id' => 1, 'title' => 'One-to-One', 'type' => 'one-to-one' ),
			array( 'id' => 2, 'title' => 'Group', 'type' => 'group' ),
			array( 'id' => 3, 'title' => 'Channel', 'type' => 'channel' ),
		);

		$q = "INSERT IGNORE INTO $table_name 
		(id, title, type) 
		VALUES ";
		foreach ( $rows as $row ) {
			$q .= $wpdb->prepare(
				"(%d, %s, %s),",
				$row['id'], $row['title'], $row['type']
			);
		}
		$q = rtrim( $q, ',' ) . ';';
		$wpdb->query( $q );

		// *************************************************
		// MESSAGES
		// *************************************************
		$table_name = $db_prefix . TBL_MESSAGES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			public_key VARCHAR(255) NOT NULL,
			private_key VARCHAR(255) NOT NULL,
			title VARCHAR(255) NULL,
			message TEXT NULL,
			reference TEXT NULL,
			user_id INT NOT NULL,
			message_category_id INT NOT NULL,
			message_category_type_id INT NOT NULL DEFAULT 1,
			message_type_id INT NOT NULL DEFAULT 1,
			parent_message_id INT NOT NULL DEFAULT 0,
			status INT NOT NULL DEFAULT 1,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// MESSAGE TYPES
		// *************************************************
		// 1) create table
		$table_name = $db_prefix . TBL_MESSAGE_TYPES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL,
			type VARCHAR(10) NOT NULL,
			PRIMARY KEY  (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// 2) add default values
		$rows = array(
			array( 'id' => 1, 'type' => 'text' ),
			array( 'id' => 2, 'type' => 'photo' ),
			array( 'id' => 3, 'type' => 'video' ),
			array( 'id' => 4, 'type' => 'file' ),
			array( 'id' => 5, 'type' => 'link' )
		);

		$q = "INSERT IGNORE INTO $table_name 
		(id, type) 
		VALUES ";
		foreach ( $rows as $row ) {
			$q .= $wpdb->prepare(
				"(%d, %s),",
				$row['id'], $row['type']
			);
		}
		$q = rtrim( $q, ',' ) . ';';
		$wpdb->query( $q );


		// *************************************************
		// MESSAGE CATEGORY ( channel / chat )
		// *************************************************
		// 1) create table
		$table_name = $db_prefix . TBL_MESSAGE_CATEGORIES;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL,
			title VARCHAR(10) NOT NULL,
			type VARCHAR(10) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// 2) add default values
		$rows = array(
			array( 'id' => 1, 'title' => 'Channel', 'type' => 'channel' ),
			array( 'id' => 2, 'title' => 'Chat', 'type' => 'chat' )
		);

		$q = "INSERT IGNORE INTO $table_name 
		(id, title, type) 
		VALUES ";
		foreach ( $rows as $row ) {
			$q .= $wpdb->prepare(
				"(%d, %s, %s),",
				$row['id'], $row['title'], $row['type']
			);
		}
		$q = rtrim( $q, ',' ) . ';';
		$wpdb->query( $q );

		// *************************************************
		// USERS META
		// *************************************************
		$table_name = $db_prefix . TBL_MESSAGE_USERS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			message_id INT NOT NULL,
			user_id INT NOT NULL,
			status INT(1) NOT NULL,
			url TEXT,
			last_message_read_at DATETIME,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );


		// *************************************************
		// NOTIFICATIONS
		// *************************************************
		$table_name = $db_prefix . TBL_NOTIFICATIONS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			public_key VARCHAR(255) NOT NULL,
			private_key VARCHAR(255) NOT NULL,
			title VARCHAR(255) NOT NULL,
			content VARCHAR(255) NOT NULL,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// NOTIFICATION_USERS
		// *************************************************
		$table_name = $db_prefix . TBL_NOTIFICATION_USERS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			notification_id INT NOT NULL,
			user_id INT NOT NULL,
			status_id INT NOT NULL,
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );

		// *************************************************
		// LINKS
		// *************************************************
		$table_name = $db_prefix . TBL_URLS;
		$create_table_sql = "CREATE TABLE $table_name (
			id INT NOT NULL AUTO_INCREMENT,
			public_key VARCHAR(255) NOT NULL,
			private_key VARCHAR(255) NOT NULL,
			link TEXT NOT NULL,
			title VARCHAR(255) NOT NULL,
			content TEXT,
			image VARCHAR(255),
			created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			UNIQUE KEY id (id)
		) $charset_collate;";
		$this->maybe_create_table( $table_name, $create_table_sql );
	}




	function maybe_create_table( $table_name, $create_ddl ) {
		global $wpdb;

		$query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

		if ( $wpdb->get_var( $query ) === $table_name ) {
			return true;
		}
	
		// Didn't find it, so try to create it.
		$q = $wpdb->query( $create_ddl );
		// var_dump($q);
		// We cannot directly tell that whether this succeeded!
		if ( $wpdb->get_var( $query ) === $table_name ) {
			return true;
		}
	
		return false;
	}



	/**
	 * Main Qinnect_Database Instance
	 *
	 * Ensures only one instance of Qinnect_Database is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Database instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
