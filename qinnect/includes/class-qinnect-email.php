<?php
/**
 * Email class file.
 *
 * @package Qinnect/Email
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Email class.
 */
class Qinnect_Email {

	/**
	 * The single instance of Qinnect_Email.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin email.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available email for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $email = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		add_filter( 'wp_mail_content_type', array($this, 'set_content_type' ) );
	}
	public function set_content_type(){
		return "text/html";
	}
	public function send_email( $args = array() ) {

		$default = array(
			'to' => 'fa@firas.org',
			'subject' => 'Hi. This is a test.',
			'headers' => 'From: ' . FROM_EMAIL_ADDRESS,
			'message' => 'This is a message.'
		);

		$arr = array_merge( $default, $args);

		extract($arr);

		wp_mail( $to, $subject, $message, $headers );

	}

	function test() {
		return 'email class loaded';
	}

	function support_text() {
		$style_args = array(
			'text-align' => 'center',
			'background' => '#fafafa',
			'padding' => '15px',
			'margin' => '25px 0'
		);
		return $this->p('Need Help? <a href="#">Contact us</a> today.', $style_args);
	}

	function signature() {
		return $this->p('Thank you!<br /> The ' . COMPANY_NAME . ' Team');
	}

	function support_signature() {
		$msg = $this->support_text();
		$msg .= $this->signature();

		return $msg;
	}

	function verify_email_address( $args, $preview = false ) {
		extract($args);

		$args['preheader'] = 'Thank you for signing up. You are almost there. You just neeed to verify your account.';
		
		$subject = 'Please verify your email address';

		$header = "Thank you for signing up to " . COMPANY_NAME;
		$subheader = "You're almost ready to go.";
		$header_args = array('text-align' => 'center');
		$p_style_args = array('text-align' => 'center');

		$msg = !$preview ? $this->email_open($args) : $this->email_open_preview($args);
		
		$msg .= $this->header( $header, $subheader, $header_args);
		$msg .= $this->p('We just need you to verify your email address.', $p_style_args);
		$msg .= $this->p('<strong>Simply click the button below to confirm:</strong>', $p_style_args);
		$msg .= $this->button("Verify your email address", $activation_url);
		$msg .= $this->p('<small>Alternatively, you can click the link below or copy and paste it into a new browser window:</small><br /><small><a href="'.$activation_url.'">'.$activation_url.'</a></small>', $p_style_args);
		$msg .= $this->support_signature();
		$msg .= !$preview ? $this->email_close($args) : $this->email_close_preview($args);

		if (!$preview) {
			$email_args = array(
				'to' => $to,
				'subject' => $subject,
				'headers' => 'From: ' . FROM_EMAIL_ADDRESS,
				'message' => $msg
			);
			$this->send_email( $email_args );
		}
		return $msg;
	}

	function welcome_email( $args, $preview = false ) {
		extract($args);

		$args['preheader'] = 'Welcome to Qinnect. You are about to embark on an adventure...';
		
		$subject = 'Welcome to ' . COMPANY_NAME;

		$header = "Welcome to " . COMPANY_NAME;
		$subheader = "";
		$header_args = array();
		$p_style_args = array();

		$msg = !$preview ? $this->email_open($args) : $this->email_open_preview($args);
		
		$msg .= $this->header( $header, $subheader, $header_args);
		$msg .= $this->p('Dear Qinnector', $p_style_args);
		$msg .= $this->p('We wwould like to first congratulate you for taken this first step to join Qinnect.', $p_style_args);
		$msg .= $this->p('Here is a link to a video guide to get you started.', $p_style_args);
		$msg .= $this->signature();
		$msg .= !$preview ? $this->email_close($args) : $this->email_close_preview($args);

		if (!$preview) {
			$email_args = array(
				'to' => $to,
				'subject' => $subject,
				'headers' => 'From: ' . FROM_EMAIL_ADDRESS,
				'message' => $msg
			);
			$this->send_email( $email_args );
		}
		return $msg;
	}

	function password_reset( $args, $preview = false ) {
		extract($args);

		$args['preheader'] = "Sorry to hear you're having trouble logging into Qinnect. We can help you get straight back into your account.";
		
		$subject = 'Qinnect password reset';

		$header = "";
		$subheader = "";
		$header_args = array('text-align' => 'left');
		$p_style_args = array('text-align' => 'left');

		$msg = !$preview ? $this->email_open($args) : $this->email_open_preview($args);
		
		$msg .= $this->header( $header, $subheader, $header_args);
		$msg .= $this->p("Sorry to hear you're having trouble logging into Qinnect.");
		$msg .= $this->p("We can help you get straight back into your account.");
		$msg .= $this->button("Reset your password", $reset_url);
		$msg .= $this->p('<small>Alternatively, you can click the link below or copy and paste it into a new browser window:</small><br /><small><a href="'.$reset_url.'">'.$reset_url.'</a></small>', $p_style_args);
		$msg .= $this->p("Didn't request this email? <a href=\"#\">Click here to let us know</a>.");
		$msg .= $this->support_signature();
		$msg .= !$preview ? $this->email_close($args) : $this->email_close_preview($args);

		if (!$preview) {
			$email_args = array(
				'to' => $to,
				'subject' => $subject,
				'headers' => 'From: ' . FROM_EMAIL_ADDRESS,
				'message' => $msg
			);
			$this->send_email( $email_args );
		}
		return $msg;
	}

	function email_open($args) {

		extract($args);
		$html = $this->html_open();
		$html .= $this->head();
		$html .= $this->body_open($preheader);
		$html .= $this->content_open(LOGO_URL);
		return $html;
	}

	function email_close($args) {
		extract($args);
		$html = $this->content_close();
		$html .= $this->footer(FULL_COMPANY_ADDRESS, EMAIL_SHOW_POWERED_BY);
		$html .= $this->body_close();
		$html .= $this->html_close();
		return $html;
	}

	function email_open_preview($args) {
		extract($args);
		$html = include 'email' . DIR_SEP . 'css.php';
		$html .= $this->body_open($preheader);
		$html .= $this->content_open(LOGO_URL);
		return $html;
	}

	function email_close_preview($args) {
		extract($args);
		$html = $this->content_close();
		$html .= $this->footer(FULL_COMPANY_ADDRESS, EMAIL_SHOW_POWERED_BY);
		$html .= $this->body_close();
		return $html;
	}


	function format($text) {

		// if (strpos($text, 'https://uk.parentadmin.com') !== false ) {
		// 	$text = str_replace('https://uk.parentadmin.com', '<a href="https://uk.parentadmin.com">https://uk.parentadmin.com</a>', $text );
		// }
		return $text;
	}

	function header($text = null, $subtext = null, $args = null) {

		$default_style = array(
			'font-family' => 'sans-serif', 
			'font-size' => '20px',
			'font-weight' => 'bold',
			'margin' => '25px 0 25px 0',
			'color' => '#333333',
			'line-height' => '150%'
		);
		$style = $this->get_style($default_style, $args);
		
		$html = '<p style="'.$style.'">';
		if ($text ) {
			$html .= $text;
		}
		if ( $subtext ) {

			$default_style['font-size'] = '16px';
			$default_style['margin'] = '0 0 25px 0';

			$style = $this->get_style($default_style, $args);

			$html .= '<br><span style="'.$style.'">'.$subtext.'</span>';
		}
		$html .= '</p>';

		return $html;

	} 

	function img( $args ) {
		$defaults = array(
			'src' => '',
			'alt' => 'This is an image',
			'width' => '',
			'height' => ''
		);
		$arr = array_replace_recursive($defaults, $args);
		
		extract($arr);

		if ($src) {

			$style = 'display:block; margin:0 auto;';
			
			$img = '<img ';

			if ( $width ) {
				$img .= 'width="'.$width.'" ';
				$style .= 'max-width:'.$width.'px; width:100%;';
			}
			if ( $height ) {
				$img .= 'height="'.$height.'" ';
				$style .= isset($width) ? 'height: auto; ' : 'height:'.$height.'px;';
			}

			$img .= 'style="'.$style.'" ';
			$img .= 'alt="'.$alt.'" ';
			$img .= '/>';

			return $this->p($img);
		}
		return false;
	} 

	function get_style($default_style, $args = null) {
		$style_arr = array_replace_recursive($default_style, $args);
	
		$style = '';
		foreach( $style_arr as $k => $v ) {
			$style .= $k . ':' . $v . ';';
		}

		return $style;
	}

	function p( $text, $args = array() ) {

		$default_style = array(
			'font-family' => 'sans-serif', 
			'font-size' => '14px',
			'font-weight' => 'normal',
			'margin' => '0 0 15px 0',
			'line-height' => '150%',
			'color' => '#333333'
		);
		$style = $this->get_style($default_style, $args);
		$text = $this->format($text);
		return '<p style="'.$style.'">' . $text . '</p>';
	} 

	function button($text, $url, $args = array() ) {
		return '
		<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
			<tbody>
				<tr>
					<td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
						<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
							<tbody>
								<tr>
									<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> <a href="'.$url.'" target="_blank" style="text-align:center; display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 10px 25px; text-transform: capitalize; border-color: #3498db; width:100%;">'.$text.'</a> </td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>';
	}
	
	function html_open() {
		return '<!doctype html><html>';
	}

	function head() {
		$html = '<head>
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>Nursery In A Box</title>';
			$html .= include 'email' . DIR_SEP . 'css.php';
		$html .= '</head>';
		return $html;
	}

	function body_open($preheader = null) {
		return '
		<body class="email-body-container" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
		<span class="email-preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$preheader.'</span>
		<table border="0" cellpadding="0" cellspacing="0" class="email-body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
		<tr>
			<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
			<td class="email-container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
			<div class="email-content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">
		';
	}

	function content_open($logo) {
		$html = '
		<!-- START CENTERED WHITE CONTAINER -->
            <table class="email-main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

				<!-- START MAIN CONTENT AREA -->
				<tr>
				<td class="email-wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">';
				if ($logo) {
					$html .= '<img class="email-img" src="'.$logo.'" style="margin-bottom:25px; max-height:50px; margin:0 auto; display: block;" />';
				}
				$html .= '<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
					<tr>
						<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">';

		return $html;
	}

	function content_close() {
		return '</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- END MAIN CONTENT AREA -->
		</table>
		<!-- END CENTERED WHITE CONTAINER -->
		';
	}

	function footer($address, $show_powered_by) {
		$html = '<!-- START FOOTER -->
		<div class="email-footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
			';
			if ($address) {	
				$html .= '<tr>
					<td class="email-content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 5px; padding-top: 5px; font-size: 11px; color: #999999; text-align: center;">
						<span class="apple-link footer-text" style="color: #999999; font-size: 12px; text-align: center;">'.$address.'</span>
					</td>
				</tr>';
			}
				if ($show_powered_by) {
					$html .= '<tr>
						<td class="email-content-block powered-by footer-text" style="font-family: sans-serif; vertical-align: top; padding-bottom: 5px; padding-top: 5px; font-size: 11px; color: #999999; text-align: center;">
							<span class="footer-text">Powered by <a href="'.SITE_URL.'" style="color: #999999; font-size: 12px; text-align: center; text-decoration: underline;">'.COMPANY_NAME.'</a>.</span>
						</td>
					</tr>';
				}
			$html .= '</table>
		</div>
		<!-- END FOOTER -->
		';

		return $html;
	}

	function body_close() {
		return '
					</div>
				</td>
				<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
			</tr>
		</table>
		</body>';
	}

	function html_close() {
		return '</html>';
	}


	/**
	 * Main Qinnect_Email Instance
	 *
	 * Ensures only one instance of Qinnect_Email is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Email instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
