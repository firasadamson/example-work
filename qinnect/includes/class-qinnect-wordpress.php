<?php
/**
 * WordPress class file.
 *
 * @package Qinnect/WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WordPress class.
 */
class Qinnect_WordPress {

	/**
	 * The single instance of Qinnect_WordPress.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin wordpress.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available wordpress for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $wordpress = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		// $this->base = 'wpt_';
		// $this->remove_roles();
		$this->roles();
		add_filter('use_block_editor_for_post_type', array($this,'disable_gutenberg_editor'), 99999, 2);
		add_filter('user_can_richedit', array($this,'remove_visual_editor'),99999);
	}
	
	function disable_gutenberg_editor($current_status, $post_type) {
		if ($post_type === POST_TYPE_SERVER ) return false;
		return $current_status;
	}

	function remove_visual_editor($default) {
		if( get_post_type() === POST_TYPE_SERVER )  return false;
		return $default;
	}

	// function remove_roles() {
	// 	remove_role( 'member_level_2' );
	// 	remove_role( 'member_level_1' );
	// 	remove_role( 'level_1' );
	// 	remove_role( 'level_2' );
	// }

	function roles() {
		add_role(ROLE_PRO, __( ROLE_PRO_LABEL )); 
		add_role(ROLE_FREE, __( ROLE_FREE_LABEL ));
	}

	/**
	 * Main Qinnect_WordPress Instance
	 *
	 * Ensures only one instance of Qinnect_WordPress is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_WordPress instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
