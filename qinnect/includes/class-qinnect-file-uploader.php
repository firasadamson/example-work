<?php
/**
 * File_Uploader class file.
 *
 * @package Qinnect/File_Uploader
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * File_Uploader class.
 */
class Qinnect_File_Uploader {

	/**
	 * The single instance of Qinnect_File_Uploader.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin file_uploader.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available file_uploader for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $file_uploader = array();

	public $s3Client = '';

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->s3Client = null;
		// $this->base = 'wpt_';
	
	}

	public function init() {
		header('Access-Control-Allow-Origin: *' );
		header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, DELETE");
		header("Access-Control-Allow-Headers: Content-Type, Authorization");
		
		return new Aws\S3\S3Client([
			'region'  => 'eu-west-2',
			'version' => 'latest',
			'credentials' => [
				'key'    => "AKIATD4ED55SMCFHTYVK",
				'secret' => "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",
			]
		]);

	}

	public function check_if_exists($key) {

	}

	public function upload($file, $root_dir = 'users', $sub_dir = 'test') {

		$s3Client = $this->init();

		$key = $root_dir . '/' . $sub_dir . '/' . basename($file);

		$result = $s3Client->putObject([
			'Bucket' => 'qinnect',
			'Key'    => $key,
			'SourceFile' => $file
		]);

		return $result;
	}

	public function get($file) {
		//Creating a presigned URL
		$s3Client = $this->init();
		$cmd = $s3Client->getCommand('GetObject', [
			'Bucket' => 'qinnect',
			'Key' => $file
		]);

		$request = $s3Client->createPresignedRequest($cmd, '+20 minutes');

		// Get the actual presigned-url
		$presignedUrl = (string)$request->getUri();

		echo $presignedUrl;
	}
	

	function handle_allowed_origins() {

		header('Access-Control-Allow-Origin: *' );
		header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS");
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, Pragma, cache-control, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
		header('Content-Type: application/json');
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			header("HTTP/1.1 200 OK");
			die();
		}
	}




	/**
	 * Main Qinnect_File_Uploader Instance
	 *
	 * Ensures only one instance of Qinnect_File_Uploader is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_File_Uploader instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
