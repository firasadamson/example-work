<?php
/**
 * Channel class file.
 *
 * @package Qinnect/Channel
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Channel class.
 */
class Qinnect_Channel {

	/**
	 * The single instance of Qinnect_Channel.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin channel.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available channel for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $channel = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
	}


	function get( $server_slug, $channel_slug ) {

		global $wpdb;

		$server_id = $this->parent->server->get_server_id_by_slug($server_slug);
	

		$query = $wpdb->prepare(
			"SELECT * FROM " . $wpdb->prefix . TBL_CHANNELS . "
			WHERE server_id = '%d'
			AND slug = '%s' 
			LIMIT 1",
			$server_id,
			$channel_slug
		);

		$row = $wpdb->get_row( $query );
		
		$channel_key = $row->public_key;

		$channel_id = $this->parent->helper->get_id_by_key($channel_key, 'channel');

		$query = $wpdb->prepare(
			"SELECT * FROM " . $wpdb->prefix . TBL_MESSAGES . "
			WHERE message_category_id = '%d'",
			$channel_id
		);
		$messages = $wpdb->get_results( $query );

		$arr = array(
			'channel' => array(
				'id' => $row->public_key,
				'title' => $row->title,
				'content' => $row->content,
				'slug' => $row->slug
			),
			'messages' => $messages
		);
		return $this->parent->response->success($arr, MSG_CHANNEL_GET_SUCCESS);
	}

	function create($server_slug = null) {

		if (!$server_slug) {
			return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
		}
	
		$user_id = get_current_user_id();
		// $user_id = 1;
		if (!$user_id) {
			return $this->parent->response->error(ERR_USER_NOT_LOGGED_IN,'ERR_USER_NOT_LOGGED_IN');
		}
		

		$title = isset($_POST['title']) ? $_POST['title'] : '' ;
		$content = isset($_POST['content']) ? $_POST['content'] : '' ;
		// $is_private = isset( $_POST['is_private']) ? $_POST['is_private'] : 0 ;
		// $is_private = VISIBILITY_PUBLIC;

		if (!$title) {
			// no title empty
			return $this->parent->response->error(ERR_CHANNEL_NO_TITLE,'ERR_CHANNEL_NO_TITLE');
		}

		$slug = $this->parent->helper->slugify($title);

		$server_id = $this->parent->server->get_server_id_by_slug($server_slug, POST_TYPE_SERVER );
		if (!$server_id) {	
			return $this->parent->response->error(ERR_CHANNEL_NO_SERVER_KEY,'ERR_CHANNEL_NO_SERVER_KEY');
		}

		$keys = $this->parent->helper->generate_public_private_keys();

		global $wpdb;
		$db_prefix = $wpdb->prefix;
		
		$query = $wpdb->prepare(
			"SELECT * FROM " . $db_prefix . TBL_CHANNELS . "
			WHERE title = '%s'
			AND server_id = '%d'
			AND user_id = '%d' 
			LIMIT 1",
			$title,
			$server_id,
			$user_id
		);
		$wpdb->query( $query );

		if ( $wpdb->num_rows ) {
			// already exists
			return $this->parent->response->error(ERR_CHANNEL_CREATE,'ERR_CHANNEL_CREATE');
		}

		// also insert into DB server/user relationship
		$table = $db_prefix . TBL_CHANNELS;
		$data = array(
			'server_id' => $server_id, 
			'user_id' => $user_id,
			'title' => $title,
			'content' => $content,
			'slug' => $slug,
			'visibility' => VISIBILITY_PUBLIC,
			'public_key' => $keys['public_key'],
			'private_key' => $keys['private_key']
		);
		$format = array('%d','%d','%s','%s','%s','%d','%s','%s');
		$wpdb->insert($table,$data,$format);
		$wpdb->insert_id;

		$arr = array(
			'title' => $title,
			'slug' => $slug,
			'public_key' => $keys['public_key']
		);
		return $this->parent->response->success($arr,MSG_CHANNEL_CREATED_SUCCESS);

	}

	function get_all($slug = null) {
		if (!$slug) return;
		$channels = $this->get_channels($slug);

		$arr = array(
			'channels' => $channels,
			'channel_count' => count($channels),
		);
		
		return $this->parent->response->success($arr,MSG_CHANNELS_GET_SUCCESS);

	}

	function get_channels($slug = null) {
		if (!$slug) return;

		// $server_id = $this->parent->server->get_server_id_by_key($server_key);
		$server_id = $this->parent->server->get_server_id_by_slug($slug);
		
		global $wpdb;
		$query = $wpdb->prepare(
			"SELECT * FROM " . $wpdb->prefix . TBL_CHANNELS . "
			WHERE server_id = '%d'
			ORDER BY title ASC",
			$server_id
		);

		$results = $wpdb->get_results( $query );

		if ( !$results ) {
			$this->create_default_channels($server_id);
			return $this->get_channels($slug);
		}

		return $results;
	}

	function create_default_channels($server_id) {
		global $wpdb;
		$user_id = get_current_user_id();
		
		// also insert into DB server/user relationship
		$table = $wpdb->prefix . TBL_CHANNELS;
		
		$data = array(
			'server_id' => $server_id, 
			'user_id' => $user_id,
			'title' => 'General',
			'slug' => 'general',
			'visibility' => VISIBILITY_PUBLIC,
			'public_key' => $this->parent->helper->generate_key(),
			'private_key' => $this->parent->helper->generate_key()
		);
		$format = array('%d','%d','%s','%s','%s','%s','%s');
		$wpdb->insert($table,$data,$format);

		return $wpdb->insert_id;
	}

	function delete_all($server_id = null) {
		if (!$server_id) return;

		global $wpdb;
		$wpdb->delete( 
			$wpdb->prefix . TBL_CHANNELS, 
			array(
				'server_id' => $server_id 
			),
			array(
				'%d'
			)
		);
		return;
	}


	/**
	 * Main Qinnect_Channel Instance
	 *
	 * Ensures only one instance of Qinnect_Channel is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Channel instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
