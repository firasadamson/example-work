<?php
/**
 * Notification class file.
 *
 * @package Qinnect/Notification
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Notification class.
 */
class Qinnect_Notification {

	/**
	 * The single instance of Qinnect_Notification.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin notification.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available notification for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $notification = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		// $this->base = 'wpt_';
	}

	function subscribe($onesignal_id) {

	}

	function unsubscribe($onesignal_id) {

	}
	
	function send($title, $message) {
		$app_id = ONESIGNAL_APP_ID;
		$rest_api_key = ONESIGNAL_API_KEY;

		$onesignal_user_ids = '';
		
		$content = array(
			"en" => $message
		);
		
		$fields = array(
			'app_id' => $app_id,
			//'data' => $extraParams,
			'contents' => $content,
			'headings' => array(
				"en" => $title
			),
			'large_icon' => 'https://aiverley.com/wp-content/uploads/2019/11/cropped-AW_Faviocn_512x512-192x192.png',
			'include_player_ids' => $onesignal_user_ids
			
			//'included_segments' => ( $isTest ? json_encode(array("Test Users")) : json_encode(array("Active Users", "Inactive Users")) ),
			// 'filters' => 
			//     array(
			//         array(
			//             'field' => 'tag',
			//             'key' => 'userId',
			//             'relation' => '=',
			//             'value' => $userId
			//         )
			//     )
		);

		// if ($userId != null) {
		//     $fields['filters'] = array(
				
		//     );
		// }


		$fields = json_encode($fields);
		//print("<br />SON sent:<br />");
		//print($fields . '<br /><br />');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		$return["allresponses"] = $response;
		$return = json_encode($return);

		header('Content-Type: application/json');

		print("\nJSON received:\n");
		print($return);
		print("\n");
	}


	/**
	 * Main Qinnect_Notification Instance
	 *
	 * Ensures only one instance of Qinnect_Notification is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Notification instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
