<?php
/**
 * Helper class file.
 *
 * @package Qinnect/Helper
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Helper class.
 */
class Qinnect_Helper {

	/**
	 * The single instance of Qinnect_Helper.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin helper.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available helper for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $helper = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
	}

	function get_id_by_key($value, $type = 'server' ) {

		if ( $type == 'user' ) {

		} else if ( $type == 'channel' ) {
			return $this->get_value_by_key_from_db( 'public_key', $value, TBL_CHANNELS );
		} else {	
			return $this->get_id_by_meta_key('public_key', $value);
		}
	}

	function get_value_by_key_from_db( $key, $value, $table ) {
		global $wpdb;
		$row = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix . $table ." WHERE $key=%s", $value ) );
		if ($row) {
			return $row->id;
		}
		return false;
	}

	function get_id_by_meta_key($key, $value, $type = '' ) {
		global $wpdb;

		$meta = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->postmeta." WHERE meta_key=%s AND meta_value=%s", $key, $value ) );
	
		if (is_array($meta) && !empty($meta) && isset($meta[0])) {
			$meta = $meta[0];
		}		
		if (is_object($meta)) {
			return $meta->post_id;
		} else {
			return false;
		}

	}

	function get_public_private_keys($id = null, $type = null) {
		if ($id == null || $type == null) return false;
		
		if ($type == 'user') {
			$public_key = get_user_meta($id, 'public_key', true );
			$private_key = get_user_meta($id, 'private_key', true );
		} 
		if ($type == POST_TYPE_SERVER ) {
			$public_key = get_post_meta($id, 'public_key', true );
			$private_key =get_post_meta($id, 'private_key', true );
		}

		if ( !$public_key || !$private_key ) {

			$keys = $this->generate_public_private_keys();
			
			
			if ( !$public_key ) {
				$public_key = $keys['public_key'];
				if ($type == 'user') {
					add_user_meta($id, 'public_key', $public_key );
				}
				if ($type == POST_TYPE_SERVER) {
					add_post_meta($id, 'public_key', $public_key );
				}
			}

			if ( !$private_key ) {
				$private_key = $keys['private_key'];
				if ($type == 'user') {
					add_user_meta($id, 'private_key', $private_key );
				}
				if ($type == POST_TYPE_SERVER) {
					add_post_meta($id, 'private_key', $private_key );
				}
			}
		}

		return array('public_key' => $public_key, 'private_key' => $private_key);

	}

	function generate_public_private_keys( $id = null, $type = null ) {
		$public = $this->generate_key();
		$private = $this->generate_key();
		if ($type == 'user') {
			add_user_meta($id, 'public_key', $public );
			add_user_meta($id, 'private_key', $private );
		} 
		if ($type == POST_TYPE_SERVER ) {
			add_post_meta($id, 'public_key', $public );
			add_post_meta($id, 'private_key', $private );
		}
		return array('public_key' => $public, 'private_key' => $private);
	}
	
	function generate_key() {
		return $this->generate_random_string(32);
		// return $this->generate_random_string(32, 'simple');
		// return random_bytes(32);
	}

	function generate_random_string($stringLength = 32, $type = 'advanced') {
		// if ( $type == 'advanced' ) {
		// 	$characters = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_[]{}!@$%^*().,>=-;|:?";
		// } else {
		// 	$characters = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";
		// }
		// //get the total length of specified characters to be used in generating random string
		// $charactersLength = strlen($characters);
		// //declare a string that we will use to create the random string
		// $randomString = '';
		// for ($i = 0;$i < $stringLength;$i++) {
		// 	//generate random characters
		// 	$randomCharacter = $characters[rand(0, $charactersLength - 1) ];
		// 	//add the random characters to the random string
		// 	$randomString .= $randomCharacter;
		// };
		$stringLength = round( $stringLength / 2);
		return bin2hex(random_bytes($stringLength));
	}

	function encrypt_url_params( $params ) {
		return base64_encode(serialize( $params ));
	}

	function generate_random_user_string($stringLength) {
		//specify characters to be used in generating random string, do not specify any characters that wordpress does not allow in the creation.
		//sanitize_user, just in case
		$randomString = $this->generate_random_string($stringLength);
		$sanRandomString = sanitize_user($randomString);
		$preg = '([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z].*[_\W])';
		//check that random string contains Uppercase/Lowercase/Intergers/Special Char and that it is the correct length
		if ((preg_match($preg, $sanRandomString) == 1) && (strlen($sanRandomString) == $stringLength)) {
			//return the random string if it meets the complexity criteria
			return $sanRandomString;
		} else {
			// if the random string does not meet minimium criteria call function again
			return call_user_func("generateRandomString", ($stringLength));
		}
	} //end of generateRandomString function


	public static function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}



	/**
	 * Main Qinnect_Helper Instance
	 *
	 * Ensures only one instance of Qinnect_Helper is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_Helper instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
