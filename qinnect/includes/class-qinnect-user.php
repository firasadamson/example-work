<?php
/**
 * User class file.
 *
 * @package Qinnect/User
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * User class.
 */
class Qinnect_User {

	/**
	 * The single instance of Qinnect_User.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin user.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available user for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $user = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		// $this->base = 'wpt_';

		add_action( 'user_register', array( $this, 'registration_send_verification' ), 10, 2 );
		add_action( 'init', array($this, 'verify_user') );
	}

	function logout() {
		global $user_ID, $user_login;
		if ( $user_ID ) {
			wp_cache_delete($user_ID, 'users');
			wp_cache_delete($user_login, 'userlogins');
		}
		wp_logout();
		return $this->parent->response->success(null, MSG_LOGOUT);
	}
	

	function login() {

		// $_POST log and pwd (optional:  rememberme)
		$user = wp_signon();
		if (!$user || is_wp_error( $user )) {
			return $this->parent->response->error(ERR_USER_NOT_FOUND,'ERR_USER_NOT_FOUND');
		}

		if ( $user->ID )

		$apiBearerAuthDb = new API_Bearer_Auth_Db();
		$user_id = $user->ID;
		if ( ( $result = $apiBearerAuthDb->login($user_id) ) !== false ) {
			if ( isset($user->errors) ) {
				$this->logout();
				return $this->parent->response->error(ERR_USER_LOGIN,'ERR_USER_LOGIN');
			}
			extract($result);
			update_user_meta( $user_id, 'access_token', $access_token);
			update_user_meta( $user_id, 'token_expiry', $expires_in);
			update_user_meta( $user_id, 'refresh_token', $refresh_token);
			$user_data = [
				'user' => $user,
				'access_token' => $access_token,
				'token_expiry' => $expires_in,
				'refresh_token' => $refresh_token,
			];
			return $this->parent->response->success($user_data,MSG_LOGIN);
		}
		// user not found
		return $this->parent->response->error(ERR_USER_NOT_FOUND,'ERR_USER_NOT_FOUND');
	}

	function get() {
		$user_id = get_current_user_id();
		
		if (!$user_id) {
			return $this->parent->response->error(ERR_USER_NOT_FOUND,'ERR_USER_NOT_FOUND');
		} 
	
		$user = get_userdata( $user_id );
		$user = array(
			'user_id' => $user_id,
			'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'full_name' => trim($user->first_name . ' ' . $user->last_name),
			'emailaddress' => $user->user_email,
			'username' => $user->user_login,
			'user_key' => $this->parent->helper->get_public_private_keys($user_id, 'user')['public_key'],
		);

		return $this->parent->response->success($user,MSG_LOGIN);
	}


	function register() {
		
		$username = $_POST['username'];
		$password = $_POST['password'];
		$email = $_POST['email'];
		$user_meta = isset($_POST['user_meta']) ? $_POST['user_meta'] : null;

		// check if blank
		if ( empty($username) || empty($password) || empty($email) ) 
		{
			return $this->parent->response->error(ERR_COMPLETE_REQUIRED_FIELDS, 'ERR_COMPLETE_REQUIRED_FIELDS');
		}
		
		// verify it's an email address
		if ( !is_email($email) ) 
		{
			// output error
			return $this->parent->response->error(ERR_EMAIL_INVALID,'ERR_EMAIL_INVALID');
		}

		// minimum user characters

		// create
		$user_id = wp_create_user( $username, $password, $email );
		if (is_wp_error($user_id) ) 
		{
			$error = $user_id->get_error_message();
			return $this->parent->response->error($error,'ERR_USER_REG');
		} else {
			// default is subscriber so let's set it 
			$this->change_user_roles( $user_id, ROLE_DEFAULT, ROLE_FREE );

			if ( isset($user_meta) ) {
				$this->update_user_metas($user_meta, $user_id);
			}
			
			$user = get_user_by('id', $user_id);
			
			// set unverified - role?

			return $this->parent->response->success($user,MSG_REG_SUCCESS);
			
		}
	}


	function update_user_email($request) {

		global $wpdb, $user_ID, $user_login, $user_email;

		$new_email_address = $request['new_email_address'];
		$confirm_email_address = $request['confirm_email_address'];
		$password = $request['current_pwd'];

		// cannot be blank
		if ( $new_email_address == '' || $confirm_email_address == '' || $password == '' ) {
			return $this->parent->response->error(ERR_COMPLETE_ALL_FIELDS,'ERR_COMPLETE_ALL_FIELDS');
		}
		
		// check if email is valid
		if ( !is_email($new_email_address) ) {
			return $this->parent->response->error(ERR_EMAIL_INVALID,'ERR_EMAIL_INVALID');
		}


		if ( $user_email == $new_email_address) {
			return $this->parent->response->error(ERR_EMAIL_SAME,'ERR_EMAIL_SAME');
		}

		// check if new email and confirm email match
		if ( $new_email_address != $confirm_email_address) {
			// error
			return $this->parent->response->error(ERR_EMAILS_NOT_MATCH,'ERR_EMAILS_NOT_MATCH');
		}

		// check if email already taken
		$user = email_exists( $new_email_address );

		// check if email address already exists
		if ($user) {
			return $this->parent->response->error(ERR_EMAIL_EXISTS,'ERR_EMAIL_EXISTS');
		}
			
		$user = get_user_by( 'email', $user_email );
		if (!$user) {
			return $this->parent->response->error(ERR_USER_UPDATE,'ERR_USER_UPDATE');
		}

		// if incorrect
		if ( $user && !wp_check_password( $password, $user->data->user_pass, $user_ID ) ) {
			return $this->parent->response->error(ERR_USER_WRONG_PASSWORD,'ERR_USER_WRONG_PASSWORD');
		}

		// CORRECT PASSWORD
		$result = $wpdb->query("UPDATE " . $wpdb->prefix . "users SET user_email = '".$new_email_address."' WHERE ID = ".$user_ID);
		
		if ( $result ) { 
			// Re-log in again
			wp_cache_delete($user_ID, 'users');
			wp_cache_delete($user_login, 'userlogins'); // This might be an issue for how you are doing it. Presumably you'd need to run this for the ORIGINAL user login name, not the new one.
			wp_logout();
			wp_signon( array( 'user_login' => $new_email_address, 'user_password' => $password ) );
			$user_email = $new_email_address;

			// renew token

			// return success
			return $this->parent->response->success(array('email_address' => $new_email_address),MSG_USER_UPDATE_EMAIL_SUCCESS);
		} else {
			// error
			return $this->parent->response->error(ERR_USER_UPDATE,'ERR_USER_UPDATE');
		}

	}

	function update_user_password($request) {

		global $wpdb, $user_ID, $user_login, $user_email;

		$current_password = $request['current_pwd'];
		$new_password = $request['new_pwd'];
		$confirm_password = $request['confirm_pwd'];
		
		// cannot be blank
		if ( $current_password == '' || $new_password == '' || $confirm_password == '' ) {
			return $this->parent->response->error(ERR_COMPLETE_ALL_FIELDS,'ERR_COMPLETE_ALL_FIELDS');
		}
		
		if ( strlen($new_password) < 8 ) {
			return $this->parent->response->error(ERR_PASSWORD_MIN_LENGTH,'ERR_PASSWORD_MIN_LENGTH');
		}
		
		if ( !preg_match('/^[aA-Za-z0-9_~\-!@#\$%\^&\*\(\)]+$/', $new_password) ) {
			return $this->parent->response->error(ERR_PASSWORD_CHARS_ALLOWED,'ERR_PASSWORD_CHARS_ALLOWED');
		}
		
		if ( $new_password != $confirm_password ) {
			// INCORRECT PASSWORD
			return $this->parent->response->error(ERR_PASSWORDS_NOT_MATCH,'ERR_PASSWORDS_NOT_MATCH');
		}
		
		$user = get_user_by( 'login', $user_login );
		if (!$user) {
			// user not exist
			return $this->parent->response->error(ERR_USER_UPDATE,'ERR_USER_UPDATE');
		}
		
		if ( !wp_check_password( $current_password, $user->data->user_pass, $user->ID ) ) {
			// password incorrect
			return $this->parent->response->error(ERR_USER_WRONG_PASSWORD,'ERR_USER_WRONG_PASSWORD');
		}
		
		$result = $wpdb->query("UPDATE " . $wpdb->prefix . "users 
			SET 
			user_pass = '" . MD5( $new_password ) . "'
			WHERE ID = ".$user_ID
		);
		
		if ( $result ) { 
			
			// Re-log in again
			wp_cache_delete($user_ID, 'users');
			wp_cache_delete($user_login, 'userlogins'); // This might be an issue for how you are doing it. Presumably you'd need to run this for the ORIGINAL user login name, not the new one.
			wp_logout();
			wp_signon( array( 'user_login' => $user_login, 'user_password' => $new_password ) );

			// successfully updated password
			return $this->parent->response->success(array(), MSG_USER_UPDATE_PASSWORD_SUCCESS);
			
		} else {
			return $this->parent->response->error(ERR_USER_UPDATE,'ERR_USER_UPDATE');
		}
	}

	function update_user() {

		global $wpdb, $user_ID, $user_login;
		
		$user_id = get_current_user_id();
		$username = $_POST['username'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];

		if ( $first_name == '' || $last_name == '' || $username == '' ) {
			return $this->parent->response->error(ERR_COMPLETE_ALL_FIELDS,'ERR_COMPLETE_ALL_FIELDS');
		}

		// check if username has changed
		if ( $user_login != $username ) {
			// if it has changed, check if new username used by another user
			$user = username_exists($username);
			if ($user) {
				// exists
				return $this->parent->response->error(ERR_USERNAME_EXISTS,'ERR_USERNAME_EXISTS');
			} else {
				// CORRECT PASSWORD
				$result = $wpdb->query("UPDATE " . $wpdb->prefix . "users SET user_login='".$username."', display_name='".$username."' WHERE ID=".$user_ID);
				if ( !$result ) { 
					return $this->parent->response->error(ERR_USER_UPDATE,'ERR_USER_UPDATE');
				}
			}
		} 

		$args = array(
			'ID' => $user_id
		);

		$keys = array(
			'first_name',
			'last_name'
		);

		foreach( $keys as $key ) {
			if ( isset($_POST[$key]) ) {
				$args[$key] = $_POST[$key];
			}
		}

		$response = wp_update_user($args);

		if (is_wp_error($response)) {
			return $this->parent->response->error(ERR_USER_UPDATE,'ERR_USER_UPDATE');
		}
		return $this->parent->response->success(array(), MSG_USER_UPDATE_SUCCESS);

	}

	function delete_user() {
		require_once(ABSPATH . 'wp-admin/includes/user.php');
		$user_id = get_current_user_id();
		$deleted = wp_delete_user( $user_id );
		if (!$deleted) {
			return $this->parent->response->error(ERR_USER_DELETE,'ERR_USER_DELETE');
		}
		return $this->parent->response->success(array(), MSG_USER_DELETE_SUCCESS);
	}

	function registration_send_verification( $user_id ) {
		// get user data
		$user_info = get_userdata($user_id);
		
		// create the url
		$activation_url = $this->create_activation_url($user_id);
		// basically we will edit here to make this nicer

		// send activation email
		$args = array(
			'to' => $user_info->user_email,
			'activation_url' => $activation_url
		);
		$this->parent->email->verify_email_address($args);
	}

	function send_password_reset() {
		// get user data
		$username = $_POST['username'];

		if ( is_email($username) ) {
			$field = 'email';
		} else {
			$field = 'login';
		}
		$user_info = get_user_by( $field, $username );

		if (!$user_info) {
			// err user not found 
			return $this->parent->response->error(ERR_USER_NOT_FOUND,'ERR_USER_NOT_FOUND');
		} 

		$user_id = $user_info->ID;
		
		// create the url
		$reset_url = $this->create_password_reset_url($user_id);
		// basically we will edit here to make this nicer

		// send activation email
		$args = array(
			'to' => $user_info->user_email,
			'reset_url' => $reset_url
		);
		$this->parent->email->password_reset($args);

		$args = array();
		return $this->parent->response->success($args,MSG_LOGIN_RESET_SUCCESS);

	}

	function generate_activation_string($user_id, $activation_code) {
		$params = array('id' => $user_id, 'activation_code' => $activation_code);
		return $this->parent->helper->encrypt_url_params($params);
	}

	function create_activation_url($user_id = null) {
		if (!$user_id) $user_id = get_current_user_id();
		$activation_code = $this->parent->helper->generate_random_user_string(32);
		$string = $this->generate_activation_string($user_id, $activation_code);
		// create the activation code and activation status
		update_user_meta($user_id, 'account_activated', 0);
		update_user_meta($user_id, 'activation_code', $activation_code);
		return ACTIVATION_URL . $string;
	}
	
	function get_activation_url($user_id = null) {
		if (!$user_id) $user_id = get_current_user_id();
		$activation_code = get_user_meta( $user_id, 'activation_code', true );
		$string = $this->generate_activation_string($user_id, $activation_code);
		return ACTIVATION_URL . $string;
	}

	function create_password_reset_url($user_id = null) {
		if (!$user_id) return;
		$reset_code = $this->parent->helper->generate_random_user_string(32);
		$string = $this->generate_password_reset_string($user_id, $reset_code);
		// create the reset code
		update_user_meta($user_id, 'reset_code', $reset_code);
		return RESET_PASSWORD_URL . $string;
	}
	function generate_password_reset_string($user_id, $reset_code) {
		$params = array('id' => $user_id, 'reset_code' => $reset_code);
		return $this->parent->helper->encrypt_url_params($params);
	}

	function verify_user() {
		if(isset($_GET['act'])) {
			$data = unserialize(base64_decode($_GET['act']));
			$code = get_user_meta($data['id'], 'activation_code', true);

			// verify whether the code given is the same as ours
			if ( $code == $data['activation_code'] ) {	
				
				$user_id = $data['id'];
				$user_info = get_userdata($user_id);

				// check if already activate
				if ( !get_user_meta($user_id, 'is_activated', 1) ) {
					
					// update the user meta
					update_user_meta($user_id, 'is_activated', 1);

					$this->generate_public_private_keys( $user_id, 'user' );
	
					// send a welcome email!
					$args = array(
						'to' => $user_info->user_email
					);
					$this->parent->email->welcome_email($args);
					
				}


				// die( __( '<strong>Success:</strong> Your account has been activated! ', 'text-domain' )  );

				// DO SOMETIBNG
			} else {
				// invalid key
			}
		}
	}

	// function reset_password() {
	// 	if(isset($_GET['rpkey'])) {
	// 		$data = unserialize(base64_decode($_GET['rpkey']));
	// 		$code = get_user_meta($data['id'], 'reset_code', true);

	// 		// verify whether the code given is the same as ours
	// 		if ( $code == $data['reset_code'] ) {	
				
	// 			$user_id = $data['id'];
	// 			$user_info = get_userdata($user_id);

	// 			// check if already activate
			
	// 				// update the user meta
	// 				update_user_meta($user_id, 'reset_code', '');

	// 				$this->generate_public_private_keys( $user_id, 'user' );
	
	// 				// send a welcome email!
	// 				$args = array(
	// 					'to' => $user_info->user_email
	// 				);
	// 				$this->parent->email->welcome_email($args);
					
	// 			}


	// 			// die( __( '<strong>Success:</strong> Your account has been activated! ', 'text-domain' )  );

	// 			// DO SOMETIBNG
	// 		} else {
	// 			// invalid key
	// 		}
	// 	}
	// }

	function change_user_roles( $user_id, $current_role, $new_role ) {
		// NOTE: Of course change 3 to the appropriate user ID
		$u = new WP_User( $user_id );

		// Remove role
		$u->remove_role( $current_role );

		// Add role
		$u->add_role( $new_role );
	}

	function update_user_metas($user_data, $user_id = null, $key = null) {
		if (!$user_id) $user_id = get_current_user_id();
		if (is_array($user_data)) {
			foreach($user_data as $key => $value) {
				update_user_meta( $user_id, $key, $value );
			}
			return;
		}
		update_user_meta( $user_id, $key, $user_data);
	}


	/**
	 * Main Qinnect_User Instance
	 *
	 * Ensures only one instance of Qinnect_User is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Qinnect()
	 * @param object $parent Object instance.
	 * @return object Qinnect_User instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Qinnect_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
