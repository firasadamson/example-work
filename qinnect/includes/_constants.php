<?php


// COMPANY AND SITE CONSTS
define('COMPANY_NAME', 'Qinnect');
define('COMPANY_ADDRESS', '123 Example Street, London, A1 23B');
define('FULL_COMPANY_ADDRESS', COMPANY_NAME . ', ' . COMPANY_ADDRESS );

define('PREFIX', 'qinnect');

// *******************************************
// EMAIL TEMPLATE STUFF
// *******************************************
define( 'FROM_EMAIL_ADDRESS', 'team@qinnectapp.com' );
define("EMAIL_SHOW_POWERED_BY", true);


// ONE SIGNAL
define('ONESIGNAL_APP_ID','cbb748e5-b1dc-469b-b06d-303b2692c158');
define('ONESIGNAL_API_KEY','NjliZjMzM2EtMjU3Zi00ODMyLTlmNTQtMGI4ZWJmOTgzOWY2');

// AWS S3
define('AWS_S3_URL', 'https://qinnect.s3.eu-west-2.amazonaws.com');

// URLS
define('SITE_URL', site_url() );
define('LOGO_URL', SITE_URL . '/wp-content/plugins/qinnect/assets/img/logo/qinnect-logo.png' );
define('ACTIVATION_URL', SITE_URL . '/my-account/?act=' );
define('RESET_PASSWORD_URL', SITE_URL . '/reset/?rpkey=' );
define('ASSETS_URL', SITE_URL . '/wp-content/plugins/qinnect/assets/' );

// ROLES
define('ROLE_DEFAULT', 'subscriber');
define('ROLE_FREE_LABEL', 'Free Member');
define('ROLE_FREE', 'free');
define('ROLE_PRO_LABEL', 'Pro Member');
define('ROLE_PRO', 'pro');

// DB tables
define("TBL_BASE", PREFIX . '_');
define('TBL_SERVER_USERS', TBL_BASE . 'server_users');
// define('TBL_SERVER_USERS_PENDING', TBL_BASE . 'server_users_pending');
define('TBL_ACCESS_STATUSES', TBL_BASE . 'access_statuses');
define('TBL_ROLE_TYPES', TBL_BASE . 'role_types');
define('TBL_VISIBILITY_TYPES', TBL_BASE . 'visibility_types');
define('TBL_CHANNELS', TBL_BASE . 'channels');
define('TBL_CHANNEL_USERS', TBL_BASE . 'channel_users');
define('TBL_CHANNEL_CATEGORIES', TBL_BASE . 'channel_categories');
// define('TBL_CHANNEL_USERS_PENDING', TBL_BASE . 'channel_users_pending');
define('TBL_CHATS', TBL_BASE . 'chats');
define('TBL_CHAT_USERS', TBL_BASE . 'chat_users');
define('TBL_CHAT_TYPES', TBL_BASE . 'chat_types');
define('TBL_MESSAGES', TBL_BASE . 'messages');
define('TBL_MESSAGE_USERS', TBL_BASE . 'message_users');
define('TBL_MESSAGE_TYPES', TBL_BASE . 'message_types');
define('TBL_MESSAGE_CATEGORIES', TBL_BASE . 'message_categories');
define('TBL_NOTIFICATIONS', TBL_BASE . 'notifications');
define('TBL_NOTIFICATION_USERS', TBL_BASE . 'notification_users');
define('TBL_URLS', TBL_BASE . 'urls');

// visibility
define('SERVER_VISIBILITY_PUBLIC', 'public');
define('SERVER_VISIBILITY_PRIVATE', 'private');
define('SERVER_VISIBILITY_UNLISTED', 'unlisted');

// server roles
define('ROLE_OWNER_ID', 3);
define('ROLE_ADMIN_ID', 2);
define('ROLE_MEMBER_ID', 1);

// server types
define("SERVER_TYPE_TOPIC", 'topic');
define("SERVER_TYPE_BUSINESS", 'business');

// API STUFF
define('API_BASE_PREFIX', 'api');
define('API_PREFIX', 'v1' );

// API ROUTES
define('ROUTE_LOGIN', 'user/login' );
define('ROUTE_LOGIN_RESET', 'user/login/reset' );
define('ROUTE_LOGOUT', 'user/logout' );
define('ROUTE_USER_REGISTER', 'user/register' );
define('ROUTE_USER_UPDATE_PROFILE', 'user/profile' );
define('ROUTE_USER_UPDATE_EMAIL', 'user/email' );
define('ROUTE_USER_UPDATE_PASSWORD', 'user/password' );
define('ROUTE_USER_DELETE', 'user/delete' );
define('ROUTE_USER_GET', 'user' );
define('ROUTE_SERVERS_GET', 'servers' );
define('ROUTE_SERVER_CATEGORIES_GET', 'servers/categories' );
define('ROUTE_SERVERS_BY_CATEGORY_GET', 'servers/(?P<slug>[a-zA-Z0-9-]+)' );
// define('ROUTE_SERVER_GET', 'server/(?P<server_key>[a-zA-Z0-9-]+)' );
define('ROUTE_SERVER_GET', 'server/(?P<slug>[a-zA-Z0-9-]+)' );
define('ROUTE_SERVER_CREATE', 'server/create' );
define('ROUTE_SERVER_UPDATE', 'server/(?P<slug>[a-zA-Z0-9-]+)/update' );
define('ROUTE_SERVER_DELETE', 'server/(?P<slug>[a-zA-Z0-9-]+)/delete' );
define('ROUTE_CHANNELS_GET', 'server/(?P<slug>[a-zA-Z0-9-]+)/channels' );
define('ROUTE_CHANNEL_GET', 'server/(?P<server_slug>[a-zA-Z0-9-]+)/channel/(?P<channel_slug>[a-zA-Z0-9-]+)' );
define('ROUTE_CHANNEL_CREATE', 'server/(?P<server_slug>[a-zA-Z0-9-]+)/channel/create' );
define('ROUTE_CHANNEL_MESSAGE_CREATE', 'server/(?P<server_slug>[a-zA-Z0-9-]+)/channel/(?P<channel_slug>[a-zA-Z0-9-]+)/message' );
define('ROUTE_MESSAGE_CREATE', 'message' );

define('ROUTE_SERVER_JOIN', 'server/(?P<slug>[a-zA-Z0-9-]+)/join' );
define('ROUTE_SERVER_LEAVE', 'server/(?P<slug>[a-zA-Z0-9-]+)/leave' );
define('ROUTE_SERVER_HAS_JOINED', 'server/(?P<slug>[a-zA-Z0-9-]+)/has-joined' );

// API response status
define('STATUS_SUCCESS', 200 );
define('STATUS_ERROR', 400 );
define('STATUS_NOT_FOUND', 404 );

// VISIBILITY
define('VISIBILITY_PUBLIC', 1 );
define('VISIBILITY_PRIVATE', -1 );
define('VISIBILITY_UNLISTED', 2 );
define('VISIBILITY_SUSPENDED', -2 );

// SUCCESS MESSAGES
define('MSG_LOGIN', 'Successfully logged in.');
define('MSG_LOGIN_RESET_SUCCESS', 'Password reset email sent.');
define('MSG_LOGOUT', 'Successfully logged out.');
define('MSG_REG_SUCCESS', 'User successfully registered.');
define('MSG_SERVERS_GET_SUCCESS', 'Servers successfully fetched.');
define('MSG_SERVER_CREATED_SUCCESS', 'Server successfully created.');
define('MSG_SERVER_UPDATED_SUCCESS', 'Server successfully updated.');
define('MSG_SERVER_DELETED_SUCCESS', 'Server successfully deleted.');
define('MSG_CHANNEL_CREATED_SUCCESS', 'Channel successfully created.');
define('MSG_SERVER_GET_SUCCESS', 'Server successfully fetched.');
define('MSG_CHANNELS_GET_SUCCESS', 'Channels successfully fetched.');
define('MSG_SERVER_CATEGORIES_GET_SUCCESS', 'Server categories successfully fetched.');
define('MSG_USER_UPDATE_SUCCESS', 'User update successfully.');
define('MSG_USER_DELETE_SUCCESS', 'User deleted successfully.');
define('MSG_USER_UPDATE_EMAIL_SUCCESS', 'Your email address was successfully updated.');
define('MSG_USER_UPDATE_PASSWORD_SUCCESS', 'Your password was successfully updated.');
define('MSG_SERVER_USER_HAS_JOINED', 'User has joined the server.');
define('MSG_SERVER_USER_HAS_NOT_JOINED', 'User has not joined the server.');
define('MSG_CHANNEL_GET_SUCCESS', 'Channel successfully fetched.');
define('MSG_MESSAGE_CREATE_SUCCESS', 'Message successfully created.');

// ERROR MESSAGES
define('ERR_DEFAULT', 'An unexpected error occurred. Please try again or contact support.' );
define('ERR_USER_NOT_FOUND', 'User not found.' );
define('ERR_USER_LOGIN', 'Incorrect username and/or password. Please try again.' );
define('ERR_USER_NOT_LOGGED_IN', 'User is not logged in.' );
define('ERR_USER_REG', 'User registration failed.' );
define('ERR_NO_USER_PERMISSION', 'You do not have permission to perform this action.' );
define('ERR_COMPLETE_REQUIRED_FIELDS', 'Please complete all required fields.' );
define('ERR_COMPLETE_ALL_FIELDS', 'Please complete all fields.' );
define('ERR_SERVER', 'There was an error loading this server. Please try again.' );
define('ERR_SERVER_CREATE', 'There is already a server by that name. Please choose another name.' );
define('ERR_SERVER_UPDATE', 'There was an error updating the server. Please try again.' );
define('ERR_SERVER_DELETE', 'There was an error deleting the server. Please try again.' );
define('ERR_SERVERS_NONE', 'No servers found.' );
define('ERR_CHANNEL_CREATE', 'There is already a channel by that name. Please choose another name.' );
define('ERR_CHANNEL_NO_TITLE', 'Channel title cannot be empty.' );
define('ERR_CHANNEL_NO_SERVER_KEY', 'There is no server associated with this request.' );
define('ERR_CHANNELS_NONE', 'There are no channels on this server.' );
define('ERR_CHANNELS_GET', 'There was an error fetching the channels.' );
define('ERR_USER_UPDATE', 'There was an error updating your account. Please try again.' );
define('ERR_USER_DELETE', 'There was an error deleting your account. Please try again.' );
define('ERR_EMAILS_NOT_MATCH', 'The email addresses you entered did not match. Please try again.' );
define('ERR_PASSWORDS_NOT_MATCH', 'The new passwords you entered did not match. Please try again.' );
define('ERR_USER_WRONG_PASSWORD', 'Your password is incorrect. Please try again.' );
define('ERR_EMAIL_EXISTS', 'This email address is already used in another Qinnect account. Please use a different email address.' );
define('ERR_EMAIL_SAME', 'This is your current email address. Please use a different email address.' );
define('ERR_EMAIL_INVALID', 'You have entered an invalid email address. Please try again.' );
define('ERR_PASSWORD_MIN_LENGTH', 'Your password needs to be at least 8 characters long. Please try again.' );
define('ERR_PASSWORD_CHARS_ALLOWED', 'Only letters, numbers and some special characters (_ ~ - ! @ # $ % ( ) ^ & *) are allowed.' );
define('ERR_USERNAME_EXISTS', 'This username is already taken. Please try a different username.' );

// SERVER STUFF
define("DIR_SEP", DIRECTORY_SEPARATOR);

// WORDPRESS STUFF
define("POST_TYPE_SERVER", 'server');
define("POST_TYPE_SERVER_SINGLE", 'Server' );
define("POST_TYPE_SERVER_PLURAL", 'Servers');
define("TAXONOMY_SERVER_CAT", POST_TYPE_SERVER . '-cat');
// THIRD PARTY STUFF

