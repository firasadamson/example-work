<?php
/**
 * Plugin Name: Qinnect
 * Version: 1.0.0
 * Plugin URI: http://www.firas.org/
 * Description: Qinnect API plugin.
 * Author: Firas
 * Author URI: http://www.firas.prg/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: qinnect
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Firas
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once 'includes/_constants.php';

require 'vendor/autoload.php';

// Load plugin class files.
require_once 'includes/class-qinnect.php';
require_once 'includes/class-qinnect-api.php';
require_once 'includes/class-qinnect-channel.php';
require_once 'includes/class-qinnect-chat.php';
require_once 'includes/class-qinnect-database.php';
require_once 'includes/class-qinnect-email.php';
require_once 'includes/class-qinnect-file-uploader.php';
require_once 'includes/class-qinnect-helper.php';
require_once 'includes/class-qinnect-message.php';
require_once 'includes/class-qinnect-notification.php';
require_once 'includes/class-qinnect-response.php';
require_once 'includes/class-qinnect-server.php';
require_once 'includes/class-qinnect-settings.php';
require_once 'includes/class-qinnect-user.php';
require_once 'includes/class-qinnect-wordpress.php';

// Load plugin libraries.
require_once 'includes/lib/class-qinnect-admin-api.php';
require_once 'includes/lib/class-qinnect-post-type.php';
require_once 'includes/lib/class-qinnect-taxonomy.php';


/**
 * Returns the main instance of Qinnect to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Qinnect
 */
function qinnect() {
	$instance = Qinnect::instance( __FILE__, '1.0.0' );

	if ( !isset( $instance->api ) ) {
		$instance->api = Qinnect_Api::instance( $instance );
	}
	if ( !isset( $instance->channel ) ) {
		$instance->channel = Qinnect_Channel::instance( $instance );
	}
	if ( !isset( $instance->chat ) ) {
		$instance->chat = Qinnect_Chat::instance( $instance );
	}
	if ( !isset( $instance->database ) ) {
		$instance->database = Qinnect_Database::instance( $instance );
	}
	if ( !isset( $instance->email ) ) {
		$instance->email = Qinnect_Email::instance( $instance );
	}
	if ( !isset( $instance->file_uploader ) ) {
		$instance->file_uploader = Qinnect_File_Uploader::instance( $instance );
	}
	if ( !isset( $instance->helper ) ) {
		$instance->helper = Qinnect_Helper::instance( $instance );
	}
	if ( !isset( $instance->message ) ) {
		$instance->message = Qinnect_Message::instance( $instance );
	}
	if ( !isset( $instance->notification ) ) {
		$instance->notification = Qinnect_Notification::instance( $instance );
	}
	if ( !isset( $instance->response ) ) {
		$instance->response = Qinnect_Response::instance( $instance );
	}
	if ( !isset( $instance->server ) ) {
		$instance->server = Qinnect_Server::instance( $instance );
	}
	if ( !isset( $instance->settings ) ) {
		$instance->settings = Qinnect_Settings::instance( $instance );
	}
	if ( !isset( $instance->user ) ) {
		$instance->user = Qinnect_User::instance( $instance );
	}
	if ( !isset( $instance->wordpress ) ) {
		$instance->wordpress = Qinnect_WordPress::instance( $instance );
	}

	return $instance;
}

qinnect();
