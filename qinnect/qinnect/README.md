# README

This readme contains the API endpoints for Qinnect APIs.

## API

**API Base: /api/v1/**

| API ENDPOINTS                               | DESCRIPTION                     | STATUS
| ------------------------------------------- | ------------------------------- | -
| user                                        | Fetch user details              | done
| user/register                               | Create new user                 | done
| user/login                                  | Log in user /jwt-auth/v1/token  | done
| user/login/reset                            | Reset password request          | done
| user/logout                                 | user logout                     | done
| user/refresh                                | Refresh Token                   |
| user/update                                 | Update email                    | done
| user/update/email                           | Update email                    | 
| user/update/password                        | Update password                 | 
| user/activate                               | Activate user                   | 
| user/deactivate                             | Deactivate user                 | 
| user/status                                 | Is user active or inactive user | 
| user/delete                                 | Delete user (for GDPR purposes) | done

| servers                                     | list of all servers             | done
| servers/categories                          | category                        | done
| server/:id                                  | get server                      | done
| server/create                               | create server                   | done
| server/:id/update                           | update server                   | done
| server/:id/delete                           | delete swever                   | done
| server/:id/channels                         | get a list of all channels      | done
| server/:id/users                            | get list of members             |

| server/:id/channels/:id/                    | get channel details             |
| server/:id/channels/:id/update              | update channel                  |
| server/:id/channels/:id/delete              | delete channel                  |

| server/:id/channels/:id/messages            | get messages                    |
| server/:id/channels/:id/messages/create     | create message for channel      |
| server/:id/channels/:id/messages/:id        | get message details             |
| server/:id/channels/:id/messages/:id/update | update message                  |
| server/:id/channels/:id/messages/:id/delete | delete message                  |

| chats/                                      | get list of DM chats            |
| chats/:id                                   | get chat details                |
| chats/create                                | create a new chat   / group     |
| chats/:id/update                            | Update chat                     |
| chats/:id/delete                            | delete chat and all messages    |
| chats/:id/messages                          | get chat message                |
| chats/:id/messages/create                   | create new message              |
| chats/:id/messages/:id/update               | update message                  |
| chats/:id/messages/:id/delete               | delete message                  |
| chats/:id/messages/:id/meta                 | get meta info                   |

| notifications                               | notifications                   |
| notifications/read                          | mark notification as read       |

