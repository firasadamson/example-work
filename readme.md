# EXAMPLE WORK

This repo contains some example work. The list below will explain what each project is:

1. flight-alerts - WP plugin used for the RareFare project
2. qinnect - this contains a WP plugin that would power a chat platform (similar to Discord) via API. However this project got cancelled but it shows what I've done so far.
3. qinnect-socket-server - this was a very basic NodeJS setup to allow instant messaging on the chats.

Frontend work (all WordPress):

1. [https://www.rarefare.co.uk/](https://www.rarefare.co.uk/)
2. [https://www.switcheroo.deals/](https://www.switcheroo.deals/)
3. [https://www.fishingtv.com/](https://www.fishingtv.com/)

If you need more example work, please do not hesitate to get in touch.
Firas