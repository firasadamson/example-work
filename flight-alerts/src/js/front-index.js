/**
 * Frontend entry point.
 *
 * src/front/front-index.js
 */

require( './front/_global' );
require( './front/_range-slider' );

require( './front/account-email' );
require( './front/account-password' );
require( './front/account-mobile' );
require( './front/account-billing' );
require( './front/account-plan' );

require( './front/airports' );
require( './front/budget' );
require( './front/calendar' );
require( './front/regions' );
require( './front/select-plan' );
require( './front/hotel-ratings' );
require( './front/notifications' );
require( './front/welcome' );
require( './front/woocommerce' );