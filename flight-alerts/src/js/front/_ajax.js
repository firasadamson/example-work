const button = require( './_button' );
const alert = require( './_alert' );

module.exports = function ajax( el, data ) {

    el.html( button.pressed( el ) );

    return jQuery.ajax({
        url: flight_alerts.ajax_url,
        type: 'POST',
        enctype: 'multipart/form-data',
        data: data
    }).done( function (response) {

        if (data.disable_default_done) { 
            return response;
        }

        if (response.status == 'success') {
            alert.success('Update successful.');
        } else {
            alert.error('Error - ' + response.message);
        }

        return response;

    }).fail( function (response) {

        if (data.disable_default_done) {
            return response;
        }

        alert.error('Error - Please try again or contact support.');
        console.log('response err',response);
        return response;

    }).done( function() {
        
        if (data.disable_default_done) {
            return;
        }
        
        el.html( button.restored( el ) );

    });
}