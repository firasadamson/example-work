const ajax = require('./_ajax');
const alert = require( './_alert' );

jQuery(document).ready( function() {
    jQuery('#account-password-save-btn').click( function(e) {
        e.preventDefault();

        var current_password = jQuery('#current-password').val();
        var new_password = jQuery('#new-password').val();
        var confirm_password = jQuery('#confirm-password').val();

        if ( !current_password || !new_password || !confirm_password ) {
            alert.error("Please complete all fields.");
            return;
        }

        if ( new_password.length < 6 ) {
            alert.error("Please enter at least 6 characters for your password.");
            return;
        }

        if ( new_password != confirm_password ) {
            alert.error("Your password did not match. Please try again.");
            return;
        }

        let data = {
            action: 'update_account_password_action',
            current_password: current_password,
            new_password: new_password,
            confirm_password : confirm_password 
        }
        ajax( jQuery(this), data ).done( function(res) {
            if ( res.status == 'success' ) {
                jQuery('#current-password').val('');
                jQuery('#new-password').val('');
                jQuery('#confirm-password').val('');
            } 
        });



    });
    
}); 