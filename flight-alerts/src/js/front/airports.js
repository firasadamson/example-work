const alert = require( './_alert' );
const ajax = require('./_ajax');
const update_welcome = require( './update-welcome' );

jQuery(document).ready( function() {

    jQuery('.airport-country-title').click( function(e) {
        e.preventDefault();
        jQuery(this).parent().children('.airports-wrapper').toggle();
        jQuery(this).find('.icon').toggleClass("ticon-chevron-down");
        jQuery(this).find('.icon').toggleClass("ticon-chevron-up");
    });

    var airports = [];

    if (jQuery('#welcome-airports-back-btn').length > 0) {
        jQuery('#welcome-airports-back-btn').remove();
    }

    jQuery('#airports-save-btn, .airports-welcome-btn, #airports-next-btn').click( function(e) {
        e.preventDefault();
        var btn_el = jQuery(this);
        var btn_action = btn_el.data('action');
        var redirect = btn_el.data('redirect');
        var count = 0;
        jQuery('input[type=checkbox]:checked').each( function() {
                let val = jQuery(this).val();
                airports.push(val);
                count++
        });
        if (count == 0) {
            alert.error('Please select at least one airport.');
            return;
        } else {

            let data = {
                action: 'update_airports_action',
                airports: airports,
                disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false
            }

            ajax( btn_el, data )
            .done( function(res) {
                if ( res.status == 'success' ) {

                    // if has redirect
                    if ( redirect ) {
                        window.location.href = redirect;
                        return;
                    }
                
                    // if welcome
                    let current_step = 1;
                    update_welcome( current_step, btn_el );

                } 
            })
            .fail( function() {

            })
            .always( function() {
                
            });

        }
    })
}); 