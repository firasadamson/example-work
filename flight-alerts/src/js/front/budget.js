const ajax = require('./_ajax');
const alert = require( './_alert' );
const update_welcome = require( './update-welcome' );

jQuery(document).ready( function() {
    console.log('budget');

    // jQuery( '.region-range-input' ).on('input change', function(e) {
    //     let val = jQuery(this).val();
    //     let key = jQuery(this).attr('data-region-key');
    //     if (val == 1000) val = '1000+';
    //     console.log('range...', key, val );
        
    //     jQuery('#'+key+'-output').html(val);
    // });

    jQuery('#budget-save-btn, .budget-welcome-btn, #budget-next-btn').click( function(e) {
        e.preventDefault();;
        var btn_el = jQuery(this);
        var btn_action = btn_el.data('action');
        var redirect = btn_el.data('redirect');

        var budgets = []

        jQuery('.region-range-input').each( function(e) {
            let key = jQuery(this).data('key');
            let val = jQuery(this).val();
            budgets.push({ key : key, val : val });
        });

        let data = {
            action: 'update_budget_action',
            budgets : budgets,
            disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false
        }

        console.log(data)
        ajax( btn_el, data )
            .done( function(res) {
                if ( res.status == 'success' ) {
                    // if has redirect
                    if ( redirect ) {
                        window.location.href = redirect;
                        return;
                    }
                    // if welcome
                    let current_step = 4;
                    update_welcome( current_step, btn_el );
                } 
            })
            .fail( function() {

            })
            .always( function() {
                
            });
    });

    // jQuery('input[type="range"]').rangeslider({
    //     polyfill: false, 

    //     // Default CSS classes
    //     rangeClass: 'rangeslider',
    //     disabledClass: 'rangeslider--disabled',
    //     horizontalClass: 'rangeslider--horizontal',
    //     verticalClass: 'rangeslider--vertical',
    //     fillClass: 'rangeslider__fill',
    //     handleClass: 'rangeslider__handle',

    //     // Callback function
    //     onInit: function() {
    //         console.log('on init');
    //     },
    
    //     // Callback function
    //     onSlide: function(position, value) {
    //         console.log('on slide end',value)
    //         if (value == 1000) value = '1000+';
    //         update_range( jQuery(this), value );
    //         // jQuery('.range-output').html(value)
    //     },
    
    //     // Callback function
    //     onSlideEnd: function(position, value) {
    //         console.log('on slide end', value)
    //         if (value == 1000) value = '1000+';
    //         update_range( jQuery(this),value );
    //         // jQuery('.range-output').html(value)
    //     }
    // });

    // function update_range(el, val) {
    //     // let val = el.val();
    //     let key = el.attr('data-region-key');
    //     console.log('range...', key, val );
        
    //     jQuery('#'+key+'-output').html(val);
    // }
}); 