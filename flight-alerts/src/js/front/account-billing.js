const ajax = require('./_ajax');
const alert = require( './_alert' );

jQuery(document).ready( function() {
    
    jQuery('#account-billing-save-btn').click( function(e) {
        e.preventDefault();
        console.log('click billing save btn');
        var billing = []
        jQuery('.billing-input').each( function() {
            let key = jQuery(this).attr('id');
            let val = jQuery(this).val();
            billing.push({key: key, val: val});
        });

        let data = {
            action: 'update_account_billing_action',
            billing: billing
        }
        ajax( jQuery(this), data );
    });
    
}); 