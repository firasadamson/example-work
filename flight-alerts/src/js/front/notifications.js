
const ajax = require('./_ajax');
const alert = require( './_alert' );
const update_welcome = require( './update-welcome' );

jQuery(document).ready( function() {

    // var notifications = [];

    if ( jQuery('#country_code').length > 0 ) {
        if ( jQuery('#country_code').val() == "" ) {
            jQuery('#country_code').val('GB');
        }
    }

    jQuery('.country-code-dropdown').on('change', function() {
        jQuery('#country-code-output').html( jQuery(this).val() );
    })

    // jQuery( '.user-notifications-range-input' ).on('input change', function(e) {
    //     let val = jQuery(this).val();
    //     console.log('user-notifications-range-input range...', val );
    //     jQuery('#user-notifications-output').html(val);
    // });

    jQuery('#notifications-save-btn, .notifications-welcome-btn').click( function(e) {
        e.preventDefault();
        var btn_el = jQuery(this);
        var btn_action = btn_el.data('action');

        var mobile = jQuery('#mobile').val();
        var country_code = jQuery('#country_code').val();

        // if ( !country_code || !mobile ) {
        //     alert.error("Please ensure you select a country code and enter your mobile number.");
        //     return;
        // }
    
        let data = {
            action: 'update_notifications_action',
            notifications: jQuery('#user-notifications-input').val(),
            mobile: mobile,
            mobile_country_code: country_code,
            disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false
        }
        ajax( btn_el, data )
        .done( function(res) {
            if ( res.status == 'success' ) {
                // if welcome
                let current_step = 6;
                    update_welcome( current_step, btn_el );

            } 
        })
        .fail( function() {

        })
        .always( function() {
            
        });
    })
}); 