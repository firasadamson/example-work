const ajax = require('./_ajax');
const alert = require( './_alert' );

jQuery(document).ready( function() {

    if ( jQuery('#country_code').length > 0 ) {
        if ( jQuery('#country_code').val() == "" ) {
            jQuery('#country_code').val('GB');
        }
    }
    
    jQuery('#mobile').on("keyup", function() {
        this.value = this.value.replace(/[^0-9\.]/g,'');
        if (/^0/.test(this.value)) {
            this.value = this.value.replace(/^0/, "")
        }
    });

    jQuery('.country-code-dropdown').on('change', function() {
        jQuery('#country-code-output').html( jQuery(this).val() );
    })

    jQuery('#account-mobile-save-btn').click( function(e) {
        e.preventDefault();
        var mobile = jQuery('#mobile').val();
        var country_code = jQuery('#country_code').val();

        if ( !country_code || !mobile ) {
            alert.error("Please ensure you select a country code and enter your mobile number.");
            return;
        }

        let data = {
            action: 'update_account_mobile_action',
            mobile: mobile,
            mobile_country_code: country_code,
        }
        ajax( jQuery(this), data );
    });
    
}); 