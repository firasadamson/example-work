module.exports = {

    success : function( text = '' ) {
        this.alert(text, 'success');
    },

    error: function( text = '' ) {
        this.alert(text, 'danger');
    },

    alert: function( text, type = 'primary' ) {
        this.remove();
        let html = '';
        html += '<div class="alert alert-' + type + '" role="alert">';
        html += text;
        html += '</div>';

        jQuery('.site-content').prepend( html );

        jQuery("html, body").animate({ scrollTop: 0 }, "fast");

        if (type != 'danger') {
            let THIS = this;
            setTimeout( function() {
                THIS.remove();
            }, 3000);
        }
    },

    remove: function() {
        jQuery('.alert').remove();
    }
    
}