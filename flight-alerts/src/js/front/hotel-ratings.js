
const ajax = require('./_ajax');
const alert = require( './_alert' );
const update_welcome = require( './update-welcome' );

jQuery(document).ready( function() {

    var hotel_ratings = [];

    jQuery('#hotel-ratings-save-btn, .hotel-ratings-welcome-btn, #hotel-ratings-next-btn').click( function(e) {
        e.preventDefault();
        var btn_el = jQuery(this);
        var btn_action = btn_el.data('action');
        var redirect = btn_el.data('redirect');
        var count = 0;

        jQuery('input[type=checkbox]:checked').each( function() {
                let val = jQuery(this).val();
                hotel_ratings.push(val);
                count++
        });

        if (count == 0) {
            alert.error('Please select a rating.');
            return;
        } else {
            let data = {
                action: 'update_hotel_ratings_action',
                hotel_ratings: hotel_ratings,
                disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false
            }
            ajax( btn_el, data )
            .done( function(res) {
                if ( res.status == 'success' ) {
                     // if has redirect
                    if ( redirect ) {
                        window.location.href = redirect;
                        return;
                    }
                    // if welcome
                    let current_step = 5;
                    update_welcome( current_step, btn_el );

                } 
            })
            .fail( function() {

            })
            .always( function() {
                
            });
        }
    })
}); 