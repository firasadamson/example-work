const alert = require( './_alert' );
const ajax = require('./_ajax');
const validate = require( './_validate' );

jQuery(document).ready( function() {

    jQuery('#account-email-save-btn').click( function(e) {

        var current_email = jQuery('#current-email').html();

        var new_email = jQuery('#new-email').val();
        var confirm_email = jQuery('#confirm-email').val();
        var password = jQuery('#password').val();

        if ( !new_email || !confirm_email || !password ) {
            alert.error("Please complete all fields.");
            return;
        }

        if ( new_email == current_email ) {
            alert.error("You have entered the same email address as your current one. Please enter a different one.");
            return;
        }

        if ( !validate.email(new_email) ) {
            alert.error("You have not entered a valid email address. Please try again.");
            return;
        }
        
        if ( new_email != confirm_email ) {
            alert.error("The email address you entered did not match. Please try again.");
            return;
        }

        let data = {
            action: 'update_account_email_action',
            new_email: new_email,
            confirm_email: confirm_email,
            password: password
        }
        ajax( jQuery(this), data ).done( function(res) {
            if ( res.status == 'success' ) {
                jQuery('#current-email').html(new_email);
                jQuery('#new-email').val('');
                jQuery('#confirm-email').val('');
                jQuery('#password').val('');
            } 
        });

    });

    // check if match

    // if all good, ajax it

    // once processed, clear form and just show current email address at the top?
    
}); 