const ajax = require( './_ajax' );

module.exports = function update_welcome( current_step, btn_el ) {
    
    var btn_action = btn_el.data('action');
    
    if ( btn_action == 'next' || btn_action == 'back' ) {
        let prev_step = ( current_step <= 1 ? 1 : current_step - 1 );
        let next_step = current_step + 1;
        let data = {
            action: 'update_welcome_setup_action',
            next_step: btn_action == 'back' ?  prev_step : next_step,
            disable_default_done: true
        }
        ajax( btn_el, data ).done( function() {
            // location.reload();
            window.location.replace('/signup/welcome/');
        })
    }

}