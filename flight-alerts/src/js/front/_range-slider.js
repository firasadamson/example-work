if ( jQuery('.range-value').length > 0 ) {
    
    update_range_values();

    // jQuery(window).on('pageshow', function(){
    //     console.info('Entered the page!');
    //     jQuery('.range-value').each( function() {
    //         jQuery(this).trigger('click');
    //     });
    // });

    

    jQuery(document).on('input change', '.region-range-input', function() {
        let val = jQuery(this).val();
        jQuery(this).val(val);
        let val2 = jQuery(this).val();
        console.log('val2',val2);
    });

}
function update_range_values() {
    jQuery('.range-value').each( function() {
        let id = jQuery(this).data('id');
        let type = jQuery(this).data('type');
        let max = jQuery(this).data('max');
        let label = type == 'currency' ? '£' : '';
        
        console.log('run!!', jQuery('#'+id + '-input').val() );
        console.log('max',max);
        const
        range = document.getElementById(id + '-input'),
        rangeV = document.getElementById('range-' + id),
        setValue = ()=> {
            const newValue = Number( (range.value - range.min) * 100 / (range.max - range.min) );
            const newPosition = 10 - (newValue * 0.2);

            let range_value = range.value;
            let output = label + range_value;
            if ( max && range_value >= max ) {
                output = label + range_value + '+';
            } 
            rangeV.innerHTML = '<span>' + output + '</span>';
            rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
        };
        document.addEventListener("DOMContentLoaded", setValue);
        range.addEventListener('input', setValue);
        
    });
}