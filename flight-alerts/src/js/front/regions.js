const ajax = require('./_ajax');
const alert = require( './_alert' );
const update_welcome = require( './update-welcome' );

jQuery(document).ready( function() {

    var regions = [];

    jQuery('.regions-checkbox').click( function() {
        let val = jQuery(this).val();
        if (jQuery(this).is(':checked') ) {
            jQuery('#map-'+val).show();
        } else {
            jQuery('#map-'+val).hide();
        }
    })
    
    jQuery('.region-select-all').click( function(e) {
        e.preventDefault();
        
        if ( jQuery('.checkbox:checked').length == jQuery('.checkbox').length ) {
            jQuery('.map-img').show();
        } else if ( jQuery('.checkbox:checked').length < jQuery('.checkbox').length ) {
            jQuery('.map-img').hide();
        } 
        
    })

    jQuery('#regions-save-btn, .regions-welcome-btn, #regions-next-btn').click( function(e) {
        e.preventDefault();
        var btn_el = jQuery(this);
        var btn_action = btn_el.data('action');
        var redirect = btn_el.data('redirect');
        var count = 0;
        jQuery('input[type=checkbox]:checked').each( function() {
                let val = jQuery(this).val();
                regions.push(val);
                count++
        });

        if (count == 0) {
            alert.error('Please select at least one region.');
            return;
        } else {
            let data = {
                action: 'update_regions_action',
                regions: regions,
                disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false
            }
            
            ajax( btn_el, data )
            .done( function(res) {
                if ( res.status == 'success' ) {
                    // if has redirect
                    if ( redirect ) {
                        window.location.href = redirect;
                        return;
                    }
                    // if welcome
                    let current_step = 2;
                    update_welcome( current_step, btn_el );

                } 
            })
            .fail( function() {

            })
            .always( function() {
                
            });
        }
    });
}); 