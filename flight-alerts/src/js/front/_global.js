jQuery(document).ready( function() {

    jQuery('.copy-to-clipboard').click( function(e) {
        e.preventDefault();
        let input_el = jQuery(this).data('element-id');
        let success_message = jQuery(this).data('success-message');
        var copyText = document.getElementById(input_el);
        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        if ( jQuery('.copy-to-clipboard-message').length > 0 ) {
            jQuery('.copy-to-clipboard-message').html("");
            setTimeout( function() {
                jQuery('.copy-to-clipboard-message').html(success_message);
            },500);
        } else {
            alert(success_message);
        }
    })

    if ( jQuery('.select-all').length > 0 ) {
        check_if_select_all();
        jQuery('.checkbox').click( function() {
            check_if_select_all();
        })
    }

    jQuery('.select-all').click( function(e) {
        e.preventDefault(); 

        jQuery(this).find('.select-all-checkbox').prop('checked', function(index, attr) {
            attr = attr == false ? true : false;
            jQuery('.checkbox').prop('checked', attr );
            return attr;
        });
    })

    jQuery('.select-none').click( function(e) {
        e.preventDefault();
        jQuery('.checkbox').prop('checked',false);
    })

    function check_if_select_all() {
        if ( jQuery('.checkbox:checked').length == jQuery('.checkbox').length ) {
            jQuery('.select-all-checkbox').prop('checked',true);
        } else if ( jQuery('.checkbox:checked').length < jQuery('.checkbox').length ) {
            jQuery('.select-all-checkbox').prop('checked', false);
        } 
    }
}); 

