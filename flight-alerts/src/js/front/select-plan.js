const ajax = require('./_ajax');
const alert = require( './_alert' );
const update_welcome = require( './update-welcome' );

jQuery(document).ready( function() {

    var selected_plan = null;

    jQuery('#select-plan-save-btn, .select-plan-welcome-btn').click( function(e) {
        e.preventDefault();
        var btn_el = jQuery(this);
        var btn_action = btn_el.data('action');
        
        jQuery('input[type=radio]:checked').each( function() {
            selected_plan = jQuery(this).val();
        });

        if ( !selected_plan ) {
            alert.error('Please select a plan.');
            return;
        }

        let data = {
            action: 'update_select_plan_action',
            selected_plan: selected_plan,
            product: jQuery('#plans-wrapper').data('product'),
            disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false
        }

        console.log('data 1',data)

        ajax( btn_el, data )
        .done( function(res) {
            if ( res.status == 'success' ) {
                // if welcome
                if ( btn_action == 'next' ) {
                    console.log('redirect.....',res.results.redirect);
                    window.location.href = res.results.redirect;
                }
                if ( btn_action == 'back' ) {
                    let current_step = 7;
                    update_welcome( current_step, btn_el );
                }

            } 
        })
        .fail( function() {

        })
        .always( function() {
            
        });
        
    });
    
}); 