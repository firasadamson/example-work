
const ajax = require('./_ajax');
const alert = require( './_alert' );
const update_welcome = require( './update-welcome' );

jQuery(document).ready( function() {

    var months = [];

    jQuery('#calendar-save-btn, .calendar-welcome-btn, #calendar-next-btn').click( function(e) {
        e.preventDefault();
        
        var btn_el = jQuery(this);
        var btn_action = btn_el.data('action');
        var redirect = btn_el.data('redirect');
        var count = 0;

        jQuery('input[type=checkbox]:checked').each( function() {
                let val = jQuery(this).val();
                months.push(val);
                count++
        });

        if (count == 0) {
            alert.error('Please select at least one month.');
            return;
        } else {
            let data = {
                action: 'update_calendar_action',
                months: months,
                disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false
            }
            ajax( btn_el, data )
            .done( function(res) {
                if ( res.status == 'success' ) {
                    // if has redirect
                    if ( redirect ) {
                        window.location.href = redirect;
                        return;
                    }
                    // if welcome
                    let current_step = 3;
                    update_welcome( current_step, btn_el );
                } 
            })
            .fail( function() {

            })
            .always( function() {
                
            });
        }
    })
}); 