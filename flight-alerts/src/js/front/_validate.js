module.exports = {

    emailFormat: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,

    email : function(email) {
        if ( this.emailFormat.test(email) ) {
            return true;
        }
        return false;
    },

}