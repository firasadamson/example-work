module.exports = {

    text: '',

    pressed : function( el ) {
        console.log('button pressed');
        this.text = el.html();
        el.html('<i class="ticon ticon-circle-o-notch ticon-spin" style="margin-right:5px" aria-hidden="true"></i>') // One moment...
    },

    restored: function( el ) {
        console.log('button restored')
        el.html( this.text );
    }
    
}