jQuery(document).ready( function() {

    // jQuery('.radio-attribute-plan').click( function(e) {
    //     var val = jQuery(this).val();
    //     console.log(val);
    //     // jQuery('#plan option').removeAttr("selected");
    //     jQuery('#plan option[value="'+val+'"]').attr("selected", "selected");
    //     // jQuery('#plan option').filter(function(i, e) { 
    //     //     console.log(jQuery(e).text());
    //     //     console.log('val:--> ',val);
    //     //     return jQuery(e).text() == val
    //     // })
    //     jQuery('#plan').on("change", function() {
    //         console.log('changed');
    //     })
    // });

    if ( jQuery('.single-product .variations_form').length > 0 ) {
        
        jQuery('.variation-radios').addClass('content-box');
        jQuery('.woocommerce-notices-wrapper').remove();

        var current_plan = jQuery('input[name=attribute_plan]:checked').val();
        console.log('current_plan',current_plan);
        setTimeout( function() {
            jQuery('.single_add_to_cart_button').addClass('disabled');
        },500)

        jQuery('.variation-radios label').on( 'click', function() {
            var selected_plan = jQuery(this).find('input[name=attribute_plan]').val();
            console.log('selected',selected_plan);
            if ( current_plan == selected_plan ) {
                console.log('disable time');
                setTimeout( function() {
                    jQuery('.single_add_to_cart_button').addClass('disabled');
                },250)
            } 
        });

        jQuery('.single-product .variation-radios').children('div').each( function() {
            add_radio_buttons(jQuery(this));
        });
    }

    if ( jQuery('.woocommerce-SavedPaymentMethods').length > 0  ) {
        jQuery( 'body' ).on( 'updated_checkout', function() {
            jQuery('.woocommerce-SavedPaymentMethods').children('li').each( function() {
                add_radio_buttons(jQuery(this));
            });
        });
    }

    if ( jQuery('.woocommerce-billing-fields').length > 0 ) {
        jQuery('.woocommerce-billing-fields').addClass('content-box');
        jQuery('.woocommerce-billing-fields').css('margin-bottom','25px');
    }

    if ( jQuery('.woocommerce-checkout-review-order').length > 0 ) {
        jQuery('.woocommerce-checkout-review-order').addClass('content-box');
    }
    
    if ( jQuery('#order_review').length > 0 ) {
        jQuery('#order_review').addClass('content-box');
    }


    function add_radio_buttons(el) {
        var input = el.find('input');
        el.find('input').remove();
        el.find('label').addClass('control').addClass('control-radio');
        el.find('label').prepend('<div class="control_indicator"></div>');
        el.find('label').prepend(input);
    }
    
}); 