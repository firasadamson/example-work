// Require path.
const path = require( 'path' );
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// Configuration object.
module.exports = env => {

	// Create the entry points.
	// One for frontend and one for the admin area.
	const prefix = (env.NODE_ENV == "production" ? '.min' : '');
	console.log('prefix: ' + prefix);

	return {
		entry: {
			frontend : ['./src/js/front-index.js', './src/scss/style.scss'],
			admin : ['./src/js/admin-index.js'],
		},
		// Create the output files.
		// One for each of our entry points.
		output: {
			path: path.resolve(__dirname, 'assets'),
			filename: 'js/[name]'+prefix+'.js'
		},

		// Setup a loader to transpile down the latest and great JavaScript so older browsers
		// can understand it.
		module: {
			rules: [
				{
					// Look for any .js files.
					test: /\.js$/,
					// Exclude the node_modules folder.
					exclude: /node_modules/,
					// Use babel loader to transpile the JS files.
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				},
				{
					test: /\.(sa|sc|c)ss$/,

					// Set loaders to transform files.
					// Loaders are applying from right to left(!)
					// The first loader will be applied after others
					use: [
						{
							// After all CSS loaders we use plugin to do his work.
							// It gets all transformed CSS and extracts it into separate
							// single bundled file
							loader: MiniCssExtractPlugin.loader
						}, 
						{
							// This loader resolves url() and @imports inside CSS
							loader: "css-loader",
						},
						{
							// Then we apply postCSS fixes like autoprefixer and minifying
							loader: "postcss-loader"
						},
						{
							// First we transform SASS to standard CSS
							loader: "sass-loader",
						}
					]
				},
				{
					test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
					use: [
						{
							loader: 'file-loader',
							options: {
							name: '[name].[ext]',
							outputPath: 'fonts/'
							}
						}
					]
				}
			]
		},
		plugins: [
			new MiniCssExtractPlugin({
				filename: 'css/[name]'+prefix+'.css',
			})
		]
	}
}
