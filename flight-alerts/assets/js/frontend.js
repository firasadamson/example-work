/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/front-index.js":
/*!*******************************!*\
  !*** ./src/js/front-index.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/**\n * Frontend entry point.\n *\n * src/front/front-index.js\n */\n__webpack_require__(/*! ./front/_global */ \"./src/js/front/_global.js\");\n\n__webpack_require__(/*! ./front/_range-slider */ \"./src/js/front/_range-slider.js\");\n\n__webpack_require__(/*! ./front/account-email */ \"./src/js/front/account-email.js\");\n\n__webpack_require__(/*! ./front/account-password */ \"./src/js/front/account-password.js\");\n\n__webpack_require__(/*! ./front/account-mobile */ \"./src/js/front/account-mobile.js\");\n\n__webpack_require__(/*! ./front/account-billing */ \"./src/js/front/account-billing.js\");\n\n__webpack_require__(/*! ./front/account-plan */ \"./src/js/front/account-plan.js\");\n\n__webpack_require__(/*! ./front/airports */ \"./src/js/front/airports.js\");\n\n__webpack_require__(/*! ./front/budget */ \"./src/js/front/budget.js\");\n\n__webpack_require__(/*! ./front/calendar */ \"./src/js/front/calendar.js\");\n\n__webpack_require__(/*! ./front/regions */ \"./src/js/front/regions.js\");\n\n__webpack_require__(/*! ./front/select-plan */ \"./src/js/front/select-plan.js\");\n\n__webpack_require__(/*! ./front/hotel-ratings */ \"./src/js/front/hotel-ratings.js\");\n\n__webpack_require__(/*! ./front/notifications */ \"./src/js/front/notifications.js\");\n\n__webpack_require__(/*! ./front/welcome */ \"./src/js/front/welcome.js\");\n\n__webpack_require__(/*! ./front/woocommerce */ \"./src/js/front/woocommerce.js\");\n\n//# sourceURL=webpack:///./src/js/front-index.js?");

/***/ }),

/***/ "./src/js/front/_ajax.js":
/*!*******************************!*\
  !*** ./src/js/front/_ajax.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var button = __webpack_require__(/*! ./_button */ \"./src/js/front/_button.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nmodule.exports = function ajax(el, data) {\n  el.html(button.pressed(el));\n  return jQuery.ajax({\n    url: flight_alerts.ajax_url,\n    type: 'POST',\n    enctype: 'multipart/form-data',\n    data: data\n  }).done(function (response) {\n    if (data.disable_default_done) {\n      return response;\n    }\n\n    if (response.status == 'success') {\n      alert.success('Update successful.');\n    } else {\n      alert.error('Error - ' + response.message);\n    }\n\n    return response;\n  }).fail(function (response) {\n    if (data.disable_default_done) {\n      return response;\n    }\n\n    alert.error('Error - Please try again or contact support.');\n    console.log('response err', response);\n    return response;\n  }).done(function () {\n    if (data.disable_default_done) {\n      return;\n    }\n\n    el.html(button.restored(el));\n  });\n};\n\n//# sourceURL=webpack:///./src/js/front/_ajax.js?");

/***/ }),

/***/ "./src/js/front/_alert.js":
/*!********************************!*\
  !*** ./src/js/front/_alert.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\n  success: function success() {\n    var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';\n    this.alert(text, 'success');\n  },\n  error: function error() {\n    var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';\n    this.alert(text, 'danger');\n  },\n  alert: function alert(text) {\n    var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'primary';\n    this.remove();\n    var html = '';\n    html += '<div class=\"alert alert-' + type + '\" role=\"alert\">';\n    html += text;\n    html += '</div>';\n    jQuery('.site-content').prepend(html);\n    jQuery(\"html, body\").animate({\n      scrollTop: 0\n    }, \"fast\");\n\n    if (type != 'danger') {\n      var THIS = this;\n      setTimeout(function () {\n        THIS.remove();\n      }, 3000);\n    }\n  },\n  remove: function remove() {\n    jQuery('.alert').remove();\n  }\n};\n\n//# sourceURL=webpack:///./src/js/front/_alert.js?");

/***/ }),

/***/ "./src/js/front/_button.js":
/*!*********************************!*\
  !*** ./src/js/front/_button.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\n  text: '',\n  pressed: function pressed(el) {\n    console.log('button pressed');\n    this.text = el.html();\n    el.html('<i class=\"ticon ticon-circle-o-notch ticon-spin\" style=\"margin-right:5px\" aria-hidden=\"true\"></i>'); // One moment...\n  },\n  restored: function restored(el) {\n    console.log('button restored');\n    el.html(this.text);\n  }\n};\n\n//# sourceURL=webpack:///./src/js/front/_button.js?");

/***/ }),

/***/ "./src/js/front/_global.js":
/*!*********************************!*\
  !*** ./src/js/front/_global.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("jQuery(document).ready(function () {\n  jQuery('.copy-to-clipboard').click(function (e) {\n    e.preventDefault();\n    var input_el = jQuery(this).data('element-id');\n    var success_message = jQuery(this).data('success-message');\n    var copyText = document.getElementById(input_el);\n    /* Select the text field */\n\n    copyText.select();\n    copyText.setSelectionRange(0, 99999);\n    /* For mobile devices */\n\n    /* Copy the text inside the text field */\n\n    document.execCommand(\"copy\");\n    /* Alert the copied text */\n\n    if (jQuery('.copy-to-clipboard-message').length > 0) {\n      jQuery('.copy-to-clipboard-message').html(\"\");\n      setTimeout(function () {\n        jQuery('.copy-to-clipboard-message').html(success_message);\n      }, 500);\n    } else {\n      alert(success_message);\n    }\n  });\n\n  if (jQuery('.select-all').length > 0) {\n    check_if_select_all();\n    jQuery('.checkbox').click(function () {\n      check_if_select_all();\n    });\n  }\n\n  jQuery('.select-all').click(function (e) {\n    e.preventDefault();\n    jQuery(this).find('.select-all-checkbox').prop('checked', function (index, attr) {\n      attr = attr == false ? true : false;\n      jQuery('.checkbox').prop('checked', attr);\n      return attr;\n    });\n  });\n  jQuery('.select-none').click(function (e) {\n    e.preventDefault();\n    jQuery('.checkbox').prop('checked', false);\n  });\n\n  function check_if_select_all() {\n    if (jQuery('.checkbox:checked').length == jQuery('.checkbox').length) {\n      jQuery('.select-all-checkbox').prop('checked', true);\n    } else if (jQuery('.checkbox:checked').length < jQuery('.checkbox').length) {\n      jQuery('.select-all-checkbox').prop('checked', false);\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/js/front/_global.js?");

/***/ }),

/***/ "./src/js/front/_range-slider.js":
/*!***************************************!*\
  !*** ./src/js/front/_range-slider.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("if (jQuery('.range-value').length > 0) {\n  update_range_values(); // jQuery(window).on('pageshow', function(){\n  //     console.info('Entered the page!');\n  //     jQuery('.range-value').each( function() {\n  //         jQuery(this).trigger('click');\n  //     });\n  // });\n\n  jQuery(document).on('input change', '.region-range-input', function () {\n    var val = jQuery(this).val();\n    jQuery(this).val(val);\n    var val2 = jQuery(this).val();\n    console.log('val2', val2);\n  });\n}\n\nfunction update_range_values() {\n  jQuery('.range-value').each(function () {\n    var id = jQuery(this).data('id');\n    var type = jQuery(this).data('type');\n    var max = jQuery(this).data('max');\n    var label = type == 'currency' ? '£' : '';\n    console.log('run!!', jQuery('#' + id + '-input').val());\n    console.log('max', max);\n\n    var range = document.getElementById(id + '-input'),\n        rangeV = document.getElementById('range-' + id),\n        setValue = function setValue() {\n      var newValue = Number((range.value - range.min) * 100 / (range.max - range.min));\n      var newPosition = 10 - newValue * 0.2;\n      var range_value = range.value;\n      var output = label + range_value;\n\n      if (max && range_value >= max) {\n        output = label + range_value + '+';\n      }\n\n      rangeV.innerHTML = '<span>' + output + '</span>';\n      rangeV.style.left = \"calc(\".concat(newValue, \"% + (\").concat(newPosition, \"px))\");\n    };\n\n    document.addEventListener(\"DOMContentLoaded\", setValue);\n    range.addEventListener('input', setValue);\n  });\n}\n\n//# sourceURL=webpack:///./src/js/front/_range-slider.js?");

/***/ }),

/***/ "./src/js/front/_validate.js":
/*!***********************************!*\
  !*** ./src/js/front/_validate.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\n  emailFormat: /^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$/,\n  email: function email(_email) {\n    if (this.emailFormat.test(_email)) {\n      return true;\n    }\n\n    return false;\n  }\n};\n\n//# sourceURL=webpack:///./src/js/front/_validate.js?");

/***/ }),

/***/ "./src/js/front/account-billing.js":
/*!*****************************************!*\
  !*** ./src/js/front/account-billing.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\njQuery(document).ready(function () {\n  jQuery('#account-billing-save-btn').click(function (e) {\n    e.preventDefault();\n    console.log('click billing save btn');\n    var billing = [];\n    jQuery('.billing-input').each(function () {\n      var key = jQuery(this).attr('id');\n      var val = jQuery(this).val();\n      billing.push({\n        key: key,\n        val: val\n      });\n    });\n    var data = {\n      action: 'update_account_billing_action',\n      billing: billing\n    };\n    ajax(jQuery(this), data);\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/account-billing.js?");

/***/ }),

/***/ "./src/js/front/account-email.js":
/*!***************************************!*\
  !*** ./src/js/front/account-email.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar validate = __webpack_require__(/*! ./_validate */ \"./src/js/front/_validate.js\");\n\njQuery(document).ready(function () {\n  jQuery('#account-email-save-btn').click(function (e) {\n    var current_email = jQuery('#current-email').html();\n    var new_email = jQuery('#new-email').val();\n    var confirm_email = jQuery('#confirm-email').val();\n    var password = jQuery('#password').val();\n\n    if (!new_email || !confirm_email || !password) {\n      alert.error(\"Please complete all fields.\");\n      return;\n    }\n\n    if (new_email == current_email) {\n      alert.error(\"You have entered the same email address as your current one. Please enter a different one.\");\n      return;\n    }\n\n    if (!validate.email(new_email)) {\n      alert.error(\"You have not entered a valid email address. Please try again.\");\n      return;\n    }\n\n    if (new_email != confirm_email) {\n      alert.error(\"The email address you entered did not match. Please try again.\");\n      return;\n    }\n\n    var data = {\n      action: 'update_account_email_action',\n      new_email: new_email,\n      confirm_email: confirm_email,\n      password: password\n    };\n    ajax(jQuery(this), data).done(function (res) {\n      if (res.status == 'success') {\n        jQuery('#current-email').html(new_email);\n        jQuery('#new-email').val('');\n        jQuery('#confirm-email').val('');\n        jQuery('#password').val('');\n      }\n    });\n  }); // check if match\n  // if all good, ajax it\n  // once processed, clear form and just show current email address at the top?\n});\n\n//# sourceURL=webpack:///./src/js/front/account-email.js?");

/***/ }),

/***/ "./src/js/front/account-mobile.js":
/*!****************************************!*\
  !*** ./src/js/front/account-mobile.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\njQuery(document).ready(function () {\n  if (jQuery('#country_code').length > 0) {\n    if (jQuery('#country_code').val() == \"\") {\n      jQuery('#country_code').val('GB');\n    }\n  }\n\n  jQuery('#mobile').on(\"keyup\", function () {\n    this.value = this.value.replace(/[^0-9\\.]/g, '');\n\n    if (/^0/.test(this.value)) {\n      this.value = this.value.replace(/^0/, \"\");\n    }\n  });\n  jQuery('.country-code-dropdown').on('change', function () {\n    jQuery('#country-code-output').html(jQuery(this).val());\n  });\n  jQuery('#account-mobile-save-btn').click(function (e) {\n    e.preventDefault();\n    var mobile = jQuery('#mobile').val();\n    var country_code = jQuery('#country_code').val();\n\n    if (!country_code || !mobile) {\n      alert.error(\"Please ensure you select a country code and enter your mobile number.\");\n      return;\n    }\n\n    var data = {\n      action: 'update_account_mobile_action',\n      mobile: mobile,\n      mobile_country_code: country_code\n    };\n    ajax(jQuery(this), data);\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/account-mobile.js?");

/***/ }),

/***/ "./src/js/front/account-password.js":
/*!******************************************!*\
  !*** ./src/js/front/account-password.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\njQuery(document).ready(function () {\n  jQuery('#account-password-save-btn').click(function (e) {\n    e.preventDefault();\n    var current_password = jQuery('#current-password').val();\n    var new_password = jQuery('#new-password').val();\n    var confirm_password = jQuery('#confirm-password').val();\n\n    if (!current_password || !new_password || !confirm_password) {\n      alert.error(\"Please complete all fields.\");\n      return;\n    }\n\n    if (new_password.length < 6) {\n      alert.error(\"Please enter at least 6 characters for your password.\");\n      return;\n    }\n\n    if (new_password != confirm_password) {\n      alert.error(\"Your password did not match. Please try again.\");\n      return;\n    }\n\n    var data = {\n      action: 'update_account_password_action',\n      current_password: current_password,\n      new_password: new_password,\n      confirm_password: confirm_password\n    };\n    ajax(jQuery(this), data).done(function (res) {\n      if (res.status == 'success') {\n        jQuery('#current-password').val('');\n        jQuery('#new-password').val('');\n        jQuery('#confirm-password').val('');\n      }\n    });\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/account-password.js?");

/***/ }),

/***/ "./src/js/front/account-plan.js":
/*!**************************************!*\
  !*** ./src/js/front/account-plan.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\njQuery(document).ready(function () {\n  console.log('account email');\n  jQuery('#account-email-save-btn').click(function (e) {\n    e.preventDefault();\n    console.log('click email save btn');\n  });\n\n  if (jQuery('.change_payment_method').length > 0) {\n    jQuery('.change_payment_method').html('Change payment method');\n  }\n});\n\n//# sourceURL=webpack:///./src/js/front/account-plan.js?");

/***/ }),

/***/ "./src/js/front/airports.js":
/*!**********************************!*\
  !*** ./src/js/front/airports.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar update_welcome = __webpack_require__(/*! ./update-welcome */ \"./src/js/front/update-welcome.js\");\n\njQuery(document).ready(function () {\n  jQuery('.airport-country-title').click(function (e) {\n    e.preventDefault();\n    jQuery(this).parent().children('.airports-wrapper').toggle();\n    jQuery(this).find('.icon').toggleClass(\"ticon-chevron-down\");\n    jQuery(this).find('.icon').toggleClass(\"ticon-chevron-up\");\n  });\n  var airports = [];\n\n  if (jQuery('#welcome-airports-back-btn').length > 0) {\n    jQuery('#welcome-airports-back-btn').remove();\n  }\n\n  jQuery('#airports-save-btn, .airports-welcome-btn, #airports-next-btn').click(function (e) {\n    e.preventDefault();\n    var btn_el = jQuery(this);\n    var btn_action = btn_el.data('action');\n    var redirect = btn_el.data('redirect');\n    var count = 0;\n    jQuery('input[type=checkbox]:checked').each(function () {\n      var val = jQuery(this).val();\n      airports.push(val);\n      count++;\n    });\n\n    if (count == 0) {\n      alert.error('Please select at least one airport.');\n      return;\n    } else {\n      var data = {\n        action: 'update_airports_action',\n        airports: airports,\n        disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false\n      };\n      ajax(btn_el, data).done(function (res) {\n        if (res.status == 'success') {\n          // if has redirect\n          if (redirect) {\n            window.location.href = redirect;\n            return;\n          } // if welcome\n\n\n          var current_step = 1;\n          update_welcome(current_step, btn_el);\n        }\n      }).fail(function () {}).always(function () {});\n    }\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/airports.js?");

/***/ }),

/***/ "./src/js/front/budget.js":
/*!********************************!*\
  !*** ./src/js/front/budget.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar update_welcome = __webpack_require__(/*! ./update-welcome */ \"./src/js/front/update-welcome.js\");\n\njQuery(document).ready(function () {\n  console.log('budget'); // jQuery( '.region-range-input' ).on('input change', function(e) {\n  //     let val = jQuery(this).val();\n  //     let key = jQuery(this).attr('data-region-key');\n  //     if (val == 1000) val = '1000+';\n  //     console.log('range...', key, val );\n  //     jQuery('#'+key+'-output').html(val);\n  // });\n\n  jQuery('#budget-save-btn, .budget-welcome-btn, #budget-next-btn').click(function (e) {\n    e.preventDefault();\n    ;\n    var btn_el = jQuery(this);\n    var btn_action = btn_el.data('action');\n    var redirect = btn_el.data('redirect');\n    var budgets = [];\n    jQuery('.region-range-input').each(function (e) {\n      var key = jQuery(this).data('key');\n      var val = jQuery(this).val();\n      budgets.push({\n        key: key,\n        val: val\n      });\n    });\n    var data = {\n      action: 'update_budget_action',\n      budgets: budgets,\n      disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false\n    };\n    console.log(data);\n    ajax(btn_el, data).done(function (res) {\n      if (res.status == 'success') {\n        // if has redirect\n        if (redirect) {\n          window.location.href = redirect;\n          return;\n        } // if welcome\n\n\n        var current_step = 4;\n        update_welcome(current_step, btn_el);\n      }\n    }).fail(function () {}).always(function () {});\n  }); // jQuery('input[type=\"range\"]').rangeslider({\n  //     polyfill: false, \n  //     // Default CSS classes\n  //     rangeClass: 'rangeslider',\n  //     disabledClass: 'rangeslider--disabled',\n  //     horizontalClass: 'rangeslider--horizontal',\n  //     verticalClass: 'rangeslider--vertical',\n  //     fillClass: 'rangeslider__fill',\n  //     handleClass: 'rangeslider__handle',\n  //     // Callback function\n  //     onInit: function() {\n  //         console.log('on init');\n  //     },\n  //     // Callback function\n  //     onSlide: function(position, value) {\n  //         console.log('on slide end',value)\n  //         if (value == 1000) value = '1000+';\n  //         update_range( jQuery(this), value );\n  //         // jQuery('.range-output').html(value)\n  //     },\n  //     // Callback function\n  //     onSlideEnd: function(position, value) {\n  //         console.log('on slide end', value)\n  //         if (value == 1000) value = '1000+';\n  //         update_range( jQuery(this),value );\n  //         // jQuery('.range-output').html(value)\n  //     }\n  // });\n  // function update_range(el, val) {\n  //     // let val = el.val();\n  //     let key = el.attr('data-region-key');\n  //     console.log('range...', key, val );\n  //     jQuery('#'+key+'-output').html(val);\n  // }\n});\n\n//# sourceURL=webpack:///./src/js/front/budget.js?");

/***/ }),

/***/ "./src/js/front/calendar.js":
/*!**********************************!*\
  !*** ./src/js/front/calendar.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar update_welcome = __webpack_require__(/*! ./update-welcome */ \"./src/js/front/update-welcome.js\");\n\njQuery(document).ready(function () {\n  var months = [];\n  jQuery('#calendar-save-btn, .calendar-welcome-btn, #calendar-next-btn').click(function (e) {\n    e.preventDefault();\n    var btn_el = jQuery(this);\n    var btn_action = btn_el.data('action');\n    var redirect = btn_el.data('redirect');\n    var count = 0;\n    jQuery('input[type=checkbox]:checked').each(function () {\n      var val = jQuery(this).val();\n      months.push(val);\n      count++;\n    });\n\n    if (count == 0) {\n      alert.error('Please select at least one month.');\n      return;\n    } else {\n      var data = {\n        action: 'update_calendar_action',\n        months: months,\n        disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false\n      };\n      ajax(btn_el, data).done(function (res) {\n        if (res.status == 'success') {\n          // if has redirect\n          if (redirect) {\n            window.location.href = redirect;\n            return;\n          } // if welcome\n\n\n          var current_step = 3;\n          update_welcome(current_step, btn_el);\n        }\n      }).fail(function () {}).always(function () {});\n    }\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/calendar.js?");

/***/ }),

/***/ "./src/js/front/hotel-ratings.js":
/*!***************************************!*\
  !*** ./src/js/front/hotel-ratings.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar update_welcome = __webpack_require__(/*! ./update-welcome */ \"./src/js/front/update-welcome.js\");\n\njQuery(document).ready(function () {\n  var hotel_ratings = [];\n  jQuery('#hotel-ratings-save-btn, .hotel-ratings-welcome-btn, #hotel-ratings-next-btn').click(function (e) {\n    e.preventDefault();\n    var btn_el = jQuery(this);\n    var btn_action = btn_el.data('action');\n    var redirect = btn_el.data('redirect');\n    var count = 0;\n    jQuery('input[type=checkbox]:checked').each(function () {\n      var val = jQuery(this).val();\n      hotel_ratings.push(val);\n      count++;\n    });\n\n    if (count == 0) {\n      alert.error('Please select a rating.');\n      return;\n    } else {\n      var data = {\n        action: 'update_hotel_ratings_action',\n        hotel_ratings: hotel_ratings,\n        disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false\n      };\n      ajax(btn_el, data).done(function (res) {\n        if (res.status == 'success') {\n          // if has redirect\n          if (redirect) {\n            window.location.href = redirect;\n            return;\n          } // if welcome\n\n\n          var current_step = 5;\n          update_welcome(current_step, btn_el);\n        }\n      }).fail(function () {}).always(function () {});\n    }\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/hotel-ratings.js?");

/***/ }),

/***/ "./src/js/front/notifications.js":
/*!***************************************!*\
  !*** ./src/js/front/notifications.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar update_welcome = __webpack_require__(/*! ./update-welcome */ \"./src/js/front/update-welcome.js\");\n\njQuery(document).ready(function () {\n  // var notifications = [];\n  if (jQuery('#country_code').length > 0) {\n    if (jQuery('#country_code').val() == \"\") {\n      jQuery('#country_code').val('GB');\n    }\n  }\n\n  jQuery('.country-code-dropdown').on('change', function () {\n    jQuery('#country-code-output').html(jQuery(this).val());\n  }); // jQuery( '.user-notifications-range-input' ).on('input change', function(e) {\n  //     let val = jQuery(this).val();\n  //     console.log('user-notifications-range-input range...', val );\n  //     jQuery('#user-notifications-output').html(val);\n  // });\n\n  jQuery('#notifications-save-btn, .notifications-welcome-btn').click(function (e) {\n    e.preventDefault();\n    var btn_el = jQuery(this);\n    var btn_action = btn_el.data('action');\n    var mobile = jQuery('#mobile').val();\n    var country_code = jQuery('#country_code').val(); // if ( !country_code || !mobile ) {\n    //     alert.error(\"Please ensure you select a country code and enter your mobile number.\");\n    //     return;\n    // }\n\n    var data = {\n      action: 'update_notifications_action',\n      notifications: jQuery('#user-notifications-input').val(),\n      mobile: mobile,\n      mobile_country_code: country_code,\n      disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false\n    };\n    ajax(btn_el, data).done(function (res) {\n      if (res.status == 'success') {\n        // if welcome\n        var current_step = 6;\n        update_welcome(current_step, btn_el);\n      }\n    }).fail(function () {}).always(function () {});\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/notifications.js?");

/***/ }),

/***/ "./src/js/front/regions.js":
/*!*********************************!*\
  !*** ./src/js/front/regions.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar update_welcome = __webpack_require__(/*! ./update-welcome */ \"./src/js/front/update-welcome.js\");\n\njQuery(document).ready(function () {\n  var regions = [];\n  jQuery('.regions-checkbox').click(function () {\n    var val = jQuery(this).val();\n\n    if (jQuery(this).is(':checked')) {\n      jQuery('#map-' + val).show();\n    } else {\n      jQuery('#map-' + val).hide();\n    }\n  });\n  jQuery('.region-select-all').click(function (e) {\n    e.preventDefault();\n\n    if (jQuery('.checkbox:checked').length == jQuery('.checkbox').length) {\n      jQuery('.map-img').show();\n    } else if (jQuery('.checkbox:checked').length < jQuery('.checkbox').length) {\n      jQuery('.map-img').hide();\n    }\n  });\n  jQuery('#regions-save-btn, .regions-welcome-btn, #regions-next-btn').click(function (e) {\n    e.preventDefault();\n    var btn_el = jQuery(this);\n    var btn_action = btn_el.data('action');\n    var redirect = btn_el.data('redirect');\n    var count = 0;\n    jQuery('input[type=checkbox]:checked').each(function () {\n      var val = jQuery(this).val();\n      regions.push(val);\n      count++;\n    });\n\n    if (count == 0) {\n      alert.error('Please select at least one region.');\n      return;\n    } else {\n      var data = {\n        action: 'update_regions_action',\n        regions: regions,\n        disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false\n      };\n      ajax(btn_el, data).done(function (res) {\n        if (res.status == 'success') {\n          // if has redirect\n          if (redirect) {\n            window.location.href = redirect;\n            return;\n          } // if welcome\n\n\n          var current_step = 2;\n          update_welcome(current_step, btn_el);\n        }\n      }).fail(function () {}).always(function () {});\n    }\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/regions.js?");

/***/ }),

/***/ "./src/js/front/select-plan.js":
/*!*************************************!*\
  !*** ./src/js/front/select-plan.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nvar alert = __webpack_require__(/*! ./_alert */ \"./src/js/front/_alert.js\");\n\nvar update_welcome = __webpack_require__(/*! ./update-welcome */ \"./src/js/front/update-welcome.js\");\n\njQuery(document).ready(function () {\n  var selected_plan = null;\n  jQuery('#select-plan-save-btn, .select-plan-welcome-btn').click(function (e) {\n    e.preventDefault();\n    var btn_el = jQuery(this);\n    var btn_action = btn_el.data('action');\n    jQuery('input[type=radio]:checked').each(function () {\n      selected_plan = jQuery(this).val();\n    });\n\n    if (!selected_plan) {\n      alert.error('Please select a plan.');\n      return;\n    }\n\n    var data = {\n      action: 'update_select_plan_action',\n      selected_plan: selected_plan,\n      product: jQuery('#plans-wrapper').data('product'),\n      disable_default_done: btn_action == 'next' || btn_action == 'back' ? true : false\n    };\n    console.log('data 1', data);\n    ajax(btn_el, data).done(function (res) {\n      if (res.status == 'success') {\n        // if welcome\n        if (btn_action == 'next') {\n          console.log('redirect.....', res.results.redirect);\n          window.location.href = res.results.redirect;\n        }\n\n        if (btn_action == 'back') {\n          var current_step = 7;\n          update_welcome(current_step, btn_el);\n        }\n      }\n    }).fail(function () {}).always(function () {});\n  });\n});\n\n//# sourceURL=webpack:///./src/js/front/select-plan.js?");

/***/ }),

/***/ "./src/js/front/update-welcome.js":
/*!****************************************!*\
  !*** ./src/js/front/update-welcome.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var ajax = __webpack_require__(/*! ./_ajax */ \"./src/js/front/_ajax.js\");\n\nmodule.exports = function update_welcome(current_step, btn_el) {\n  var btn_action = btn_el.data('action');\n\n  if (btn_action == 'next' || btn_action == 'back') {\n    var prev_step = current_step <= 1 ? 1 : current_step - 1;\n    var next_step = current_step + 1;\n    var data = {\n      action: 'update_welcome_setup_action',\n      next_step: btn_action == 'back' ? prev_step : next_step,\n      disable_default_done: true\n    };\n    ajax(btn_el, data).done(function () {\n      // location.reload();\n      window.location.replace('/signup/welcome/');\n    });\n  }\n};\n\n//# sourceURL=webpack:///./src/js/front/update-welcome.js?");

/***/ }),

/***/ "./src/js/front/welcome.js":
/*!*********************************!*\
  !*** ./src/js/front/welcome.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("jQuery(document).ready(function () {\n  // if welcome page\n  if (jQuery('.page-id-122').length > 0) {\n    // jQuery(window).load(function () {\n    //     // jQuery('html, body').animate({scrollTop: '0px'}, 0);\n    //     setTimeout( function() {\n    //         jQuery('html, body').animate({scrollTop: '0px'}, 0);\n    //     },1);\n    // });\n    jQuery(window).on('beforeunload', function () {\n      jQuery(window).scrollTop(0);\n    });\n  }\n});\n\n//# sourceURL=webpack:///./src/js/front/welcome.js?");

/***/ }),

/***/ "./src/js/front/woocommerce.js":
/*!*************************************!*\
  !*** ./src/js/front/woocommerce.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("jQuery(document).ready(function () {\n  // jQuery('.radio-attribute-plan').click( function(e) {\n  //     var val = jQuery(this).val();\n  //     console.log(val);\n  //     // jQuery('#plan option').removeAttr(\"selected\");\n  //     jQuery('#plan option[value=\"'+val+'\"]').attr(\"selected\", \"selected\");\n  //     // jQuery('#plan option').filter(function(i, e) { \n  //     //     console.log(jQuery(e).text());\n  //     //     console.log('val:--> ',val);\n  //     //     return jQuery(e).text() == val\n  //     // })\n  //     jQuery('#plan').on(\"change\", function() {\n  //         console.log('changed');\n  //     })\n  // });\n  if (jQuery('.single-product .variations_form').length > 0) {\n    jQuery('.variation-radios').addClass('content-box');\n    jQuery('.woocommerce-notices-wrapper').remove();\n    var current_plan = jQuery('input[name=attribute_plan]:checked').val();\n    console.log('current_plan', current_plan);\n    setTimeout(function () {\n      jQuery('.single_add_to_cart_button').addClass('disabled');\n    }, 500);\n    jQuery('.variation-radios label').on('click', function () {\n      var selected_plan = jQuery(this).find('input[name=attribute_plan]').val();\n      console.log('selected', selected_plan);\n\n      if (current_plan == selected_plan) {\n        console.log('disable time');\n        setTimeout(function () {\n          jQuery('.single_add_to_cart_button').addClass('disabled');\n        }, 250);\n      }\n    });\n    jQuery('.single-product .variation-radios').children('div').each(function () {\n      add_radio_buttons(jQuery(this));\n    });\n  }\n\n  if (jQuery('.woocommerce-SavedPaymentMethods').length > 0) {\n    jQuery('body').on('updated_checkout', function () {\n      jQuery('.woocommerce-SavedPaymentMethods').children('li').each(function () {\n        add_radio_buttons(jQuery(this));\n      });\n    });\n  }\n\n  if (jQuery('.woocommerce-billing-fields').length > 0) {\n    jQuery('.woocommerce-billing-fields').addClass('content-box');\n    jQuery('.woocommerce-billing-fields').css('margin-bottom', '25px');\n  }\n\n  if (jQuery('.woocommerce-checkout-review-order').length > 0) {\n    jQuery('.woocommerce-checkout-review-order').addClass('content-box');\n  }\n\n  if (jQuery('#order_review').length > 0) {\n    jQuery('#order_review').addClass('content-box');\n  }\n\n  function add_radio_buttons(el) {\n    var input = el.find('input');\n    el.find('input').remove();\n    el.find('label').addClass('control').addClass('control-radio');\n    el.find('label').prepend('<div class=\"control_indicator\"></div>');\n    el.find('label').prepend(input);\n  }\n});\n\n//# sourceURL=webpack:///./src/js/front/woocommerce.js?");

/***/ }),

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/scss/style.scss?");

/***/ }),

/***/ 0:
/*!***********************************************************!*\
  !*** multi ./src/js/front-index.js ./src/scss/style.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./src/js/front-index.js */\"./src/js/front-index.js\");\nmodule.exports = __webpack_require__(/*! ./src/scss/style.scss */\"./src/scss/style.scss\");\n\n\n//# sourceURL=webpack:///multi_./src/js/front-index.js_./src/scss/style.scss?");

/***/ })

/******/ });