<?php
/**
 * Woocommerce class file.
 *
 * @package WordPress Plugin Template/Woocommerce
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Woocommerce class.
 */
class Flight_Alerts_Woocommerce {

	/**
	 * The single instance of Flight_Alerts_Woocommerce.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin woocommerce.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available woocommerce for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $woocommerce = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = 'wpt_';
		$this->assets_url = $this->parent->assets_url;

		// add_filter( 'wc_add_to_cart_message_html', '__return_false' );
		
		// add_filter( 'woocommerce_enable_order_notes_field', '__return_false', 9999 );
		// add_filter('woocommerce_checkout_fields', array ($this, 'override_checkout_fields' ));
		add_filter( 'woocommerce_checkout_fields' , array ($this, 'simplify_checkout_virtual' ) );

		add_filter( 'gettext', array( $this, 'wc_billing_field_strings' ), 20, 3 );
		// add_filter( 'woocommerce_get_order_item_totals', array( $this, 'remove_from_orders_total_lines' ), 100, 1 );

		// add_action( 'woocommerce_after_checkout_billing_form', 'woocommerce_order_review', 20 );
		remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );

		
		add_action( 'woocommerce_thankyou', array($this, 'first_time_purchase_send_email'), 10, 1 ); 
		add_action( 'woocommerce_thankyou', array($this, 'assign_role'), 999, 1 ); 
		
		// add_action( 'wp', array( $this, 'cancelled_membership'), 1, 1);
		add_action( 'woocommerce_scheduled_subscription_end_of_prepaid_term', array( $this, 'cancelled_membership'), 1, 1);

		add_action( 'fa_after_product_title', array( $this, 'add_text_after_product_title'));
		add_action( 'woocommerce_after_checkout_form', array( $this, 'checkout_bottom_text'), 10 );


		add_filter( 'gettext', array($this, 'woocommerce_rename_coupon_field_on_cart'), 10, 3 );
		add_filter( 'gettext', array($this, 'woocommerce_rename_coupon_field_on_cart'), 10, 3 );
		add_filter('woocommerce_coupon_error', array($this, 'rename_coupon_label'), 10, 3);
		add_filter('woocommerce_coupon_message', array($this, 'rename_coupon_label'), 10, 3);
		add_filter('woocommerce_cart_totals_coupon_label', array($this, 'rename_coupon_label'),10, 1);
		add_filter( 'woocommerce_checkout_coupon_message', array($this, 'woocommerce_rename_coupon_message_on_checkout' ));

		// add_filter( 'wcml_product_custom_prices', array($this,'change_product_custom_prices'), 10, 3 );
 
	}

	// function change_product_custom_prices( $custom_prices, $product_id, $currency ) {
 
	// 	// Change all custom prices in USD to 99 
	// 	if ( $product_id  == FA_SUBSCRIPTION_PRODUCT_ID ) {
	// 		if ( 'GBP' == $currency ) {
	// 			$custom_prices[ '_custom_variation_regular_price' ] = 101;
	// 			// $custom_prices[ '_custom_variation_regular_price' ] = 102;
	// 			// _custom_variation_regular_price[USD][112]
	// 		}
	
	// 		if ( 'EUR' == $currency ) {
	// 			$custom_prices[ '_custom_variation_regular_price' ] = 201;
	// 			// $custom_prices[ '_custom_variation_regular_price' ][113] = 202;
	// 			// _custom_variation_regular_price[USD][112]
	// 		}
	// 	}

	
	// 	return $custom_prices; 
	// }

	function add_text_after_product_title() {
		if ( isset($_GET['switch-subscription']) ) {
			$text = 'Select a plan that you want to change to.';
		} else {
			$text = 'Select a plan below.';
		}
		echo '<div>' . $text . '</div>';
	}

	function billing_text() {
		$html = '<div style="margin-top:45px; line-height:125%; text-align:center">';
		$html .= '<p><small>Your card will not be charged during your trial period. After 30 days, you will be billed recurring monthly for the cost of your selected membership option. Cancel any time during your trial and you will not be charged. If you for any reason forgot to cancel your free trial subscription and you let us know within 14 days of being billed then we will happily refund you, no questions asked.</small></p>';
		// $html .= '<p><small>Remember that for every new RareFare member we plant a tree... it’s all part of our mission to do our bit for the environment. Here at RareFare, we know that you won’t be able to resist our incredible flight deals, so to help you do your bit for the environment too we’ll plant a tree for every new RareFare member, to help you offset your Carbon Footprint and minimise our effect on the environment.</small></p>';  
		$html .= '</div>';

		return $html;
	}

	function checkout_bottom_text() {
		echo $this->billing_text();
	}

	// function remove_cart_item_before_add_to_cart( $passed, $product_id, $quantity ) {
	// 	if( ! WC()->cart->is_empty() )
	// 		WC()->cart->empty_cart();
	// 	return $passed;
	// }

	function simplify_checkout_virtual( $fields ) {
		
		$only_virtual = true;
		
		foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		// Check if there are non-virtual products
			if ( ! $cart_item['data']->is_virtual() ) $only_virtual = false;   
		}

		if( $only_virtual ) {
			unset($fields['billing']['billing_company']);
			unset($fields['billing']['billing_address_1']);
			unset($fields['billing']['billing_address_2']);
			unset($fields['billing']['billing_city']);
			unset($fields['billing']['billing_postcode']);
			unset($fields['billing']['billing_country']);
			unset($fields['billing']['billing_state']);
			unset($fields['billing']['billing_phone']);
			add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );
		}

		$fields['billing']['billing_email'] = [
			// 'label' => 'Email Address',
			'required'  => false,
			'custom_attributes' => [
				'disabled' => 'disabled', 
			],
			// 'type' => 'hidden'
		];
			
		return $fields;
	}

	function remove_from_orders_total_lines( $totals ) {
		unset($totals['cart_subtotal'] );
		return $totals;
	}

	/**
	 * Initialise woocommerce
	 *
	 * @return void
	 */
	public function override_checkout_fields($fields) {

		// $fields['billing']['billing_phone']['required'] = false;

		// unset( $fields['billing']['billing_address_1'] );
		// unset( $fields['billing']['billing_address_2'] );
		// unset( $fields['billing']['billing_city'] );
		// unset( $fields['billing']['billing_state'] );
		// unset( $fields['billing']['billing_phone'] );
		// unset( $fields['order']['order_comments'] );

		unset($fields['billing']['billing_first_name']);
		unset($fields['billing']['billing_last_name']);
		unset($fields['billing']['billing_company']);
		unset($fields['billing']['billing_address_1']);
		unset($fields['billing']['billing_address_2']);
		unset($fields['billing']['billing_city']);
		unset($fields['billing']['billing_postcode']);
		unset($fields['billing']['billing_country']);
		unset($fields['billing']['billing_state']);
		unset($fields['billing']['billing_phone']);
		unset($fields['order']['order_comments']);
		unset($fields['billing']['billing_email']);
		unset($fields['account']['account_username']);
		unset($fields['account']['account_password']);
		unset($fields['account']['account_password-2']);

		return $fields;
	
	}	

	function wc_billing_field_strings( $translated_text, $text, $domain ) {
		switch ( $translated_text ) {
			case 'Billing details' :
				$translated_text = __( 'Final Step', 'woocommerce' );
				break;
		}
		return $translated_text;
	}

	function get_latest_subscription() {

		$user_id = get_current_user_id();

		$customer_orders = get_posts(
			array(
				'numberposts' => 1,
				'meta_key' => '_customer_user',
				'orderby' => 'date',
				'order' => 'DESC',
				'meta_value' => $user_id,
				'post_type' => 'shop_subscription',
				'post_status' => array_keys( wc_get_order_statuses() ),  
				'post_status' => array('wc-processing','wc-active','wc-completed', 'wc-cancelled', 'wc-pending-cancel','wc-on-hold'),
			)
		);
		
		// 
		if (!$customer_orders) return; 
		
		foreach ($customer_orders as $customer_order) {
		
			// var_dump($customer_order);
			// if ($customer_order->parent_id == 0)
			// $orderq = wc_get÷_order($customer_order);
			// $order_id = $orderq->get_id();
			$order_id = $customer_order->post_parent; 
		
		
			$subscriptions = wcs_get_subscriptions([
				'subscriptions_per_page' => -1, 
				'customer_id' => $user_id,
				'order_id' => $order_id
			]);
			
			// Loop through subscriptions protected objects
			foreach ( $subscriptions as $subscription ) {
				// Unprotected data in an accessible array
				if ($order_id == $subscription->parent_id) {
					// $data = $subscription->get_data();
					return $subscription;
					break;
				}
			}
		
			// $wc = new WC_Subscription($order_id);
			break;
		}

		return false;
	}

	function change_plan_redirect_url($data = null) {
		
		$url = site_url().'/product/rarefare-membership/';

		if (!$data) {
			$subscription = $this->get_latest_subscription();
			if (!$subscription) return $url;
			$data = $subscription->get_data();
		}

		if ($data) {
			foreach($data['line_items'] as $item_id => $item) {
				break;
			}
			$url .= '?switch-subscription='.$data['id'].'&item='.$item_id.'&_wcsnonce='.wp_create_nonce();
		}
		
		return $url;
	
	}


	function change_plan_redirect() {
		$url = $this->change_plan_redirect_url();
		return '<script>window.location.replace("'.$url.'");</script>';
	}

		
	// define the woocommerce_thankyou callback 
	function first_time_purchase_send_email( $order_id ) { 
		
		$order = wc_get_order($order_id);

		if ( $order->has_status('processing') || $order->has_status('completed') ) {
			
			$user_id = get_current_user_id();
			$user_info = get_userdata($user_id);
			$first_name = $user_info->first_name;
			$email_address = $user_info->user_email;

			// only do the following if wc-processing or wc-success

			// check if sent_welcome_email exists.
			$sent_welcome_email = get_user_meta( $user_id, 'sent_welcome_email', true );

			// if yes, do not send
			if ($sent_welcome_email) {
				return;
			}

			$args = [ 'first_name' => $first_name ];
			// if no, send
			$email_message = $this->parent->email->welcome_email_template($args);
			// echo $email_message;
			$email_args = array(
				'to' => $email_address,
				'subject' => 'Welcome to RareFare',
				'message' => $email_message
			);
			$this->parent->email->send_email( $email_args );

			// also set to sent
			update_user_meta( $user_id, 'sent_welcome_email', true );
		}

	}

	function assign_role( $order_id ) { 

		// for display arrays
		// ini_set("xdebug.var_display_max_children", '-1');
		// ini_set("xdebug.var_display_max_data", '-1');
		// ini_set("xdebug.var_display_max_depth", '-1');

		$order = wc_get_order($order_id);

		if ( $order->has_status('processing') || $order->has_status('completed') ) {
			
			// remove all roles first?
			$user_id = get_current_user_id();

			UM()->user()->remove_cache( $user_id );
			$user = new WP_User($user_id); 
			$role = UM()->user()->get_role();

			// we want to change the role of current user
			
			if ($role != "administrator") {
				$user->remove_role('um_premium');
				$user->remove_role('um_basic');
			}
			
			// let's get the order item_type
			// if premium, assign role
			if ($role != "administrator") {
				foreach( $order->get_items() as $item_id => $item ){
					if (strpos( strtolower($item->get_name()), 'premium') !== false) {
						$user->set_role('um_premium');
						$user->remove_role('subscriber');
					} else {
						$user->set_role('um_basic');
						$user->remove_role('subscriber');
					}
				}
			}

			wp_redirect( site_url() . '/thankyou');

		}

		// die();

	}

	function cancelled_membership( $subscription_id ) {
		// using $subscription_id  get subscription 
		$subscription = wcs_get_subscription( $subscription_id );
		// then get user_id
		$user_id = $subscription->data['customer_id'];
		// the remove all roles and downgrade
		$this->remove_all_plan_roles($user_id);
	}

	function remove_all_plan_roles($user_id) {

		if (!$user_id) $user_id = get_current_user_id();

		UM()->user()->remove_cache( $user_id );
		$user = new WP_User($user_id); 
		$role = UM()->user()->get_role();

		// we want to change the role of current user
		
		if ($role != "administrator") {
			$user->remove_role('um_premium');
			$user->remove_role('um_basic');
			$user->set_role('subscriber');
		}

	}

	function woocommerce_rename_coupon_field_on_cart( $translated_text, $text, $text_domain ) {
	// bail if not modifying frontend woocommerce text
	if ( is_admin() || 'woocommerce' !== $text_domain ) {
		return $translated_text;
	}
	if ( 'Coupon:' === $text ) {
		$translated_text = 'Discount Code:';
	}

	if ('Coupon has been removed.' === $text){
		$translated_text = 'Discount code has been removed.';
	}

	if ( 'Apply coupon' === $text ) {
		$translated_text = 'Apply Code';
	}

	if ( 'Coupon code' === $text ) {
		$translated_text = 'Discount Code';
	
	} 

	return $translated_text;
}


// rename the "Have a Coupon?" message on the checkout page
function woocommerce_rename_coupon_message_on_checkout() {
	return 'Have a discount code?' . ' ' . __( '<a href="#" class="showcoupon">Click here to enter your code</a>', 'woocommerce' ) . '';
}


function rename_coupon_label($err, $err_code=null, $something=null){

	$err = str_ireplace("Coupon","Discount Code ",$err);

	return $err;
}


	/**
	 * Main Flight_Alerts_Woocommerce Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Woocommerce is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Woocommerce instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
