<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
if (!$user_id) $this->auth_failed();

$budgets = $_POST['budgets'];
$arr = array();
foreach( $budgets as $budget) {
    $arr[ $budget['key'] ] = $budget['val'];
}

update_user_meta($user_id, 'user_budgets', $arr);

$res = array( 'budgets' => $budgets );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - budget',
            'results' => $res
        )
    )
);