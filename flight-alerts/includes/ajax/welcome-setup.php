<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();

$setup_step = $_POST['next_step'];

update_user_meta($user_id, 'setup_step', $setup_step);

$res = array( 'setup_step' => $setup_step );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - next_step',
            'results' => $res
        )
    )
);