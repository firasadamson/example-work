<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();

$current_password = $_POST['current_password'];
$new_password = $_POST['new_password'];
$confirm_password = $_POST['confirm_password'];

$userdata = get_user_by('ID', $user_id);

// compare
if ( $new_password != $confirm_password ) {
    $this->parent->error->send_error('password_not_match');
    return;
}

// check password
$result = wp_check_password($current_password, $userdata->user_pass, $user_id);
// check password
if (!$result) {
    $this->parent->error->send_error('password_failed');
    return;
}

global $wpdb;
$user_login = $userdata->user_login;
$wpdb->update(
    $wpdb->users, 
    array(
        'user_pass' => MD5($new_password)
    ), 
    array('ID' => $user_id)
);

// log out if logged in
if ( is_user_logged_in() ) {
    wp_logout();
    wp_cache_delete($user_id, 'users');
	wp_cache_delete($user_login, 'userlogins');
}

wp_signon( 
    array( 
        'user_login' => $user_login, 
        'user_password' => $new_password 
    ) 
);

wp_set_current_user( $user_id, $user_login );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update password'
        )
    )
);