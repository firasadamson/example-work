<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
if (!$user_id) $this->auth_failed();

$notifications = $_POST['notifications'];
$mobile = $_POST['mobile'];
$mobile_country_code = $_POST['mobile_country_code'];

update_user_meta($user_id, 'user_notifications', $notifications);
update_user_meta($user_id, 'mobile', $mobile);
update_user_meta($user_id, 'mobile_country_code', $mobile_country_code);

// $crm = flight_alerts();
// $crm->crm->update_mobile();
if ($mobile) {
    $this->parent->crm->update_mobile();
}

$res = array( 'notifications' => $notifications, 'mobile' => $mobile );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - user notifications',
            'results' => $res
        )
    )
);