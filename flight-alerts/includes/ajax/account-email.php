<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
$password = $_POST['password'];
$new_email = $_POST['new_email'];
$confirm_email = $_POST['confirm_email'];
$userdata = get_user_by('ID', $user_id);

// check new email
if ( $new_email != $confirm_email ) {
    $this->parent->error->send_error('email_not_matched');
    return;
}

if ( $new_email == $userdata->user_email ) {
    $this->parent->error->send_error('email_same');
    return;
}

$result = wp_check_password($password, $userdata->user_pass, $user_id);
// check password
if (!$result) {
    $this->parent->error->send_error('password_failed');
    return;
}

global $wpdb;
$user_login = $userdata->user_login;
$wpdb->update(
    $wpdb->users, 
    array(
        'user_email' => $new_email,
        'user_login' => $new_email,
    ), 
    array('ID' => $user_id)
);

// log out if logged in
if ( is_user_logged_in() ) {
    wp_logout();
    wp_cache_delete($user_id, 'users');
	wp_cache_delete($user_login, 'userlogins');
}

wp_signon( 
    array( 
        'user_login' => $new_email, 
        'user_password' => $password 
    ) 
);

wp_set_current_user( $user_id, $new_email );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update email'
        )
    )
);