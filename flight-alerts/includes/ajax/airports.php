<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
if (!$user_id) $this->auth_failed();

$airports = $_POST['airports'];

update_user_meta($user_id, 'user_airports', $airports);

$res = array( 'airports' => $airports );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - airports',
            'results' => $res
        )
    )
);