<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
if (!$user_id) $this->auth_failed();

$months = $_POST['months'];

update_user_meta($user_id, 'user_months', $months);

$res = array( 'months' => $months );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - months',
            'results' => $res
        )
    )
);