<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
if (!$user_id) $this->auth_failed();

$regions = $_POST['regions'];

update_user_meta($user_id, 'user_regions', $regions);

$res = array( 'regions' => $regions );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - regions',
            'results' => $res
        )
    )
);