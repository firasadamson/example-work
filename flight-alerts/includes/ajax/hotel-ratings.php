<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
if (!$user_id) $this->auth_failed();

$hotel_ratings = $_POST['hotel_ratings'];

update_user_meta($user_id, 'user_hotel_ratings', $hotel_ratings);

$res = array( 'hotel_ratings' => $hotel_ratings );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - hotel ratings',
            'results' => $res
        )
    )
);