<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();
$product_id = $_POST['product'];
$selected_plan = $_POST['selected_plan'];

if ( !$product_id || !$selected_plan ) {
    // fail
}


if( !WC()->cart->is_empty() ) {
    WC()->cart->empty_cart();
}

WC()->cart->add_to_cart( $product_id, 1, $selected_plan );
// get selected plan
// add to cart
// redirect to checkout
$res = array( 
    'product_id' => $product_id, 
    'selected_plan' => $selected_plan, 
    'checkout_html' => do_shortcode('[woocommerce_checkout]'),
    'redirect' => site_url() . '/signup/pay'
);

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - select plan',
            'results' => $res
        )
    )
);