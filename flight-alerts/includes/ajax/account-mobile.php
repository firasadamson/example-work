<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();

$mobile = $_POST['mobile'];
$mobile_country_code = $_POST['mobile_country_code'];

update_user_meta($user_id, 'mobile', $mobile);
update_user_meta($user_id, 'mobile_country_code', $mobile_country_code);

$res = array( 'mobile' => $mobile_country_code . $mobile );

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - mobile',
            'results' => $res
        )
    )
);