<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();

$billing = $_POST['billing'];

foreach( $billing as $row ) {
    update_user_meta( $user_id, $row['key'], $row['val'] );
}

exit(
    wp_send_json(
        array(
            'status' => 'success', 
            'message' => 'update - billing',
            'results' => $res
        )
    )
);