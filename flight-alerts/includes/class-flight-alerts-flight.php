<?php
/**
 * Flight class file.
 *
 * @package WordPress Plugin Template/Flight
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require __DIR__ . '/../vendor/autoload.php';
use Twilio\Rest\Client;

/**
 * Flight class.
 */
class Flight_Alerts_Flight {

	/**
	 * The single instance of Flight_Alerts_Flight.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin flight.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available flight for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $flight = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = $this->parent->_token;
		$this->assets_url = $this->parent->assets_url;

	}

	/**
	 * Initialise flight
	 *
	 * @return void
	 */


	function generate_destination_id( $country_from_code, $origin_from, $country_to_code, $origin_to ) 
	{
		return $country_from_code . ':' . $origin_from . '-' . $country_to_code . ':' . $origin_to;
	}

	function generate_flight_url( $args ) 
	{
		extract($args);
		
		return 'https://www.google.com/flights?hl=en#flt=' . $origin . '.' . $destination . '.' . $departure_date . '*' . $destination . '.' . $origin . '.' . $return_date . ';c:GBP;e:1;sd:1;t:f;sc:'.$cabin_class[0];
		// https://www.google.com/flights?hl=en#flt=/m/052bw.BER.2021-02-18*BER./m/052bw.2021-02-22;c:GBP;e:1;sc:b;sd:1;t:f
		// https://www.google.com/flights?hl=en#flt=/m/052bw.BER.2021-02-18*BER./m/052bw.2021-02-22;c:GBP;e:1;sc:;sd:1;t:f
	}

	function extract_data_from_destination_id( $destination_id ) 
	{
		$codes = explode('-',$destination_id);
		$origin_data = explode(':',$codes[0]);
		$destination_data = explode(':',$codes[1]);
		return array(
			'origin_code' => $origin_data[1],
			'origin_country_code' => $origin_data[0],
			'destination_code' => $destination_data[1],
			'destination_country_code' => $destination_data[0]
		);
	}
	
	function get_cabin_class( $letter ) 
	{
		switch( $letter ) 
		{
			case 'C': return 'business';
			case 'F': return 'first';
			case 'M':
			default: return 'economy';
		}
	}

	function get_city_image_url( $city_code, $city_name, $country_name ) 
	{
		$file = $city_code.'.jpeg';
		$file_url = FA_PLUGIN_ASSETS_URL . '/img/cities/'.$file;
		if ( !file_exists ( FA_PLUGIN_ASSETS_IMG_PATH . 'cities' . DIR_SEP . $file ) ) 
		{
			// let's fetch using API
			$file_url = $this->parent->api->fetch_city_image( $city_code, $city_name, $country_name );
			if ( !$file_url ) return false;
		}
		return $file_url;
	}

	public function load_destination_json() {
		return array(
			'airports' => $this->load_airports_json(),
			'countries' => $this->load_countries_json(),
			'cities' => $this->load_cities_json()
		);
	}

	public function load_airports_json() 
	{
		return $this->parent->json->load_json('airports.min.json');
	}

	public function load_countries_json() 
	{
		return $this->parent->json->load_json('countries.json');
	}

	public function load_cities_json() 
	{
		return $this->parent->json->load_json('cities.json');
	}

	public function get_country_data( $code, $data ) 
	{
		return include 'flight' . DIR_SEP . 'get-country-data.php';
	}

	public function get_city_data( $code, $data ) 
	{
		return include 'flight' . DIR_SEP . 'get-city-data.php';
	}
	
	public function get_airport_data( $airport_code, $data ) 
	{
		return include 'flight' . DIR_SEP . 'get-airport-data.php';
	}

	// api
	public function api_fetch_flight_data( $country = 'UK' ) {
		include 'flight' . DIR_SEP . 'api-fetch-flight-data.php';
	}
	
	// api
	public function db_insert_flight_data( $country = 'UK' ) {
		include 'flight' . DIR_SEP . 'db-insert-flight-data.php';
	}

	/**
	 * Main Flight_Alerts_Flight Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Flight is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Flight instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
