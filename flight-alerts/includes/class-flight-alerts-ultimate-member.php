<?php
/**
 * Ultimate_Member class file.
 *
 * @package WordPress Plugin Template/Ultimate_Member
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ultimate_Member class.
 */
class Flight_Alerts_Ultimate_Member {

	/**
	 * The single instance of Flight_Alerts_Ultimate_Member.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin ultimate_member.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available ultimate_member for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $ultimate_member = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = 'wpt_';
		$this->assets_url = $this->parent->assets_url;

		add_filter( 'um_email_template_html_formatting', array( $this, 'email_template_html' ), 10, 2 );
	}

	function email_template_html( $slug, $args ) {
		ob_start(); ?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<!--[if gte mso 15]>
			<xml>
				<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
				</o:OfficeDocumentSettings>
			</xml>
			<![endif]-->
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<!--[if !mso]><!-->
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<!--<![endif]-->
			<meta name="format-detection" content="telephone=no">
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<title>Rare Fare</title>
			<!--[if mso]>
			<style type="text/css">
				body { font-family: Helvetica, Arial, sans-serif }
				.header_headline,
				.bgcolor_headline { font-family: Georgia,"Times New Roman",Times,serif; }
			</style>
			<![endif]-->
		</head>
		<?php $head = ob_get_clean();
		return $head;
	}



	/**
	 * Main Flight_Alerts_Ultimate_Member Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Ultimate_Member is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Ultimate_Member instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
