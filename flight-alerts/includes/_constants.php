<?php
define( 'FA_SCRIPT_DEBUG', false );

define( 'DIR_SEP', DIRECTORY_SEPARATOR );

define( 'FA_COMPANY_NAME', 'RareFare');
define( 'FA_COMPANY_ADDRESS', '1b Blackfriars House, Parsonage, Manchester, M3 2JA');
define( 'FA_FULL_COMPANY_ADDRESS', FA_COMPANY_NAME . ', ' . FA_COMPANY_ADDRESS );

define( 'FA_SUBSCRIPTION_PRODUCT_ID', 111 );

define( 'FA_PLUGIN_URL', site_url() . '/wp-content/plugins/flight-alerts' );
define( 'FA_PLUGIN_ASSETS_URL', FA_PLUGIN_URL . '/assets' );
define( 'FA_PLUGIN_IMG_URL', FA_PLUGIN_ASSETS_URL . '/img' );
define( 'FA_PLUGIN_CITIES_URL', FA_PLUGIN_IMG_URL . '/cities' );
define( 'FA_LOGO_INVERTED_URL', FA_PLUGIN_ASSETS_URL . '/img/logo-inverted.png' );

define( 'FA_PLUGINS_PATH', ABSPATH . DIR_SEP . 'wp-content' . DIR_SEP . 'plugins' . DIR_SEP );
define( 'FA_PLUGIN_PATH', FA_PLUGINS_PATH . 'flight-alerts' . DIR_SEP );
define( 'FA_PLUGIN_DATA_PATH', FA_PLUGIN_PATH . 'data' .  DIR_SEP );
define( 'FA_PLUGIN_API_DATA_PATH', FA_PLUGIN_DATA_PATH . 'api-data' .  DIR_SEP );
define( 'FA_PLUGIN_ASSETS_PATH', FA_PLUGIN_PATH . 'assets' .  DIR_SEP );
define( 'FA_PLUGIN_ASSETS_IMG_PATH', FA_PLUGIN_ASSETS_PATH . 'img' . DIR_SEP );

define( 'FA_WCS_PLUGIN_PATH', FA_PLUGINS_PATH . 'woocommerce-subscriptions' . DIR_SEP );
define( 'FA_WCS_TEMPLATE_PATH', FA_WCS_PLUGIN_PATH . 'templates' . DIR_SEP );

define( 'FA_FROM_EMAIL_ADDRESS', 'help@rarefare.co.uk' );


// API keys
define( 'TWILIO_ACCOUNT_ID', 'AC3b3a1782264515092729b83ef4e54bb9' );
define( 'TWILIO_AUTH_TOKEN', '8ca657d0290507e13467f05352d9c001' );
define( 'TWILIO_PHONE_NUMBER', '+447862127244' );
define( 'TWILIO_FROM', FA_COMPANY_NAME );


// Amadeus API
$is_live = true;
if (!$is_live) {
    // test
    define( 'FLIGHT_API_BASE_URL', 'test.api.amadeus.com' );
    define( 'FLIGHT_API_KEY', 'l62Rpowbx84HW5eyhKpAM44Umji9o5My' );
    define( 'FLIGHT_API_SECRET', 'r3ayROJiMMyg1zBM' );
} else {
    // live
    define( 'FLIGHT_API_BASE_URL', 'api.amadeus.com' );
    define( 'FLIGHT_API_KEY', 'sBSnobxXhC7MflWTdLmwYNxedPcSJN2N' );
    define( 'FLIGHT_API_SECRET', 'IKdTpgZVauR7PPMf' );
}


// Kiwi Tequila API
// Quota information:
// 200 requests every 1 minute
define('KIWI_FLIGHT_API_KEY', 'D11nM0muwGH2VkNOnWaOZ5w6W2H9PVoz' );

define( 'PEXELS_API_KEY', '563492ad6f91700001000001d7a49378b7bb47719b3251d71f698bc9' );


// ActiveCampaign
// define("ACTIVECAMPAIGN_URL", "https://craigtomkins.api-us1.com");
// define("ACTIVECAMPAIGN_API_KEY", "a14109a7680e0d87b9e43589b9f2100c5b33a7a289ce19f655e7d647f70d98883f0de866");