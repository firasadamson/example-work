<?php
/**
 * Shortcode class file.
 *
 * @package WordPress Plugin Template/Shortcode
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Shortcode class.
 */
class Flight_Alerts_Shortcode {

	/**
	 * The single instance of Flight_Alerts_Shortcode.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin shortcode.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available shortcode for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $shortcode = array();
	
	public $auth_failed = null;
	public $regions = array();

	/**
	 * Construcregionsr function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = 'wpt_';
		$this->assets_url = $this->parent->assets_url;
		// $this->base = $this->parent->_token ;
		if ( is_admin()){
			return;
		}
		add_shortcode( 'fa_airports', array($this, 'airports') );
		add_shortcode( 'fa_regions', array($this, 'regions') );
		add_shortcode( 'fa_calendar', array($this, 'calendar') );
		add_shortcode( 'fa_budget', array($this, 'budget') );
		add_shortcode( 'fa_notifications', array($this, 'notifications') );

		add_shortcode( 'fa_account_email', array($this, 'account_email') );
		add_shortcode( 'fa_account_password', array($this, 'account_password') );
		add_shortcode( 'fa_account_mobile', array($this, 'account_mobile') );
		// add_shortcode( 'fa_account_notifications', array($this, 'budget') );
		add_shortcode( 'fa_account_plan', array($this, 'account_plan') );
		add_shortcode( 'fa_account_switch', array($this, 'account_switch') );
		add_shortcode( 'fa_account_billing', array($this, 'account_billing') );
		add_shortcode( 'fa_account_payment_history', array($this, 'account_payment_history') );
		add_shortcode( 'fa_welcome_setup', array($this, 'welcome_setup') );
		add_shortcode( 'fa_hotel_ratings', array($this, 'hotel_ratings') );
		add_shortcode( 'fa_logout', array($this, 'logout') );
		add_shortcode( 'fa_process_login', array($this, 'process_login') );

		add_shortcode( 'fa_subscription_redirect_to_account_plan', array($this, 'subscription') );

		add_shortcode( 'fa_redirect', array($this, 'redirect') );
		add_shortcode( 'fa_referral_link', array($this, 'referral_link') );
		add_shortcode( 'fa_test', array($this, 'test') );
		
		$this->regions = array(
			'AF' => 'Africa',
			'AS' => 'Asia',
			'CI' => 'Caribbean',
			'CA' => 'Central America',
			'EU' => 'Europe',
			'ME' => 'Middle East',
			'NA' => 'North America',
			'OC' => 'Oceania',
			'SA' => 'South America',
		);
	}

	function test() {
		$product_id = FA_SUBSCRIPTION_PRODUCT_ID;
$product = wc_get_product( $product_id );
// echo '<pre>';
// var_dump($product);
// echo '</pre>';
$available_variations = $product->get_available_variations();
// echo '<pre>';
// var_dump($available_variations);
// echo '</pre>';
$count = 0;
$output = '<div class="plans-wrapper content-box select-plan-wrapper" id="plans-wrapper" data-product="'.$product_id.'">';
foreach( $available_variations as $var ) {
    // $output .= '<div class="form-check">';
    //     $output .= '<input class="form-check-input" type="radio" name="plan" id="plan-'.$var['variation_id'].'" value="'.$var['variation_id'].'" '.($count == 0 ? ' checked="checked"' : '' ).' />';
    //     $output .= '<label class="form-check-label" for="premium">';
    //     $output .= $var['attributes']['attribute_plan'] . ' - ' . $var['price_html'];
    //     if ($var['variation_description']) {
    //         $output .= '<br /><small>'.$var['variation_description'].'</small>';
    //     }
    // $output .= '</div>';
    // $count++;

    $plan_title = $var['attributes']['attribute_plan'];
    $price = $var['price_html'];
    $id = $var['variation_id'];

    $output .= '<div class="form-check">';
        $output .= '<label class="form-check-label control control-radio" for="plan-'.$id.'">';
            $output .= '<input class="form-check-input" type="radio" name="plan" id="plan-'.$id.'" value="'.$id.'" '.($count == 0 ? ' checked="checked"' : '' ).' /><div class="control_indicator"></div>';
            $output .= '<div class="select-plan-title">' . $plan_title . '</div>';
            $output .= '<div>' . $price. '</div>';
            if ($var['variation_description']) {
                $output .= '<div><small>'.$var['variation_description'].'</small></div>';
            }
        $output .= '</label>';
    $output .= '</div>';
    $count++;

}
$output .= '</div>';
return $output;
	}

	public function check_auth() {
		$user_id = get_current_user_id();
		return !$user_id ? false : true;
	}

	private function auth_failed() {
		return $this->parent->error->output_error('auth_failed');
	}

	private function logout() {
		$this->parent->user->logout();
	}

	public function subscription() {
		wp_redirect( site_url() . '/members/account/plan/' );
		exit;
	}

	/**
	 * Initialise shortcode
	 *
	 * @return void
	 */
	
	private function inc($name, $atts = array() ) {
		return !$this->check_auth() ? $this->auth_failed() : include "shortcode" . DIR_SEP . "$name.php";
	}
	
	public function airports( $atts ) {
		return $this->inc('airports', $atts);
	}
	
	public function regions( $atts ) {
		return $this->inc('regions', $atts);
	}
	
	public function calendar( $atts ) {
		return $this->inc('calendar', $atts);
	}
	
	public function budget( $atts ) {
		return $this->inc('budget', $atts);
	}

	public function notifications( $atts ) {
		return $this->inc('notifications', $atts);
	}

	public function hotel_ratings( $atts ) {
		return $this->inc('hotel-ratings', $atts);
	}

	public function select_plan( $atts ) {
		return $this->inc('select-plan', $atts);
	}

	public function account_email() {
		return $this->inc('account-email');
	}

	public function account_password() {
		return $this->inc('account-password');
	}

	public function account_mobile() {
		return $this->inc('account-mobile');
	}

	public function account_plan() {
		return $this->inc('account-plan');
	}

	// public function account_switch() {
	// 	return $this->inc('account-switch');
	// }
	
	public function account_billing() {
		return $this->inc('account-billing');
	}

	public function account_payment_history() {
		return $this->inc('account-payment-history');
	}

	public function welcome_setup() {
		return $this->inc('welcome-setup');
	}

	public function welcome_content($content, $step) {
		// $output = '<h2>Step ' . $step . ' of 7 - ' . $text . '</h2>'; 
		// $output = '<h2>' . $text . '</h2>'; 
		$output  = strip_shortcodes($content);
		return $output;
	}


	private function welcome_buttons_arr($section = 'airports', $back_text = 'Back', $next_text = 'Next' ) {

		return  array(
			array(
				'id' => 'welcome-'.$section.'-back-btn', 
				'text' => $back_text,
				'class' => 'primary sm '.$section.'-welcome-btn'
			),
			array(
				'id' => 'welcome-'.$section.'-nxt-btn', 
				'text' => $next_text,
				'class' => 'secondary float-right '.$section.'-welcome-btn'
			)
		);
	}

	function process_login() {
		$user_id = get_current_user_id();
		UM()->user()->remove_cache( $user_id );
		$user = new WP_User($user_id); 
		$role = UM()->user()->get_role();

		if ( $role == "um_premium" || $role == 'um_basic' ) {
			$url = '/members/airports';
		} else if( $role == 'subscriber' ) {
			$url = '/signup/welcome';
		} else {
			$url = '/';
		}
		
		wp_redirect( $url );
		exit;
	}

	function get_mobile_form() {
		$user_id = get_current_user_id();
		$mobile = get_user_meta( $user_id, 'mobile', true);
		$mobile_country_code = get_user_meta( $user_id, 'mobile_country_code', true);
		if (!$mobile_country_code) $mobile_country_code = "+44";

		return '
		<div class="form-group row">
			<label for="mobile" class="col-sm-2 col-form-label">Mobile number</label>
			<div class="col-sm-10">
				<div class="input-group mb-3">
					' . $this->parent->component->country_code_dropdown('country_code', $mobile_country_code) . '
					<span class="input-group-text input-group-text-mobile" id="country-code-output" style="border-right:0; box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);">'.$mobile_country_code.'</span>
					<input type="tel" placeholder="Your mobile number without 0..." class="form-control mobile-number-input" style="padding-left:0; border-left:0" id="mobile" value="'.$mobile.'" /></div>
				</div>
			</div>
		</div>
		';
	}

	function redirect( $atts ) {

		if ( is_admin() ) return;

		$a = shortcode_atts( array(
			'url' => '',
		), $atts );

		extract($a);

		if (!$url) return;

		// wp_redirect( $url );
		// exit;
		return '<script>window.location.replace("'.$url.'");</script>';
	}


	function referral_link() {
		return '<div class="content-box">
		<p><input id="referral-link-input" value="'.$this->parent->user->user_affiliate_url().'" style="width:100%; max-width:500px; border:1px solid #ccc;" readonly="readonly" /></p>
		<p><button data-success-message="Referral link copied!" data-element-id="referral-link-input" class="btn btn-primary copy-to-clipboard">Copy Referral Link</button>	<span class="copy-to-clipboard-message" style="margin-left:15px; font-weight:bold"></span></p>
		</div>';
	}


	/**
	 * Main Flight_Alerts_Shortcode Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Shortcode is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Shortcode instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
