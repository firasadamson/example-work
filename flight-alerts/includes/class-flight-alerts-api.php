<?php
/**
 * Api class file.
 *
 * @package WordPress Plugin Template/Api
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Api class.
 */
class Flight_Alerts_Api {

	/**
	 * The single instance of Flight_Alerts_Api.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin api.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available api for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $regions = array();
	
	public $auth_failed = null;
	
	public $json_data = null;

	/**
	 * Construcregionsr function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) 
	{
		$this->parent = $parent;
		$this->base = $this->parent->_token;
	}

	// function process_curl_urls( $curl_urls, $data ) 
	// {
	// 	foreach( $curl_urls as $curl_url ) 
	// 	{
	// 		$resp = $this->curl($curl_url);
	// 		foreach( $resp->data as $row ) 
	// 		{
	// 			$data[] = $row;
	// 		}
	// 		sleep(3);
	// 	}
	// 	return $data;
	// }


	public function curl( $curl_url ) 
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $curl_url ); // &one_for_city=1
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'accept: application/json',
				'apikey: ' . KIWI_FLIGHT_API_KEY
			)
		);
		$resp = curl_exec($curl);
		$resp = json_decode($resp);
		curl_close($curl);
		
		return $resp;
	} 

	public function auth_flight_api() 
	{
		Requests::register_autoloader();
		$base_url = FLIGHT_API_BASE_URL;
		$api_key = FLIGHT_API_KEY;
		$api_secret = FLIGHT_API_SECRET;
		$url = 'https://' . $base_url . '/v1/security/oauth2/token';
		$auth_data = array();
		$auth_data[ 'client_id' ] = $api_key;
		$auth_data[ 'client_secret' ] = $api_secret;
		$auth_data[ 'grant_type' ] = 'client_credentials';
		$headers = array( 'Content-Type' => 'application/x-www-form-urlencoded' );

		try {
			$requests_response = Requests::post( $url, $headers, $auth_data );
			$response_body = json_decode( $requests_response->body );
			if ( property_exists($response_body, 'error') ) {
				die( '<p>' . ( $response_body -> error_description ) . '.</p>' );
			}
			$access_token = $response_body->access_token;
		} catch (Exception $e) {
			die( '<p>' . ( $e -> getMessage() ) . '.</p>' );
		}

		try {
			$requests_response = Requests::post( $url, $headers, $auth_data );
			$response_body = json_decode( $requests_response -> body );
			if ( property_exists( $response_body, 'error' ) ) {
				die( '<p>' . ( $response_body -> error_description ) . '.</p>' );
			}
			$access_token = $response_body -> access_token;
		} catch ( Exception $e ) {
			die( '<p>' . ( $e -> getMessage() ) . '.</p>' );
		}

		return $access_token;
	}

	public function fetch_data_from_api($endpoint, $access_token, $travel_data, $file_name) 
	{

		Requests::register_autoloader();

		$params = http_build_query( $travel_data );
		$url = $endpoint.'?'.$params;
		$headers = array( 'Authorization' => 'Bearer ' . $access_token );
		$options = array( 'timeout' => 10 );
		try {
			$requests_response = Requests::get( $url, $headers, $options );
			$response_body = json_decode( $requests_response->body );
			if ( property_exists($response_body, 'error') ) {
				die( '<p>' . ( $response_body -> error_description ) . '.</p>' );
			}
		
			return $response_body;

		} catch ( Exception $e ) {
			die( '<p>' . ( $e -> getMessage() ) . '.</p>' );
		}
	}

	function fetch_city_image( $city_code, $city_name, $country_name ) 
	{
		// $keyword = urlencode( $city_name . ' ' . $country_name);
		$keyword = urlencode( $city_name );
		$api = PEXELS_API_KEY;
		$curl_url = "https://api.pexels.com/v1/search?orientation=landscape&query=" . $keyword;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $curl_url ); // &one_for_city=1
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Authorization:' . $api
			)
		);
		$resp = curl_exec($curl);
		$resp = json_decode($resp);
		curl_close($curl);

		// var_dump($resp);

		foreach( $resp->photos as $img ) {
			if ( $img->width > $img->height ) {
				$img_url = $img->src->large;
				break;
			}
		}

		if (!$img_url) {
			return false;
		}
		$file = $city_code.'.jpeg';
		$filename = FA_PLUGIN_ASSETS_IMG_PATH . 'cities' . DIR_SEP . $file;
		file_put_contents ($filename, file_get_contents( $img_url ) );

		return FA_PLUGIN_ASSETS_URL . '/img/cities/'.$file;
	}

	/**
	 * Main Flight_Alerts_Api Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Api is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Api instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
