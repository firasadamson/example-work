<?php
/**
 * Json class file.
 *
 * @package WordPress Plugin Template/Json
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Json class.
 */
class Flight_Alerts_Json {

	/**
	 * The single instance of Flight_Alerts_Json.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin json.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available json for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $json = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
	}

	public function load_json($filename) 
	{
		return $this->get_json_file( $filename );
	}


	public function create_api_json_file($file_name, $data) 
	{
		$dir = FA_PLUGIN_API_DATA_PATH;
		$this->create_json_file($file_name, $data, $dir );
	}

	public function create_json_file($file_name, $data, $dir = null )
	{
		if (!$dir) $dir = FA_PLUGIN_DATA_PATH;
	
		$fp = fopen( $dir . $file_name . '.json', 'w');
		fwrite($fp, json_encode($data));
		fclose($fp);
	}
	
	public function get_api_json_file( $file_name ) 
	{
		$dir = FA_PLUGIN_API_DATA_PATH;
		return $this->get_json_file( $file_name, $dir );
	}

	public function get_json_file( $file_name, $dir = null )
	{
		if (!$dir) $dir = FA_PLUGIN_DATA_PATH;

		if ( pathinfo($file_name, PATHINFO_EXTENSION) != 'json' ) {
			$file_name = $file_name . '.json';
		}

		$file_name =  $dir . $file_name;
		if ( !file_exists ( $file_name ) ) 
		{
			return false;
		} 
		return json_decode( file_get_contents($file_name) );
	}
	

	/**
	 * Main Flight_Alerts_Json Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Json is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Json instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
