<?php
require_once( "../../../../../wp-blog-header.php");
require_once( "../../../../../wp-load.php");

$hotel_url = 'https://www.booking.com/searchresults.en-gb.html?sb=1&sb_lp=1&src=city&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fcity%2Fit%2Fmilan.en-gb.html%3Flabel%3Dgen173nr-1FCAMocTiSA0gJWARoUIgBAZgBCbgBB8gBDNgBAegBAfgBAogCAagCA7gC4emK_QXAAgHSAiQ0Yjk1YWQyZi1jMzMwLTQ5YjQtYTM3OS00ZGI1OGMwNjhkMjXYAgXgAgE%3Bsid%3Db28caba2b7c7eefa7dd902c45605da30%3Binac%3D0%26%3B&ss=Milan&is_ski_area=0&ssne=Milan&ssne_untouched=Milan&city=-121726&checkin_year=2020&checkin_month=12&checkin_monthday=1&checkout_year=2020&checkout_month=12&checkout_monthday=4&group_adults=2&group_children=0&no_rooms=1&b_h4u_keep_filters=&from_sf=1';

$fa = flight_alerts();

// $image_file = '/wp-content/plugins/flight-alerts/assets/img/cities/MIL.jpg';

if ( !file_exists( $image_file ) ) {
	// $image_file = null;
}

$default = array(
	'first_name' => 'Craig',
	'destination_city_code' => 'MIL',
	'destination_city_image' => $image_file,
	'destination_city' => 'Milan',
	'destination_country' => 'Italy',
	'lowest_price' => '&pound;26',
	'affiliate_url' => $fa->user->user_affiliate_url(),
	'data' => array(
		'flights' => array(
			array(
				'origin_city' => 'Manchester',
				'origin_airport' => 'Manchester Airport',
				'data' => array(
					array(
						'year_month_date' => 'November 2020',
						'price' => '&pound;26',
						'url' => 'https://www.google.com/flights?hl=en#flt=STN.BGY.2020-11-30*BGY.STN.2020-12-14;c:GBP;e:1;sd:1;t:f'
					),
					array(
						'year_month_date' => 'December 2020',
						'price' => '&pound;36',
						'url' => 'https://www.google.com/flights?hl=en#flt=STN.BGY.2020-11-30*BGY.STN.2020-12-14;c:GBP;e:1;sd:1;t:f'
					),
					array(
						'year_month_date' => 'January 2021',
						'price' => '&pound;46',
						'url' => 'https://www.google.com/flights?hl=en#flt=STN.BGY.2020-11-30*BGY.STN.2020-12-14;c:GBP;e:1;sd:1;t:f'
					),
				),
			)
		),
		'hotels' => array(
			array(
				'name' => 'The Awesome Hotel',
				'rating' => $fa->hotel->get_hotel_rating( 4 ), 
				'description' => 'Make yourself at home in one of the 8 air-conditioned guestrooms. Complimentary wireless Internet access keeps you connected, and digital programming is available for your entertainment. Partially open bathrooms with showers feature complimentary toiletries and hair dryers.Featured amenities include a 24-hour front desk and luggage storage.',
				'image' => 'https://d2573qu6qrjt8c.cloudfront.net/EFC9F04DB2D748E79812934CD062DE3B/B.JPEG',
				'url' => $hotel_url
			),
			array(
				'name' => 'The Semi-Awesome Hotel',
				'rating' => $fa->hotel->get_hotel_rating( 2 ), 
				'description' => 'Make yourself at home in one of the 8 air-conditioned guestrooms. Complimentary wireless Internet access keeps you connected, and digital programming is available for your entertainment. Partially open bathrooms with showers feature complimentary toiletries and hair dryers.',
				'image' => 'https://d2573qu6qrjt8c.cloudfront.net/EFC9F04DB2D748E79812934CD062DE3B/B.JPEG',
				'url' => $hotel_url
			),
			
		)
	)
);

extract($default);

$email = include_once 'flight-deals-email-template.php';

echo $email;