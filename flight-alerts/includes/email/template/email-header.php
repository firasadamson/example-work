<?php
return '
<tr>
  <td>
    <table width="100%" role="presentation" border="0" cellpadding="0" cellspacing="0" bgcolor="48a79f">
      <tr>
        <td style="vertical-align:middle;" class="email-template-header-left"><h1 style="color:#ffffff; font-family: sans-serif;line-height: 1.4;margin: 0; padding: 0; font-size: 20px !important; text-align: left; padding-left: 20px; font-weight: bold;" class="email-template-header-text">' . ( $header_text ? $header_text : '' ) . '</h1></td>
        <td width="120" style="vertical-align:middle" class="email-template-header-right">
          <img class="email-template-logo" style="width:108px; height:20px; padding: 10px 15px 5px 0" width="108" height="20" src="' . FA_LOGO_INVERTED_URL . '" />
        </td>
      </tr>
    </table>
  </td>
</tr>';