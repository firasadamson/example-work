
<?php 

$body_content = '<tr>';
	$body_content .= '<td class="wrapper">';
		$body_content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0">';
			$body_content .= '<tr>';
				$body_content .= '<td>';		
					$body_content .= '<p>Congratulations ' . $first_name . '</p>';
					$body_content .= '<p>Thanks for signing up to RareFare, you’re now officially a RareFare Flyer Club Member!</p>';
					$body_content .= '<p style="font-weight:bold">So, What’s Next?</p>';
					$body_content .= '<p>Give our high-tech software up to 24 Hours to scan the web for the very best flight deals that match your travel preferences. We’ll be in touch by email (and SMS if you opted in for text alerts) as soon as we have your incredible RareFares!</p>';
					$body_content .= '<p>In the meantime, why not check out this video for details on how it all works!</p>';
					$body_content .= '<p style="text-align:center"><a href="'.site_url().'/thankyou/" target="_blank"><img style="width:100%; max-width:600px;" alt="watch video" src="https://www.rarefare.co.uk/wp-content/uploads/2021/04/rarefare-video.png" /></a></p>';
				$body_content .= '</td>';
			$body_content .= '</tr>';
		$body_content .= '</table>';
	$body_content .= '</td>';
$body_content .= '</tr>';

return $body_content;