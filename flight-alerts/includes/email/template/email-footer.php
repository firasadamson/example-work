<?php
return '
<div class="footer" style="clear: both;margin-top: 10px;text-align: center;width: 100%;">
	<table width="100%" role="presentation" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding-bottom: 10px;padding-top: 10px;">
				<p class="footer-text" style="color: #999999;font-size: 12px;text-align: center;">
					' . FA_FULL_COMPANY_ADDRESS . '
					<br><br>Manage your subscription <a style="color: #999999;text-align: center;" href="'.site_url().'/members/alerts/">here</a>.
				</p>
			</td>
		</tr>
	</table>
</div>
';