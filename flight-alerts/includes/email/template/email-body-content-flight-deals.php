
<?php 

$FA = flight_alerts();

$body_content = '';

if ($destination_city_image) { 
	$body_content .= '<tr>';
	$body_content .= '<td>';
	$body_content .= '<img style="width:100%" width="100%" src="' . $destination_city_image . '" />';
	$body_content .= '</td>';
	$body_content .= '</tr>';
} 

$body_content .= '<tr>';
$body_content .= '<td class="wrapper">';
$body_content .= '<table role="presentation" border="0" cellpadding="0" cellspacing="0">';
$body_content .= '<tr>';
$body_content .= '<td>';		
$body_content .= '<p>Hi ' . $first_name . '</p>';
$body_content .= '<p>We have found <strong>' . $cabin_class . ' class</strong> return flights to ' . $destination_city . ' for ' . $lowest_price . ' (usually '.$usual_lowest_price.').</p>';
				
$body_content .= '<h2 style="font-size:18px; margin-bottom: 10px; margin-top: 25px; background-color:#EEE; padding: 10px; text-transform: uppercase">Available Deals</h2>';

foreach( $data['flights'] as $flights ) { 
	$body_content .= '<h3 style="margin-bottom: 10px; margin-top: 25px; border-bottom: 1px solid #eee; padding-bottom: 5px;">From ' . $flights['origin_airport'] . ', ' . $flights['origin_city'] . '</h3>';
	$body_content .= '<ul style="padding-left: 0; margin-left: 10px;">';
		foreach( $flights['data'] as $flight ) {
			$body_content .= '<li style="margin-left: 0; margin-bottom:5px;">' . $flight['year_month_date'] . ' - <a href="' . $flight['url'] . '" target="_blank">View Deal</a> (' . $flight['price'] . ')</li>';
		}
	$body_content .= '</ul>';
}
$body_content .= '<p style="margin-top:35px">These prices are in GBP and are accurate at the time this email is sent but are subject to change quickly.</p>';
				
$body_content .= '<h3 style="margin-bottom: 10px; margin-top: 25px; border-bottom: 1px solid #eee; padding-bottom: 5px;">How to Book Your Flight</h3>';
				
$body_content .= '<p>
				Step 1: Click a "View Deal" link above.<br />
				Step 2: Search the month for the best available flight that works for you.<br />
				Step 3: Book your flight directly with the airline.<br />
				Step 4: GO TRAVEL!
			</p>
					
			<p>Still have questions? Watch this video for a step by step example.</p>';
			
			if ( count($data['hotels']) > 0 ) {
				$body_content .= '<h2 style="font-size:18px; margin-bottom: 10px; margin-top: 25px; background-color:#EEE; padding: 5px; text-transform: uppercase">Available Hotels in ' . $destination_city . '</h2>';
				$body_content .= '<table>';
				foreach( $data['hotels'] as $hotel ) { 
					$body_content .= '<tr>';
					$body_content .= '<td width="115"><img width="100" height="66" style="width:100px; height: 66px;" src="' . $hotel['image'] . '" /></td>';
					$body_content .= '<td>';
					$body_content .= '<h4 style="padding: 0; margin: 0"><a href="' . $hotel['url'] . '" target="_blank">' . $hotel['name'] . '</a> ' . $hotel['rating'] . '</h4>';
					$body_content .= '<div style="margin-bottom:15px;">' . $hotel['description'] . '</div>';
					$body_content .= '</td>';
					$body_content .= '</tr>';
				} 
				$body_content .= '</table>';
			}

			$body_content .= '<h2 style="font-size:18px; margin-bottom: 10px; margin-top: 25px; background-color:#EEE; padding: 5px; text-transform: uppercase">Give a gift to a friend</h2>';
			$body_content .= '<p>We encourage sharing our deals! Forward this email to a friend who might like ' . $destination_city . '. Plus, anyone who gets forwarded this email can sign up for Rare Fare with this special link to get two months of free deals:</p>';
			$body_content .= '<p><a href="'.$affiliate_url.'" target="_blank">' . $affiliate_url . '</a></p>';


			$body_content .= '<h2 style="font-size:18px; margin-bottom: 10px; margin-top: 25px; background-color:#EEE; padding: 5px; text-transform: uppercase">Did you book this flight?</h2>';
			$body_content .= '<p>Nothing would make us happier than to hear that you scored a great deal using this notification! Share your Rare Fare using #RareFare on Instagram or Twitter and we’ll repost it on our @rarefaretravel account!</p>';
			$body_content .= '</td>';
			$body_content .= '</tr>';
			$body_content .= '</table>';
			$body_content .= '</td>';
			$body_content .= '</tr>';


return $body_content;