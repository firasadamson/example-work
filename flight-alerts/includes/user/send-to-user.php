<?php
// for display arrays
// ini_set("xdebug.var_display_max_children", '-1');
// ini_set("xdebug.var_display_max_data", '-1');
// ini_set("xdebug.var_display_max_depth", '-1');
// ini_set('memory_limit', '1024M');

um_fetch_user( $user_id );
$role = UM()->user()->get_role();

if ( $role != "um_premium" && $role != 'administrator' && $role != 'um_basic' ) {
    echo 'not '. $role;
    return false;
} 

$user_meta = UM()->user()->profile;

$first_name = $user_meta['first_name'];
$email_address = $user_meta['user_email'];

$user_mobile = $user_meta['mobile'];
$user_mobile_country_code = $user_meta['mobile_country_code'];

if ( $user_mobile_country_code && $user_mobile ) {
    $user_mobile = $user_mobile_country_code.$user_mobile;
} else {
    $user_mobile = null;
}

$user_airports_arr = maybe_unserialize($user_meta['user_airports']);
$user_regions_arr = maybe_unserialize($user_meta['user_regions']);
$user_months_arr = maybe_unserialize($user_meta['user_months']);
$user_budgets_arr = maybe_unserialize($user_meta['user_budgets']);
$user_hotel_ratings_arr = maybe_unserialize($user_meta['user_hotel_ratings']);

// var_dump( $user_airports_arr );
// var_dump( $user_regions_arr );
// var_dump( $user_months_arr );
// var_dump( $user_budgets_arr );
// var_dump( $user_hotel_ratings_arr );

global $wpdb;

$destinations = [];
$user_flights_data = [];

$this->json_data = $this->parent->flight->load_destination_json();

$table_name = $wpdb->prefix . $this->base . '_destination_dates';
$dd_rows = $wpdb->get_results(
    "SELECT * FROM $table_name WHERE departure_date > NOW() 
    ORDER BY departure_date ASC, destination_id ASC
    "
);


// GET deals that have already been sent to user
$table_name = $wpdb->prefix . $this->base . '_user_destination_dates';
$udd_rows = $wpdb->get_results(
    "SELECT * FROM $table_name WHERE user_id = '$user_id' AND sent_to_user=1
    "
);

echo 'destination_dates: ' . count($dd_rows) . '<br>';
echo 'user_destination_dates: ' . count($udd_rows) . '<br>';

$calc = count($dd_rows) - count($udd_rows);
// echo 'diff: ' . $calc . '<br>';

if ( count($udd_rows) > 0) {

    foreach( $udd_rows as $k2 => $r2 ) {
        foreach( $dd_rows as $k => $r ) {
            if ( $r->id == $r2->destination_date_id ) {
                unset($dd_rows[$k]);
            }
        }
    }
}
    
// echo '$dd_rows:' . count($dd_rows) . '<br>';

$user_flights = [];

$todays_date = date( 'Y-m-d H:i:s' );

$count3 = 0;

/* 
##########################################################
BUILD EMAIL DATA
##########################################################
*/
foreach($dd_rows as $row) {

    $codes = explode('-', $row->destination_id );

    $origin_data = explode(':',$codes[0]);
    $origin = $origin_data[1];
    $origin_country = $origin_data[0];
    $origin_airport_data = $this->parent->flight->get_airport_data( $origin, $this->json_data );
    $origin_city_code = $origin_airport_data['city_code'];
    
    $destination_data = explode(':',$codes[1]);
    $destination = $destination_data[1];
    $destination_country = $destination_data[0];
    $destination_airport_data = $this->parent->flight->get_airport_data( $destination, $this->json_data );
    $destination_city_code = $destination_airport_data['city_code'];
    
    $dep_date = $row->departure_date;
    $dep_year_date = date( 'Y-m', strtotime($dep_date) );
    $dep_month = date( 'm', strtotime($dep_date) );

    $country_data = $this->parent->flight->get_country_data( $destination_country, $this->json_data );
    $region_code = $country_data['region_code'];

    // regions
    if ( 
        in_array( $region_code, $user_regions_arr ) &&
        in_array( $dep_month,  $user_months_arr )  && 
        in_array( $origin_city_code,  $user_airports_arr )
    ) {

        if ( $role == "um_premium" || $role == 'administrator' ) {
            $class_arr =  [ 'first', 'business', 'economy' ];
        } else {
            $class_arr =  [ 'economy' ];
        }
        
        foreach( $class_arr as $cabin_class ) {

            $region_budget_key = $region_code . '_' . $cabin_class;
            
            if ( array_key_exists( $region_budget_key,  $user_budgets_arr ) && $cabin_class == $row->cabin_class ) {

                $budget = $user_budgets_arr[$region_budget_key];
                
                $price = $row->price;
                $usual_price = $row->usual_price;

                if ( ( $price <= $budget || $budget == 1000 ) && $price < 3000 ) {
                    // echo $row->id;
                    $destination_date_id = $row->id;
                    $user_flights[] = array(
                        'region_code' => $region_code,
                        'country' => $country_data['country_name'],
                        'destination' => $destination,
                        'destination_country' => $destination_country,
                        'destination_city_code' => $destination_city_code,
                        'destination_date_id' => $destination_date_id,
                        'origin_city_code' => $origin_city_code,
                        'origin' => $origin,
                        'dep_year_date' => $dep_year_date,
                        'price' => $price,
                        'usual_price' => $usual_price,
                        'cabin_class' => $cabin_class,
                        'user_id' => $user_id,
                        'sent_to_user' => 1,
                        'date_sent' => $todays_date,
                        'last_updated' => $todays_date,
                        'departure_date' => $row->departure_date,
                        'return_date' => $row->return_date
                    );
                    $count3++;
                }
            }
        }
    }
}
// echo '$count3:' . $count3 . '<br>';
// echo 'count($user_flights): ' . count($user_flights) . '<br>';
/* 
##########################################################
UPDATE USER FLIGHTS DB TABLE
##########################################################
*/
if ( count($user_flights) > 0) {
    $table_name = $wpdb->prefix . $this->base . '_user_destination_dates';
    $q = "REPLACE INTO $table_name (
        id,
        destination_date_id,
        user_id,
        sent_to_user,
        date_sent,
        price,
        usual_price,
        last_updated
    ) VALUES ";
    $county = 0;
    foreach ( $user_flights as $row5 ) {
        $q .= $wpdb->prepare(
            "(%s, %s, %d, %d, %s, %s, %s, %s),",
            $row5['user_id'] . '_' . $row5['destination_date_id'], 
            $row5['destination_date_id'], 
            $row5['user_id'], 
            $row5['sent_to_user'], 
            $row5['date_sent'], 
            $row5['price'], 
            $row5['usual_price'], 
            $row5['last_updated']
        );
        $county++;
    }
} else {
    return;
}

$q = rtrim( $q, ',' ) . ';';

$wpdb->query( $q );

// echo '>> count user destination:' . $county . '<br>';
// echo 'user_flights: ' . count($user_flights);

/* 
##########################################################
EMAIL - add all flights to be included in emails
##########################################################
*/
$flights_to_send_arr = [];
$count = 0;
foreach( $user_flights as $r ) {
    $flights_to_send_arr[ $r['region_code'] ][ $r['destination'] ][ $r['origin_city_code'] ][ $r['origin'] ][ $r['dep_year_date'] ] = array(
        'country' => $r['country'],
        'destination_city_code' => $r['destination_city_code'],
        'destination_country' => $r['destination_country'],
        'destination' => $r['destination'],
        'price' => $r['price'],
        'usual_price' => $r['usual_price'],
        'cabin_class' => $r['cabin_class'],
        'destination_date_id' => $r['destination_date_id'],
        'user_id' => $r['user_id'],
        'sent_to_user' => $r['sent_to_user'],
        'date_sent' => $r['date_sent'],
        'last_updated' => $r['last_updated']
    );
    $count++;
}


/* 
##########################################################
USER NOTIFICATIONS - count how many emails to send
##########################################################
*/
// $user_notifications = get_user_meta( $user_id, 'user_notifications', true );
$user_notifications = 3;

if ( $user_notifications == 1 || $user_notifications == 2 ) { 
    $low_rand = 1; 
} else if ( $user_notifications == 3 ) { 
    $low_rand = $user_notifications - 2; 
    
} else if ( $user_notifications >= 4 ) { 
    $low_rand = $user_notifications - 3; 
}
$no_of_emails_to_send = rand( $low_rand, $user_notifications );

$count_emails_sent_so_far = 0;

/* 
##########################################################
SEND MEAIL
##########################################################
*/
foreach ( $flights_to_send_arr as $region_code => $row1 ) {
    
    foreach ( $row1 as $destination => $row2 ) {
    
        $destination_airport_data = $this->parent->flight->get_airport_data( $destination, $this->json_data );
        $destination_city_code = $destination_airport_data['city_code'];
        $destination_city_name = $destination_airport_data['city_name'];
        $destination_country_name = $destination_airport_data['country_name'];

        $lowest_price = 0;
        $usual_lowest_price = 0;
        $best_hotel_dates = array();

        $image_file = $this->parent->flight->get_city_image_url( $destination_city_code, $destination_city_name, $destination_country_name );

        // 1. set the array for the email
        $data_for_email_to_send = array(
            'first_name' => $first_name,
            'destination_city_code' => $destination_city_code,
            'destination_city_image' => $image_file,
            'destination_city' => $destination_airport_data['city_name'],
            'destination_country' => $destination_airport_data['country_name'],
            'affiliate_url' => $this->user_affiliate_url($user_id),
            'cabin_class' => $cabin_class,
            'lowest_price' => '0',
            'usual_lowest_price' => '0',
            'data' => array(
                'flights' => array(),
                'hotels' => array()
            )
        );
        
        foreach ( $row2 as $origin_city_code => $row3 ) {
            
            foreach ( $row3 as $origin => $row4 ) {
                
                $origin_airport_data = $this->parent->flight->get_airport_data( $origin, $this->json_data );
                $origin_city_code = $origin_airport_data['city_code'];
                $origin_city_name = $origin_airport_data['city_name'];
                $origin_airport_name = $origin_airport_data['airport_name'];

                $data_for_email_to_send['data']['flights'][$origin] = array( 
                    'origin_city' => $origin_city_name,
                    'origin_airport' => $origin_airport_name
                );

                // 2. run
                foreach ( $row4 as $dep_year_date => $item ) {

                    // get hotel for first run...
                    
                    $date_data = explode("_", $item['destination_date_id']);

                    $args = array(
                        'destination' => $item['destination'],
                        'origin' => $origin,
                        'departure_date' => $date_data[1],
                        'return_date' => $date_data[2],
                        'cabin_class' => $item['cabin_class'],
                    );

                    $url = $this->parent->flight->generate_flight_url( $args );

                    $year_month_date = date('F Y',strtotime( $dep_year_date ));

                    $data_for_email_to_send['data']['flights'][$origin]['data'][] = array(
                        'year_month_date' => $year_month_date,
                        'price' => '&pound;' . round($item['price']),
                        'usual_price' => '&pound;' . round($item['usual_price']),
                        'cabin_class' => $item['cabin_class'],
                        'url' => $url
                    );

                    if ( $lowest_price == 0 || $item['price'] < $lowest_price ) {
                        $lowest_cabin_class = $item['cabin_class'];
                        $lowest_price = round($item['price']);
                        $usual_lowest_price = round($item['usual_price']);
                        $lowest_price_year_month_date = $year_month_date;
                        $best_hotel_dates = array(
                            'check_in_date' => $date_data[1],
                            'check_out_date' => $date_data[2],
                        );
                    }
                }
            }
        }


        // 3. hotels API
        // pass into API request and save as JSON.
        $hotels = $this->parent->hotel->api_get_hotels( $destination_city_code, $best_hotel_dates );
        $count_hotels = 0;

        if ( $hotels ) {

            foreach( $hotels as $hotel ) {
                if (isset($hotel->hotel)) {
                    $data = $hotel->hotel;
                    // echo $data->cityCode . '<br>';
                    if ( isset($data->description->text) && isset($data->media[0]->uri) && in_array( $data->rating, $user_hotel_ratings_arr ) ) {
                        $data_for_email_to_send['data']['hotels'][] = array( 
                            'name' => $data->name,
                            'description' => $this->parent->helper->limit_text($data->description->text, 35),
                            'rating' => $this->parent->hotel->get_hotel_rating( $data->rating ), 
                            'image' => $data->media[0]->uri,
                            'url' => $this->parent->hotel->get_hotel_url( $destination_city_name, $best_hotel_dates )
                        );
                    }
                    $count_hotels++;
                    if ($count_hotels > 10)  break;
                }
            }

        }

        $data_for_email_to_send['lowest_price'] = '&pound;' . $lowest_price;
        $data_for_email_to_send['usual_lowest_price'] = '&pound;' . $usual_lowest_price;
        $data_for_email_to_send['cabin_class'] = $lowest_cabin_class;
        $email_message = $this->parent->email->flight_deals_email_template( $data_for_email_to_send );
    
        // echo $email_message;
        // die();

        // $class_arr =  [ 'economy', 'business', 'first' ];
        if ( $lowest_cabin_class == 'business' ) {
            $cabin_class_label = '[Business] ';
        } else  if ( $lowest_cabin_class == 'first' ) {
            $cabin_class_label = '[First Class] ';
        } else {
            $cabin_class_label = '';
        }

        $email_args = array(
            'to' => $email_address,
            'subject' => $cabin_class_label . $data_for_email_to_send['destination_city'] . ', ' . $data_for_email_to_send['destination_country'] . ' £' . $lowest_price . ' (usually £'.$usual_lowest_price.')',
            'message' => $email_message
        );

        $this->parent->email->send_email( $email_args );

        $count_emails_sent_so_far++;
        if ( $no_of_emails_to_send <= $count_emails_sent_so_far ) {
            break;
        } 
    }
    if ( $no_of_emails_to_send <= $count_emails_sent_so_far ) {
        break;
    } 
}

/* 
##########################################################
SMS
##########################################################
*/
if ( $count_emails_sent_so_far > 0 && $user_mobile ) {

    $sms_args = $data_for_email_to_send;
    
    $sms_args['email_count'] = $count_emails_sent_so_far;
    $sms_args['lowest_price'] = $lowest_price;
    $sms_args['usual_lowest_price'] = $usual_lowest_price;
    $sms_args['lowest_price_year_month_date'] = $lowest_price_year_month_date;
    $sms_args['lowest_cabin_class'] = $lowest_cabin_class;

    $sms_args = array(
        'user_mobile' => $user_mobile,
        'sms_msg' => $this->parent->sms->sms_templates( $sms_args ) 
    );

    $this->parent->sms->send_sms( $sms_args );
}

print 'email count: ' . $count_emails_sent_so_far;