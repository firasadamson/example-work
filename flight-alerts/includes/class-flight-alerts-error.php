<?php
/**
 * Error class file.
 *
 * @package WordPress Plugin Template/Error
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Error class.
 */
class Flight_Alerts_Error {

	/**
	 * The single instance of Flight_Alerts_Error.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin error.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	public $error = array();

	/**
	 *
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = 'wpt_';

		$this->error = array(
			'auth_failed' => 'You need to be logged in to access this page. Go to the <a href="'.site_url().'/login">login</a> page.',
			'email_not_matched' => 'The email address you entered did not match. Please try again.',
			'email_same' => 'You have entered the same email address as your current one. Please enter a different one.',
			'password_failed' => 'The password you entered did not match your current password. Please try again.'
		);
	}

	public function output_error( $error ) {
		return $this->error[ $error ];
	}

	public function send_error( $error ) {
		exit(
			wp_send_json(
				array(
					'status' => 'failed', 
					'message' => $this->error[ $error ]
				)
			)
		);
	}


	/**
	 * Main Flight_Alerts_Error Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Error is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Error instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
