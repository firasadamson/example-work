<?php
/**
 * Database class file.
 *
 * @package WordPress Plugin Template/Database
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Database class.
 */
class Flight_Alerts_Database {

	/**
	 * The single instance of Flight_Alerts_Database.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin database.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available database for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $regions = array();
	public $auth_failed = null;

	/**
	 * Construcregionsr function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = $this->parent->_token ;
		$this->assets_url = $this->parent->assets_url;

		// $this->install_tables();
		// $this->drop_tables();
	}

	// function drop_tables() {
	// 	// if( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit();
	// 	global $wpdb;
	// 	$tables = [
	// 		'_destinations',
	// 		'_destination_prices',
	// 		'_user_destination_prices'
	// 	];
	// 	foreach( $tables as $table ) {
	// 		$table_name = $prefix . $this->base . $table;
	// 		$wpdb->query( "DROP TABLE IF EXISTS $table_name;" );
	// 	}
	// 	// delete_option("my_plugin_db_version");
	// }

	private function install_tables() {
		global $wpdb;

		$prefix = $wpdb->prefix;
		$charset_collate = $wpdb->get_charset_collate();

		/*
		hotels
		*/
		// $table_name = $prefix . $this->base . '_hotels';

		// $create_table_sql = "CREATE TABLE $table_name (
		// 	id VARCHAR(20) NOT NULL,
		// 	city_code VARCHAR(5) NOT NULL,
		// 	origin_country VARCHAR(5) NOT NULL,
		// 	destination VARCHAR(5) NOT NULL,
		// 	destination_country VARCHAR(5) NOT NULL,
		// 	last_updated DATETIME NOT NULL,
		// 	PRIMARY KEY (id)
		// ) $charset_collate;";

		// $this->maybe_create_table( $table_name, $create_table_sql );


		/*
		destinations
		*/
		$table_name = $prefix . $this->base . '_destinations';

		$create_table_sql = "CREATE TABLE $table_name (
			id VARCHAR(50) NOT NULL,
			origin VARCHAR(5) NOT NULL,
			origin_country VARCHAR(5) NOT NULL,
			destination VARCHAR(5) NOT NULL,
			destination_country VARCHAR(5) NOT NULL,
			last_updated DATETIME NOT NULL,
			PRIMARY KEY (id)
		) $charset_collate;";

		$this->maybe_create_table( $table_name, $create_table_sql );

		/*
		destinations dates
		*/
		$table_name = $prefix . $this->base . '_destination_dates';
		$create_table_sql = "CREATE TABLE $table_name (
			id VARCHAR(50) NOT NULL,
			destination_id VARCHAR(50) NOT NULL,
			departure_date DATETIME NOT NULL,
			return_date DATETIME NOT NULL,
			days INT(3) NOT NULL,
			price DECIMAL(13,2) NOT NULL, 
			prev_price DECIMAL(13,2) NOT NULL, 
			usual_price DECIMAL(13,2) NOT NULL, 
			cabin_class VARCHAR(20) NOT NULL,
			last_updated DATETIME NOT NULL,
			PRIMARY KEY (id)
		) $charset_collate;";

		$this->maybe_create_table( $table_name, $create_table_sql );
		
		/*
		destination prices
		*/
		// $table_name = $prefix . $this->base . '_destination_prices';

		// $create_table_sql = "CREATE TABLE $table_name (
		// 	id INT(11) NOT NULL AUTO_INCREMENT,
		// 	destination_date_id VARCHAR(40) NOT NULL,
		// 	currency VARCHAR(3) NOT NULL,
		// 	price DECIMAL(13,2) NOT NULL,
		// 	previous_price DECIMAL(13,2) NOT NULL,
		// 	base_price DECIMAL(13,2) NOT NULL,
		// 	previous_base_price DECIMAL(13,2) NOT NULL,
		// 	departure_datetime DATETIME NOT NULL,
		// 	arrival_datetime DATETIME NOT NULL,
		// 	duration INT(11) NOT NULL,
		// 	number_of_stops INT(3) NOT NULL,
		// 	departure_airport VARCHAR(5) NOT NULL,
		// 	arrival_airport VARCHAR(5) NOT NULL,
		// 	blacklisted_in_eu TINYINT(1) NOT NULL,
		// 	cabin_class VARCHAR(10) NOT NULL, 
		// 	last_updated DATETIME NOT NULL,
		// 	PRIMARY KEY (id)
		// ) $charset_collate;";

		// $this->maybe_create_table( $table_name, $create_table_sql );
		
		/*
		user destination prices
		*/
		$table_name = $prefix . $this->base . '_user_destination_dates';

		$create_table_sql = "CREATE TABLE $table_name (
			id  VARCHAR(100) NOT NULL,
			destination_date_id VARCHAR(50) NOT NULL,
			user_id INT(11) NOT NULL,
			sent_to_user TINYINT(1) NOT NULL,
			date_sent DATETIME NOT NULL,
			price DECIMAL(13,2) NOT NULL,
			prev_price DECIMAL(13,2) NOT NULL,
			usual_price DECIMAL(13,2) NOT NULL,
			last_updated DATETIME NOT NULL,
			PRIMARY KEY (id)
		) $charset_collate;";

		$this->maybe_create_table( $table_name, $create_table_sql );


		/*
		cronjob
		*/
		$table_name = $prefix . $this->base . '_cron_jobs';

		$create_table_sql = "CREATE TABLE $table_name (
			id INT(11) NOT NULL AUTO_INCREMENT,
			cron_job_type VARCHAR(5) NOT NULL,
			description VARCHAR(5) NOT NULL,
			start_datetime DATETIME NOT NULL,
			end_datetime DATETIME NOT NULL,
			PRIMARY KEY (id)
		) $charset_collate;";

		$this->maybe_create_table( $table_name, $create_table_sql );

	}

	function maybe_create_table( $table_name, $create_ddl ) {
		global $wpdb;

		$query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

		if ( $wpdb->get_var( $query ) === $table_name ) {
			return true;
		}
	
		// Didn't find it, so try to create it.
		$wpdb->query( $create_ddl );
	
		// We cannot directly tell that whether this succeeded!
		if ( $wpdb->get_var( $query ) === $table_name ) {
			return true;
		}
	
		return false;
	}



	/**
	 * Main Flight_Alerts_Database Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Database is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Database instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
