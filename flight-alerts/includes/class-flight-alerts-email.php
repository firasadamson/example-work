<?php
/**
 * Email class file.
 *
 * @package WordPress Plugin Template/Email
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Email class.
 */
class Flight_Alerts_Email {

	/**
	 * The single instance of Flight_Alerts_Email.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin email.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available email for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $email = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		add_filter( 'wp_mail_content_type', array($this, 'set_content_type' ) );
	}

	public function set_content_type(){
		return "text/html";
	}

	public function flight_deals_email_template( $args =  array() ) {
		extract($args);
		$header_text = ucfirst($cabin_class) . ' Class, ' . $destination_city . ', ' . $destination_country . ' - from ' . $lowest_price;
		return require 'email' . DIR_SEP . 'flight-deals-email-template.php';
	}

	public function welcome_email_template( $args =  array() ) {
		extract($args);
		$header_text = 'Welcome to RareFare';
		return require 'email' . DIR_SEP . 'welcome-email-template.php';
	}

	public function send_email( $args = array() ) {

		$default = array(
			'to' => 'fa@firas.org',
			'subject' => 'Hi. This is a test.',
			'headers' => 'From: ' . FA_FROM_EMAIL_ADDRESS,
			'message' => 'This is a message.'
		);

		$arr = array_merge( $default, $args);

		extract($arr);

		wp_mail( $to, $subject, $message, $headers );

	}

	/**
	 * Main Flight_Alerts_Email Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Email is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Email instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
