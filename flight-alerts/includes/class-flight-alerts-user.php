<?php
/**
 * User class file.
 *
 * @package WordPress Plugin Template/User
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * User class.
 */
class Flight_Alerts_User {

	/**
	 * The single instance of Flight_Alerts_User.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin user.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available user for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $user = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) 
	{
		$this->parent = $parent;
		$this->base = $this->parent->_token;

		add_shortcode( 'um_user', array($this,'um_user_shortcode') );
	}

	// user
	public function send_to_user( $user_id = null ) 
	{
		if (!$user_id) $user_id = get_current_user_id();
		include 'user' . DIR_SEP . 'send-to-user.php';
	}

	public function logout() {
		wp_logout();
		wp_redirect( site_url() );
		exit;
	}

	public function user_affiliate_url( $user_id = null ) 
	{
		if (!$user_id) $user_id = get_current_user_id();
		return site_url() . '/?r=' . $user_id;
	}

	/**
	 * Returns a user meta value
	 * Usage [um_user user_id="" meta_key="" ] // leave user_id empty if you want to retrive the current user's meta value.
	 * meta_key is the field name that you've set in the UM form builder
	 * You can modify the return meta_value with filter hook 'um_user_shortcode_filter__{$meta_key}'
	 */
	function um_user_shortcode( $atts ) {
		$atts = extract( shortcode_atts( array(
			'user_id' => get_current_user_id(),
			'meta_key' => '',
		), $atts ) );
		
		if ( empty( $meta_key ) ) return;
		
		if( empty( $user_id ) ) $user_id = get_current_user_id(); 
		
		$meta_value = get_user_meta( $user_id, $meta_key, true );
		if( is_serialized( $meta_value ) ){
			$meta_value = unserialize( $meta_value );
		} 
		if( is_array( $meta_value ) ){
			$meta_value = implode(",",$meta_value );
		}  
		return apply_filters("um_user_shortcode_filter__{$meta_key}", $meta_value );
	
	}


	/**
	 * Main Flight_Alerts_User Instance
	 *
	 * Ensures only one instance of Flight_Alerts_User is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_User instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
