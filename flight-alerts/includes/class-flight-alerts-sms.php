<?php
/**
 * Sms class file.
 *
 * @package WordPress Plugin Template/Sms
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require __DIR__ . '/../vendor/autoload.php';
use Twilio\Rest\Client;

/**
 * Sms class.
 */
class Flight_Alerts_Sms {

	/**
	 * The single instance of Flight_Alerts_Sms.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin sms.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available sms for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $sms = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;

	}

	public function sms_templates( $args ) 
	{

		extract($args);

		$rand = rand(1,3);

		$greeting = ( $first_name ? 'Hi ' . $first_name : 'Hi' ) . '. ';
		$sms = $greeting;

		$cabin_class = $lowest_cabin_class . ' class';

		switch( $rand ) {

			case 2: {
				if ($email_count == 1) {
					// $sms .= 'We found a rare fare for you. See email for full details. - RF';
					$sms .= 'Look what we found - ' . $destination_city . ', ' . $destination_country . ' for £' . $lowest_price . ' in ' . $lowest_price_year_month_date . ' (' . $cabin_class . '). See email for full details.';
				} else {
					$sms .= 'Look what we found - ' . $destination_city . ', ' . $destination_country . ' for £' . $lowest_price . ' in ' . $lowest_price_year_month_date . ' (' . $cabin_class . ') + some more destinations. See emails for more details.';
				}
				break;
			}

			case 3: {
				if ($email_count == 1) {
					// $sms .= 'We found a rare fare for you. See email for full details. - RF';
					$sms .= 'Check this out - ' . $destination_city . ', ' . $destination_country . ' for £' . $lowest_price . ' in ' . $lowest_price_year_month_date . ' (' . $cabin_class . '). See email for full details.';
				} else {
					$sms .= 'Check this out - ' . $destination_city . ', ' . $destination_country . ' for £' . $lowest_price . ' in ' . $lowest_price_year_month_date . ' (' . $cabin_class . ') + some more destinations. See emails for full details.';
				}
				break;
			}

			default: {
				if ($email_count == 1) {
					// $sms .= 'We found a rare fare for you. See email for full details. - RF';
					$sms .= 'We found a rare fare to ' . $destination_city . ', ' . $destination_country . ' for £' . $lowest_price . ' in ' . $lowest_price_year_month_date . ' (' . $cabin_class . '). See email for full details.';
				} else {
					$sms .= 'We found a rare fare to ' . $destination_city . ', ' . $destination_country . ' for £' . $lowest_price . ' in ' . $lowest_price_year_month_date . ' (' . $cabin_class . ') and some more destinations. See emails for full details.';
				}
				break;
			}
		}

		$sms .= ' - RF';

		return $sms;

	}

	/**
	 * Initialise sms
	 *
	 * @return void
	 */
	public function send_sms( $args = array() ) {

		$default = array(
			'user_mobile' => '+447515569915',
			'sms_msg' => 'Test message'
			// Manchester to Copenhagen, Denmark round trip for £26 in September. See email for full details. -FD
		);

		$arr = array_merge( $default, $args);

		extract($arr);

		// Your Account SID and Auth Token from twilio.com/console
		$account_sid = TWILIO_ACCOUNT_ID;
		$auth_token = TWILIO_AUTH_TOKEN;
		// In production, these should be environment variables. E.g.:
		// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]

		// A Twilio number you own with SMS capabilities
		// $twilio_number = TWILIO_PHONE_NUMBER;
		
		$client = new Client($account_sid, $auth_token);

		try {
			$client->messages->create(
				// Where to send a text message (your cell phone?)
				$user_mobile,
				array(
					// 'from' => $twilio_number,
					'from' => TWILIO_FROM,
					'body' => $sms_msg
				)
			);
		}
		catch (exception $e) {
			//code to handle the exception
			// we can remove the mobile number from here if error.
			// then display message to user maybe or email the user perhaps?
		}
		// finally {
		// 	//optional code that always runs
		// }
	}

	/**
	 * Main Flight_Alerts_Sms Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Sms is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Sms instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
