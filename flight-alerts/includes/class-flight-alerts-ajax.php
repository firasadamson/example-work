<?php
/**
 * Ajax class file.
 *
 * @package WordPress Plugin Template/Ajax
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Ajax class.
 */
class Flight_Alerts_Ajax {

	/**
	 * The single instance of Flight_Alerts_Ajax.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin ajax.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available ajax for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $ajax = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = 'wpt_';
		$this->assets_url = $this->parent->assets_url;

		$arr = array(
			'update_airports',
			'update_budget',
			'update_calendar',
			'update_regions',
			'update_notifications',
			'update_account_email',
			'update_account_password',
			'update_account_mobile',
			'update_account_billing',
			'update_welcome_setup',
			'update_select_plan',
			'update_hotel_ratings',
		);

		foreach( $arr as $action) {
			add_action('wp_ajax_'.$action.'_action', array( $this, $action ));
			add_action('wp_ajax_nopriv_'.$action.'_action', array( $this, $action ));
		}

	}

	/**
	 * Initialise ajax
	 *
	 * @return void
	 */

	private function inc ($name) {
		include "ajax" . DIR_SEP . "$name.php";
	} 

	public function update_airports() {
		$this->inc('airports');
	}
	public function update_budget() {
		$this->inc('budget');
	}
	public function update_regions() {
		$this->inc('regions');
	}
	public function update_notifications() {
		$this->inc('notifications');
	}
	public function update_calendar() {
		$this->inc('calendar');
	}
	public function update_account_email() {
		$this->inc('account-email');
	}
	public function update_account_password() {
		$this->inc('account-password');
	}
	public function update_account_mobile() {
		$this->inc('account-mobile');
	}
	public function update_account_billing() {
		$this->inc('account-billing');
	}
	public function update_welcome_setup() {
		$this->inc('welcome-setup');
	}
	public function update_select_plan() {
		$this->inc('select-plan');
	}
	public function update_hotel_ratings() {
		$this->inc('hotel-ratings');
	}

	/**
	 * Main Flight_Alerts_Ajax Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Ajax is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Ajax instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
