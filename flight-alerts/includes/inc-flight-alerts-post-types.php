<?php
/*
 * this will be used for 'From' and 'To'
 * from
 * To
 * 
 */



$options = array(
    'hierarchical' => true,
    'exclude_from_search' => true,
    'publicly_queryable' => false,
    'supports' => array( 'title', 'page-attributes' ),
);

$this->register_post_type( 'airport', 'Origin Airports', 'Origin  Airport', 'Origin Airports available', $options );
// $this->register_post_type( 'country', 'Countries', 'Countries', 'Add the countries you want users to be able to select as their origin countries.', $options );
