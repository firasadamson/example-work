<?php

// the API request will first check if JSON file is present...
// if not, fetch from API and create JSON
// if yes, fetch JSON

$base_url = FLIGHT_API_BASE_URL;
$api_key = FLIGHT_API_KEY;
$api_secret = FLIGHT_API_SECRET;

$check_in_date = $best_hotel_dates['check_in_date'];
$check_out_date = $best_hotel_dates['check_out_date'];

$json_filename = $this->get_hotel_filename( $city_code, $best_hotel_dates );

$json = $this->parent->json->get_api_json_file( $json_filename );

if ($json) return $json;

// fetch API
$access_token = $this->parent->api->auth_flight_api();

if ( !$access_token ) return false;

$endpoint = 'https://' . $base_url . '/v2/shopping/hotel-offers';

$travel_data = array(
    'cityCode' 	  => sanitize_text_field( $city_code ),
    'checkInDate' 	        => sanitize_text_field( $check_in_date ),
    'checkOutDate' 	        => sanitize_text_field( $check_out_date ),
    // 'ratings' 	        => sanitize_text_field( '3,4,5' ),
);

$hotels = $this->parent->api->fetch_data_from_api($endpoint, $access_token, $travel_data, $json_filename);

if (isset($hotels->data)) {
    $this->parent->json->create_api_json_file( $json_filename, $hotels->data );
    return $hotels;
}

return false;


