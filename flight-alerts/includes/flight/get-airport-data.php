<?php
$airports = $data['airports'];
$airport_name = '';
foreach ( $airports as $key => $item ) 
{
    if ($item->iata_code == $airport_code ) 
    {
        $airport_name = $item->name;
        $country_code = $item->country_code;
        $city_name = $item->city;
        $city_code = $item->city_code;
        break;
    }
}

if(!$airport_name) {
    foreach ( $airports as $key => $item ) 
    {
        if ($item->city_code == $airport_code ) 
        {
            $airport_name = $item->name;
            $country_code = $item->country_code;
            $city_name = $item->city;
            $city_code = $item->city_code;
            break;
        }
    }
}

// we want the country_code
$countries = $data['countries'];
foreach ( $countries as $key => $item ) 
{
    if ($item->code ==  $country_code ) {
        $country_name = $item->name;
        $region_code = $item->region;
        break;
    }
}

$arr = array(
    'airport_name' => $airport_name,
    'airport_code' => $airport_code,
    'city_name' => $city_name,
    'city_code' => $city_code,
    'country_code' => $country_code,
    'country_name' => $country_name,
    'region_code' => $region_code,
);

return $arr;