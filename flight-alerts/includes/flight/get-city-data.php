<?php

// we want the country_code
$cities = $data['cities'];
foreach ( $cities as $key => $item ) {
    if ($item->city_code == $code ) {
        $country_code = $item->country_code;
        $city = $item->city;
        $blacklist = isset($item->blacklist) ? $item->blacklist : 'N';
        break;
    }
}

return array(
    'city' => $city,
    'city_code' => $code,
    'country_code' => $country_code,
    'blacklist' => $blacklist
);