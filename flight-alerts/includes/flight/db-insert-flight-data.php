<?php

ini_set('memory_limit', '1024M');

global $wpdb;

$sql = '';
$current_date = date( 'Y-m-d H:i:s' );

$countries_data = array();
$countries_data['countries'] = $this->load_countries_json();

$path = FA_PLUGIN_API_DATA_PATH; 
$files = glob($path.'flights_'.$country.'_*');

// var_dump($files);
// die();

if ( count($files) == 0) return;

foreach( $files as $file ) {
    // echo basename($file);
    $rows = $this->parent->json->get_api_json_file( basename($file) );
    // var_dump($rows);
    // die();
    $destination_rows = [];
    $destination_date_rows = [];

    foreach ( $rows as $row ) {

        $country = $this->get_country_data( $row->countryTo->code, $countries_data );
        if ( $country['blacklist'] == 'Y' ) {
            continue;
        }

        $city = $this->get_city_data( $row->cityCodeFrom, $cities_data );
        if ( $city['blacklist'] == 'Y' ) {
            continue;
        }

        // country code (from), origin airport code (from), country code (to), origin airport code (to)
        $code = $this->parent->flight->generate_destination_id( $row->countryFrom->code, $row->flyFrom, $row->countryTo->code, $row->flyTo );

        $destination_rows[] = [
            'id' => $code,
            'origin' => $row->flyFrom,
            'origin_country' => $row->countryFrom->code,
            'destination' => $row->flyTo,
            'destination_country' => $row->countryTo->code,
            'last_updated' => $current_date,
        ];

        $dep_date = explode('T', $row->local_departure);
        $dep_date = $dep_date[0];
        
        $rtn_date = date('Y-m-d', strtotime("$dep_date +$row->nightsInDest days"));
        $id = $code . '_' . $dep_date . '_' . $rtn_date;

        $destination_date_rows[] = [
            'id' => $id,
            'destination_id' => $code,
            'departure_date' => $dep_date,
            'return_date' => $rtn_date,
            'days' => $row->nightsInDest,
            'price' => $row->price,
            'usual_price' => $row->price * ((rand(30,60)/100) + 1),
            'cabin_class' => $this->parent->flight->get_cabin_class($row->route[0]->fare_category),
            'last_updated' => $current_date
        ];
    }

    $table_name = $wpdb->prefix . $this->base . '_destinations';
    // UPDATE INSTEAD OF INSERT IGNORE...
    $q = "INSERT IGNORE INTO $table_name 
    (id, origin, origin_country, destination, destination_country, last_updated) 
    VALUES ";
    foreach ( $destination_rows as $row ) {
        $q .= $wpdb->prepare(
            "(%s, %s, %s, %s, %s, %s),",
            $row['id'], $row['origin'], $row['origin_country'], $row['destination'], $row['destination_country'], $row['last_updated']
        );
    }
    $q = rtrim( $q, ',' ) . ';';
    $wpdb->query( $q );

    $table_name = $wpdb->prefix . $this->base . '_destination_dates';
    $q = "REPLACE INTO $table_name (id, destination_id, departure_date, return_date, days, price, usual_price, cabin_class, last_updated) VALUES ";
    foreach ( $destination_date_rows as $row ) {
        $q .= $wpdb->prepare(
            "(%s, %s, %s, %s, %s, %s, %s, %s, %s),",
            $row['id'], $row['destination_id'], $row['departure_date'], $row['return_date'], $row['days'], $row['price'],$row['usual_price'], $row['cabin_class'], $row['last_updated']
        );
    }
    $q = rtrim( $q, ',' ) . ';';
    $query_res = $wpdb->query( $q );

}


// echo '<h2>title: '.$query_res.'</h2>';

// echo '<pre>';
// var_dump($destination_date_rows);
// echo '</pre>';

// $path = FA_PLUGIN_API_DATA_PATH; 
// $files = glob($path.'flights_'.$country.'_*');

// foreach ( $files as $filename) {
//     unlink($filename);
// }

echo 'done';