<?php


ini_set("xdebug.var_display_max_children", '-1');
ini_set("xdebug.var_display_max_data", '-1');
ini_set("xdebug.var_display_max_depth", '-1');

ini_set('memory_limit', '1024M');

$cities = $this->parent->json->get_json_file( 'cities-top100' );
$city_keys = array_rand ( $cities, 10);
$cities_arr = array();
foreach( $city_keys as $key ) {
    $cities_arr[] = $cities[$key];
}
$cities = $cities_arr;
// we need to select 10 random cities
// var_dump($cities);
// die();

$dates = array(
    array(
        'from' => date('d/m/Y'),
        'to' => date('d/m/Y',  strtotime("+1 month"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+1 month")),
        'to' => date('d/m/Y',  strtotime("+2 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+2 months")),
        'to' => date('d/m/Y',  strtotime("+3 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+3 months")),
        'to' => date('d/m/Y',  strtotime("+4 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+4 months")),
        'to' => date('d/m/Y',  strtotime("+5 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+5 months")),
        'to' => date('d/m/Y',  strtotime("+6 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+6 months")),
        'to' => date('d/m/Y',  strtotime("+7 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+7 months")),
        'to' => date('d/m/Y',  strtotime("+8 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+8 months")),
        'to' => date('d/m/Y',  strtotime("+9 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+9 months")),
        'to' => date('d/m/Y',  strtotime("+10 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+10 months")),
        'to' => date('d/m/Y',  strtotime("+11 months"))
    ),
    array(
        'from' => date('d/m/Y',  strtotime("+11 months")),
        'to' => date('d/m/Y',  strtotime("+12 months"))
    )
);

$data = [];

// we need to fetch business class, first class and economy class
// M (economy)
// C (business),
// F (first class)
$cabin_classes = array( 'C', 'F', 'M' );
// $cabin_classes = array( 'F' );

foreach( $cabin_classes as $cabin_class ) {
    
    foreach( $dates as $date ) {

        $from_month = date( 'Y-m', strtotime( str_replace('/', '-', $date['from'])) );
        $date_from = str_replace( '/', '%2F', $date['from'] );
        $date_to = str_replace( '/', '%2F', $date['to'] );

        if ( $cabin_class == 'C' || $cabin_class == 'F' ) {

            foreach( $cities as $city ) {

                $curl_url = "https://tequila-api.kiwi.com/v2/search?fly_from=$country&date_from=$date_from&date_to=$date_to&flight_type=round&adults=1&partner_market=gb&vehicle_type=aircraft&nights_in_dst_from=2&nights_in_dst_to=14&max_stopovers=2&max_sector_stopovers=2&selected_cabins=$cabin_class&fly_to=" . $city->city_code;
                
                $resp = $this->parent->api->curl($curl_url);
                try {
                    if ( isset($resp->data) ) {
                        foreach( $resp->data as $row ) 
                        {
                            $data[] = $row;
                        }
                    }
                }
                catch (Exception $e) {
                }

                sleep(3);
            
                // save to JSON?
                if (count($data) > 0) {
                    $file_name = 'flights_' . $country . '_' . $city->city_code . '_' . $from_month . '_'  . $cabin_class;
                    $this->parent->json->create_api_json_file($file_name, $data);
                }
                $data = array();
            }


        } else {

            $curl_urls = [
                "https://tequila-api.kiwi.com/v2/search?fly_from=$country&date_from=$date_from&date_to=$date_to&flight_type=round&adults=1&partner_market=gb&vehicle_type=aircraft&nights_in_dst_from=2&nights_in_dst_to=14&max_stopovers=2&max_sector_stopovers=2",
                "https://tequila-api.kiwi.com/v2/search?fly_from=$country&date_from=$date_from&date_to=$date_to&flight_type=round&adults=1&partner_market=gb&vehicle_type=aircraft&nights_in_dst_from=2&nights_in_dst_to=14&max_stopovers=2&max_sector_stopovers=2&one_for_city=1"
            ];
            
            foreach( $curl_urls as $curl_url ) 
            {
                $resp = $this->parent->api->curl($curl_url);
                try {

                    foreach( $resp->data as $row ) 
                    {
                        $data[] = $row;
                    }
                }
                catch (Exception $e) {
                }
                sleep(3);
            }

            // save to JSON?
            if (count($data) > 0) {
                $file_name = 'flights_' . $country . '_' . $cabin_class . '_' . $from_month;
                $this->parent->json->create_api_json_file($file_name, $data);
            }
            $data = array();
        }
        
    }
    
}

// echo '<pre>';
// var_dump($resp);
// echo '</pre>';

echo 'done';