<?php

// we want the country_code
$countries = $data['countries'];
foreach ( $countries as $key => $item ) {
    if ($item->code ==  $code ) {
        $country_name = $item->name;
        $region_code = $item->region;
        $blacklist = isset($item->blacklist) ? $item->blacklist : 'N';
        break;
    }
}

return array(
    'country_code' => $code,
    'country_name' => $country_name,
    'region_code' => $region_code,
    'blacklist' => $blacklist
);