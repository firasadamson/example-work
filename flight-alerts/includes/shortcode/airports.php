<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$array = shortcode_atts( array(
    'is_welcome_page' => false,
    'buttons' => array(
        array(
            'id' => 'airports-save-btn',
            'text' => 'Update',
            'class' => 'primary'
        ),
        array(
            'id' => 'airports-next-btn', 
            'text' => 'Next',
            'class' => 'secondary float-right preferences-next-btn',
            'redirect' => '/members/regions/'
        )
    )
), $atts );

$output = '';
$countries = [];
$country_ids = [];

$user_id = get_current_user_id();
$user_airports = maybe_unserialize(get_user_meta( $user_id, 'user_airports', true));
if (!$user_airports) $user_airports = array();

$args = array(
    'post_type' => 'airport',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'posts_per_page' => -1,
    'post_parent' => 0
);

$query = new WP_Query( $args ); 

if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();
        $id = get_the_ID();
        $country_ids[] = $id;
        $countries[$id] = get_the_title();
    }
}

wp_reset_postdata();

$output .= '<div class="airports-container">';

if ( count($country_ids) > 0 ) {  

    foreach( $countries as $country_id => $country_title ) {

        $country_code =  strtolower(get_post_meta( $country_id, 'country_code', true));

        $args = array(
            'post_type' => 'airport',
            'order' => 'ASC',
            'orderby' => 'title',
            'posts_per_page' => -1,
            'post_parent' => $country_id
        );
        
        $query = new WP_Query( $args ); 

        
        $open_country = false;
        if ( $query->have_posts() ) {
            $output .= '<div class="airport-country-wrapper no-padding content-box click">';
                $output .= '<div class="airport-country-title" id="airport-country-title-' . $country_id . '"><span class="flag-icon flag-icon-'.$country_code.'" style="margin-right:2px"></span> '.$country_title.' <span class="float-right"><i class="icon ticon ticon-chevron-down" id="country-chevron-'.$country_id.'" aria-hidden="true"></i></span></div>';
                $output .= '<ul class="airports-wrapper list-wrapper" id="airports-wrapper-' . $country_id . '">';
                while ( $query->have_posts() ) {
                    $query->the_post();
                    $airport_id = $this->parent->helper->get_string_between( get_the_title(), '(', ')' );
                    $output .= '<li class="list-item list-col-1-of-3"><label for="'.$airport_id.'" class="control control-checkbox"><input '.(in_array( $airport_id, $user_airports ) ? 'checked="checked"' : '') .' type="checkbox" id="'.$airport_id.'" value="'.$airport_id.'" /><div class="control_indicator"></div> ' . get_the_title() . '</label></li>';
                    
                    if ( !$open_country && in_array( $airport_id, $user_airports )) {
                        $open_country = true;
                    }

                }
                $output .= '</ul>';
            $output .= '</div>';

            if ( $open_country ) {
                $output .= "<script> jQuery(document).ready( function() { 
                    let id = '" . $country_id . "';
                    jQuery('#airports-wrapper-' + id).show();  
                    jQuery('#country-chevron-' + id).toggleClass('ticon-chevron-down');
                    jQuery('#country-chevron-' + id).toggleClass('ticon-chevron-up');
                }); </script>";
            }

		} 
    }
	$output .= $this->parent->component->btns( $array );
} else {
    $output = 'No countries available.';
}

$output .= '</div>';


return $output;

