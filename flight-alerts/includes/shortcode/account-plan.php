<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


ini_set("xdebug.var_display_max_children", '-1');
ini_set("xdebug.var_display_max_data", '-1');
ini_set("xdebug.var_display_max_depth", '-1');

ini_set('memory_limit', '1024M');

$account_section = 'account-plan';

$user_id = get_current_user_id();
if (!$user_id) return $this->auth_failed();


$subscription = $this->parent->woocommerce->get_latest_subscription();
// var_dump($subscription);

if (!$subscription) {
    // if a user doesn't have a subscription, let's redirect them to the product page...
    $plan_product_page_url = $this->parent->woocommerce->change_plan_redirect_url();
    $output .= '<div class="content-box" style="margin-bottom:25px">';
    $output .= '<div class="content-box-header"><h3>You do not currently have a plan.</h3></div>';
    $output .= '<p>Please click below to select a plan.</p>';
    $output .= '<p><a class="button btn btn-primary" href="'.$plan_product_page_url.'">Select a Plan</a></p>';
    $output .= '</div>';

    return $output;
}

$data = $subscription->get_data();
$status = $data['status'];
if ($data) {
    foreach($data['line_items'] as $item_id => $item) {
        $name = $item->get_name();
        break;
    }
}

$plan_product_page_url = $this->parent->woocommerce->change_plan_redirect_url($data);


$output .= '<div class="content-box" style="margin-bottom:25px">';
    $output .= '<div class="content-box-header"><h3> ' .( $status == 'pending-cancel' || $status == 'active' ? 'Current Plan' : 'Previous Plan' ) . '</h3></div>';
    $output .= '<p>' . $name . '</p>';
    
    if ( $status == 'pending-cancel' ) {
        $output .= '<p>To change your plan, you will need to first reactivate your current plan.</p>';
    }

    if ( $status == 'active' ) {
        $label =  $status == 'active' ? 'Change Plan' : 'Select a Plan';
        $output .= $plan_product_page_url ? '<p><a class="button btn btn-primary" href="'.$plan_product_page_url.'">'.$label.'</a></p>' : '';
    }
$output .= '</div>';

$output .= '<div class="content-box plan-table">';
    $output .= '<div class="content-box-header"><h3>Plan Details</h3></div>';
    ob_start();
    wcs_get_template( 'myaccount/subscription-details.php', $subscription );
    $output .= ob_get_clean();

$output .= '</div>';

return $output;

function wcs_get_template( $file, $subscription ) {
    return include FA_WCS_TEMPLATE_PATH . $file;
}