<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$array = shortcode_atts( array(
    'is_welcome_page' => false,
    'buttons' => array(
        array(
            'id' => 'calendar-save-btn',
            'text' => 'Update',
            'class' => 'primary'
        ),
        array(
            'id' => 'calendar-next-btn', 
            'text' => 'Next',
            'class' => 'secondary float-right preferences-next-btn',
            'redirect' => '/members/budget/'
        )
    )
), $atts );

$user_id = get_current_user_id();
$user_months = maybe_unserialize(get_user_meta( $user_id, 'user_months', true));
if (!$user_months) $user_months = array();

$months = array(
    '01' => 'January',
    '02' => 'February',
    '03' => 'March',
    '04' => 'April',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July',
    '08' => 'August',
    '09' => 'September',
    '10' => 'October',
    '11' => 'November',
    '12' => 'December',
);

$output .= '<div class="content-box">';

    $output .= '<div class="list-header row">';
    $output .= '<h3 class="col col-sm-10">Select the months you want to travel in</h3>';
    $output .= '<span class="col col-sm-2 select-all-wrapper"><label class="control control-checkbox select-all">Select All <input type="checkbox" class="select-all-checkbox" /><div class="control_indicator"></div></label></span>';
    $output .= '</div>';

    $output .= '<ul class="list-wrapper">';
        foreach( $months as $code => $month ) {
            $output .= '<li class="list-item list-col-1-of-3">';
                $output .= '<label for="'.$code.'" class="control control-checkbox">';
                $output .= '<input class="checkbox" '.(in_array( $code, $user_months ) || count($user_months) == 0 ? 'checked="checked"' : '') .' type="checkbox" class="calendar-checkbox" id="'.$code.'" name="'.$code.'" value="'.$code.'" autocomplete="off"><div class="control_indicator"></div> ' . $month;
                $output .= '</label>';
            $output .= '</li>';
        }
        $output .= '</ul>';
    $output .= '</div>';
$output .= '</div>';

$output .= $this->parent->component->btns( $array );

return $output;