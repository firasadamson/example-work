<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$array = shortcode_atts( array(
    'is_welcome_page' => false,
    'buttons' => array(
        array(
            'id' => 'regions-save-btn',
            'text' => 'Update',
            'class' => 'primary'
        ),
        array(
            'id' => 'regions-next-btn', 
            'text' => 'Next',
            'class' => 'secondary float-right preferences-next-btn',
            'redirect' => '/members/calendar/'
        )
    )
), $atts );

$user_id = get_current_user_id();
$user_regions = maybe_unserialize(get_user_meta( $user_id, 'user_regions', true));
if (!$user_regions) $user_regions = array();

$regions = $this->regions;

    
$output .= '<div style="display:block;">';
    $output .= '<div class="map">';
        $output .= '<img src="'.$this->assets_url.'/img/map/map_base.png" />';
        $output .= '<img class="map-img" data-region="AF" id="map-AF" '.(in_array( 'AF', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_africa.png">';
        $output .= '<img class="map-img" data-region="AS" id="map-AS" '.(in_array( 'AS', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_asia.png">';
        $output .= '<img class="map-img" data-region="CA" id="map-CA" '.(in_array( 'CA', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_central_america.png">';
        $output .= '<img class="map-img" data-region="CI" id="map-CI" '.(in_array( 'CI', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_caribbean.png">';
        $output .= '<img class="map-img" data-region="EU" id="map-EU" '.(in_array( 'EU', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_europe.png">';
        $output .= '<img class="map-img" data-region="ME" id="map-ME" '.(in_array( 'ME', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_middle_east.png">';
        $output .= '<img class="map-img" data-region="NA" id="map-NA" '.(in_array( 'NA', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_north_america.png">';
        $output .= '<img class="map-img" data-region="OC" id="map-OC" '.(in_array( 'OC', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_oceania.png">';
        $output .= '<img class="map-img" data-region="SA" id="map-SA" '.(in_array( 'SA', $user_regions ) || count($user_regions) == 0  ? '' : 'style="display:none"') .' src="'.$this->assets_url.'/img/map/map_south_america.png">';
    $output .= '</div>';
$output .= '</div>';

$output .= '<div class="content-box">';

    $output .= '<div class="list-header row">';
        $output .= '<h3 class="col col-sm-10">Select the regions you want to travel to</h3>';
        $output .= '<span class="col col-sm-2 select-all-wrapper"><label class="control control-checkbox select-all region-select-all">Select All <input type="checkbox" class="select-all-checkbox" /><div class="control_indicator"></div></label></span>';
    $output .= '</div>';

    $output .= '<ul class="list-wrapper">';
    foreach( $regions as $region_id => $region_title ) {
        $output .= '<li class="list-item list-col-1-of-3">';
        $output .= '<label for="'.$region_id.'" class="control control-checkbox">';
        $output .= '<input class="regions-checkbox checkbox" '.(in_array( $region_id, $user_regions ) || count($user_regions) == 0 ? 'checked="checked"' : '') .' type="checkbox" id="'.$region_id.'" name="'.$region_id.'" value="'.$region_id.'" autocomplete="off"><div class="control_indicator"></div> ' . $region_title;
        $output .= '</label>';
        $output .= '</li>';
    }
    $output .= '</ul>';
$output .= '</div>';

$output .= $this->parent->component->btns( $array );

return $output;