<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$account_section = 'password';

$user_id = get_current_user_id();
$output = '';

$output = '
<div class="content-box">
    <div class="form-group row">
        <label for="current-password" class="col-sm-4 col-form-label">Enter current password</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" id="current-password" autocomplete="off" />
        </div>
    </div>
    <div class="form-group row">
        <label for="new-password" class="col-sm-4 col-form-label">Enter new password</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" id="new-password" autocomplete="off" />
        </div>
    </div>
    <div class="form-group row">
        <label for="confirm-password" class="col-sm-4 col-form-label">Confirm password</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" id="confirm-password" autocomplete="off" />
        </div>
    </div>
</div>
';

$output .= $this->parent->component->save_btn('account-'.$account_section.'-save-btn');

return $output;