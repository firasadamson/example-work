<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$array = shortcode_atts( array(
    'is_welcome_page' => false,
    'buttons' => array(
        array(
            'id' => 'calendar-save-btn',
            'text' => 'Select Plan',
            'class' => 'primary'
        )
    )
), $atts );

$user_id = get_current_user_id();

$output = '';

$output .= '
<p> Final Step - Begin your <strong>FREE 30 Day Trial</strong>.</p>
';
$product_id = FA_SUBSCRIPTION_PRODUCT_ID;
$product = wc_get_product( $product_id );
// echo '<pre>';
// var_dump($product);
// echo '</pre>';
$available_variations = $product->get_available_variations();
// echo '<pre>';
// var_dump($available_variations);
// echo '</pre>';
$count = 0;
$output .= '<div class="plans-wrapper content-box select-plan-wrapper" id="plans-wrapper" data-product="'.$product_id.'">';
foreach( $available_variations as $var ) {
    // $output .= '<div class="form-check">';
    //     $output .= '<input class="form-check-input" type="radio" name="plan" id="plan-'.$var['variation_id'].'" value="'.$var['variation_id'].'" '.($count == 0 ? ' checked="checked"' : '' ).' />';
    //     $output .= '<label class="form-check-label" for="premium">';
    //     $output .= $var['attributes']['attribute_plan'] . ' - ' . $var['price_html'];
    //     if ($var['variation_description']) {
    //         $output .= '<br /><small>'.$var['variation_description'].'</small>';
    //     }
    // $output .= '</div>';
    // $count++;

    $plan_title = $var['attributes']['attribute_plan'];
    $price = $var['price_html'];
    $id = $var['variation_id'];

    $output .= '<div class="form-check">';
        $output .= '<label class="form-check-label control control-radio" for="plan-'.$id.'">';
            $output .= '<input class="form-check-input" type="radio" name="plan" id="plan-'.$id.'" value="'.$id.'" '.($count == 0 ? ' checked="checked"' : '' ).' /><div class="control_indicator"></div>';
            $output .= '<div class="select-plan-title">' . $plan_title . '</div>';
            $output .= '<div>' . $price. '</div>';
            if ($var['variation_description']) {
                $output .= '<div><small>'.$var['variation_description'].'</small></div>';
            }
        $output .= '</label>';
    $output .= '</div>';
    $count++;

}
$output .= '</div>';

// $output .= '<div>Checkout...<br>';
//     $output .= $this->get_form();
//     // $output .= '<a href="/checkout/?add-to-cart=111&switch-subscription=158&variation-id=112&product-id=111">Swtich</a>';
// $output .= '</div>';

$output .= $this->parent->component->btns( $array );

// $output .= '
// One last step: Start your trial
// 30 days free access, then $3.99/month billed yearly.

// Get Business Class Deals
// Upgrade to a Business plan for an additional $3/month billed yearly. Get all our regular economy deals, as well as our exciting Business class deals and error fares when we find them!


// ✔ Secure form powered by Stripe
// Credit or debit card

// Your card will not be charged during trial. After trial your card will be charged $47.88 US for the year, auto renewing.
// [START FREE TRIAL] button';

$output .= $this->parent->woocommerce->billing_text();

return $output;