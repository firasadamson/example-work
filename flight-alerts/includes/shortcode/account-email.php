<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$account_section = 'email';

$user_id = get_current_user_id();
$userdata = get_user_by('ID', $user_id);

$output = '
<div class="content-box">
    <div class="form-group row">
        <div class="col-sm-4">
            Current email address
        </div>
        <div class="col-sm-8">
            <strong><span id="current-email">' . $userdata->user_email.'</span></strong>
        </div>
    </div>

    <div class="form-group row">
        <label for="new-email" class="col-sm-4 col-form-label">New email address</label>
        <div class="col-sm-8">
            <input type="email" class="form-control" id="new-email" autocomplete="off" />
        </div>
    </div>

    <div class="form-group row">
        <label for="confirm-email" class="col-sm-4 col-form-label">Confirm email address</label>
        <div class="col-sm-8">
            <input type="email" class="form-control" id="confirm-email" autocomplete="off" />
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-sm-4 col-form-label">Enter current password</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" id="password" autocomplete="off" />
        </div>
    </div>
</div>
';

$output .= $this->parent->component->save_btn('account-'.$account_section.'-save-btn');

return $output;