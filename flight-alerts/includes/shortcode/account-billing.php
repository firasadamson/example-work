<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$account_section = 'billing';

$user_id = get_current_user_id();
if (!$user_id) return $this->auth_failed();

$billing_fields = array(
	'billing_first_name' => 'First Name',
	'billing_last_name' => 'Last Name',
	// 'billing_company' => 'Company (optional)',
	'billing_address_1' => 'Address',
	'billing_address_2' => '&nbsp;',
	'billing_city' => 'City',
	'billing_state' => 'County/State',
	'billing_postcode' => 'Post Code',
	'billing_country' => 'Country',
);
$output = '';
foreach( $billing_fields as $key => $label ) {
	$val = get_user_meta( $user_id, $key, true );

	
	$output .= '
	<div class="form-group row content-box">
		<label for="mobile" class="col-sm-4 col-form-label">'.$label.'</label>
		<div class="col-sm-8">';
		if ( $key == 'billing_country' ) {
			global $woocommerce;    
			$output .= woocommerce_form_field( $key, array( 'type' => 'country', 'return' => true, 'id'=> $key, 'input_class' => array('form-control', 'billing-input') ), $val );
		} else {
			$output .= '<input type="text" class="form-control billing-input" id="'.$key.'" value="'.$val.'" />';
		}
		$output .= '</div>
	</div>
	';
}

$output .= $this->parent->component->save_btn('account-'.$account_section.'-save-btn');

return $output;