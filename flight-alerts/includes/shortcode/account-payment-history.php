<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// ini_set("xdebug.var_display_max_children", '-1');
// ini_set("xdebug.var_display_max_data", '-1');
// ini_set("xdebug.var_display_max_depth", '-1');

// ini_set('memory_limit', '1024M');

$account_section = 'payment-history';

$user_id = get_current_user_id();
if (!$user_id) return $this->auth_failed();


// $output = do_shortcode('[woocommerce_my_account order_count="-1"]');

// $output .= $this->parent->component->save_btn('account-'.$account_section.'-save-btn');

// ob_start();
// wc_get_template( 'myaccount/my-orders.php', array(
// 	'current_user'  => get_user_by( 'id', get_current_user_id() ),
// 	'order_count'   => -1,
// 	''
// ) );
// return ob_get_clean();

// return $output;

// $args = array(
//     'customer_id' => $user_id
// );

// $orders = wc_get_orders($args);

// foreach( $orders as $order ) {
// 	$data = $order->data;
// 	$items = $order->items;
// 	// var_dump($items);
// 	// $items['line_items'];

// 	echo '<div>';
// 	echo $data['total'] . ' ';
// 	echo '</div>';

// 	foreach( $items as $item ) {
// 		$item_data = $item->data;

// 		echo $item_data->order_id . ' ';
// 		echo $item_data->name . ' ';
// 	}

// }

// var_dump($orders);


$customer = $user_id;
// Get all customer orders
$customer_orders = get_posts(array(
	'numberposts' => -1,
	'meta_key' => '_customer_user',
	'orderby' => 'date',
	'order' => 'DESC',
	'meta_value' => $user_id,
	'post_type' => wc_get_order_types(),
	'post_status' => array_keys(wc_get_order_statuses()), 'post_status' => array('wc-processing', 'wc-completed'),
));

$output .= '<div class="row content-box">';

	$output .= '<div class="row payment-history-row payment-history-title desktop">';
		$output .= '<div class="col col-md-2">Order No.</div>';
		$output .= '<div class="col col-md-2">Date</div>';
		$output .= '<div class="col col-md-6">Plan</div>';
		$output .=  '<div class="col col-md-2 payment-history-total">Total</div>';
	$output .= '</div>';

foreach ($customer_orders as $customer_order) {

	$orderq = wc_get_order($customer_order);
	// var_dump($orderq);

	// $Order_Array[] = [
	// 	"id" => $orderq->get_id(),
	// 	// "name" => $orderq->get_name(),
	// 	"total" => $orderq->get_total(),
	// 	"date" => $orderq->get_date_created()->date_i18n('Y-m-d'),
	// ];

	$order_items = $orderq->get_items();
	$order_id = $orderq->get_id();
	$total = $orderq->get_total();
	$date = $orderq->get_date_created()->date_i18n('Y-m-d');

	foreach( $order_items as $item_id => $item ) {

		// var_dump($order_items);

		// methods of WC_Order_Item class
	
		// The element ID can be obtained from an array key or from:
		$item_id = $item->get_id();
	
		// methods of WC_Order_Item_Product class
	
		// $item_name = $item->get_name(); // Name of the product
		// $item_type = $item->get_type(); // Type of the order item ("line_item")
	
		// $product_id = $item->get_product_id(); // the Product id
		// $wc_product = $item->get_product();    // the WC_Product object
	
		// order item data as an array
		$item_data = $item->get_data();

		$output .= '<div class="row payment-history-row">';
			$output .= '<div class="col col-md-2"><div class="payment-history-title mobile">Order No</div>' . $order_id . '</div>';
			$output .= '<div class="col col-md-2"><div class="payment-history-title mobile">Date</div>' . $date . '</div>';
			$output .= '<div class="col col-md-6"><div class="payment-history-title mobile">Plan</div>' . $item_data['name'] . '</div>';
			// echo $item_data['product_id'];
			// echo $item_data['variation_id'];
			// echo $item_data['quantity'];
			// echo $item_data['tax_class'];
			// echo $item_data['subtotal'];
			// echo $item_data['subtotal_tax'];
			// $output .=  '<div>' . $item_data['total'] . '</div>';
			$output .=  '<div class="col col-md-2 payment-history-total"><div class="payment-history-title mobile">Total</div>&pound;' . $total . '</div>';
		// echo $item_data['total_tax'];
		$output .= '</div>';
	}

}

$output .= '</div>';

return $output;
// var_dump($Order_Array);