<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$array = shortcode_atts( array(
    'is_welcome_page' => false,
    'buttons' => array(
        array(
            'id' => 'notifications-save-btn',
            'text' => 'Update',
            'class' => 'primary'
        )
    )
), $atts );

$user_id = get_current_user_id();
$user_notifications = get_user_meta( $user_id, 'user_notifications', true);
if (!$user_notifications) $user_notifications = 3;

// $mobile = get_user_meta( $user_id, 'mobile', true);

$output = '<div class="content-box" style="display:none">';
    $output .= '
    <div class="content-box-header"><h3>Email Alerts</h3></div>
    <div class="row range-slider-row">
        <div class="col-sm-4">Set maximum daily email alerts</div>
        <div class="col-sm-8">
            <div class="range-wrap">
                <div class="range-value" data-id="user-notifications" id="range-user-notifications"></div>
                <input
                    style="width:100%"
                    class="range-input user-notifications-range-input"
                    id="user-notifications-input"
                    type="range"
                    min="1"
                    max="10"
                    step="1"
                    value="'. $user_notifications .'"
                />';
            $output .= '</div>';
        $output .= '</div>';
    $output .= '</div>';
$output .= '</div>';

$output .= '
<div class="content-box form-group row" style="margin-top:25px;">
    <div class="content-box-header"><h3>Mobile Alerts</h3></div>
    <p>If you want to an receive SMS alerts, add your number below otherwise leave blank.</p>
    '.$this->get_mobile_form().'
</div>
';

$output .= $this->parent->component->btns( $array );

return $output;
