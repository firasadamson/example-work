<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_id = get_current_user_id();

$output = '';

$setup_step = get_user_meta( $user_id, 'setup_step', true );
if (!$setup_step) $setup_step = 1;



switch($setup_step) {
    case 2: {
        $title = 'Where do you want flights to?';
        $content = get_post_field('post_content',12);
        $body = $this->regions( 
            array( 
                'is_welcome_page' => true, 
                'buttons' => $this->welcome_buttons_arr('regions')
            ) 
        );
        break;
    }
    case 3: {
        $title = 'What months do you want flights in?';
        $content = get_post_field('post_content',14);
        $body = $this->calendar( 
            array( 
                'is_welcome_page' => true, 
                'buttons' => $this->welcome_buttons_arr('calendar')
            )
        );
        break;
    }
    case 4: {
        $title = 'What’s the maximum you’ll pay for a flight?';
        $content = get_post_field('post_content',16);
        $body = $this->budget( 
            array( 
                'is_welcome_page' => true, 
                'buttons' => $this->welcome_buttons_arr('budget')
                ) 
            );
        break;
    }
    case 5: {
        $title = 'Hotel Ratings';
        $content = get_post_field('post_content',170);
        $body = $this->hotel_ratings( 
            array( 
                'is_welcome_page' => true, 
                'buttons' => $this->welcome_buttons_arr('hotel-ratings')
                ) 
            );
        break;
    }
    case 6: {
        $title = 'Notifications';
        $content = get_post_field('post_content',301);
        $body = $this->notifications( 
            array( 
                'is_welcome_page' => true, 
                'buttons' => $this->welcome_buttons_arr('notifications')
                ) 
            );
        break;
    }
    case 7: {
        $title = 'Select a Plan';
        $content = '<h2>'.$title.'</h2>';
        $body = $this->select_plan( 
            array( 
                'is_welcome_page' => true, 
                'buttons' => $this->welcome_buttons_arr('select-plan', '', 'Select Plan')
            ) 
        );
        break;
    }
    case 1:
    default: {
        $title = 'Where do you want flights from?';
        $content = get_post_field('post_content',10);
        $body = $this->airports( 
            array( 
                'is_welcome_page' => true, 
                'buttons' => $this->welcome_buttons_arr('airports')
            ) 
        );
        break;
    }
}


$output .= $this->welcome_content( $content, $setup_step );
$output .= $body;
return $output;