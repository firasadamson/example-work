<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$array = shortcode_atts( array(
    'is_welcome_page' => false,
    'buttons' => array(
        array(
            'id' => 'budget-save-btn',
            'text' => 'Update',
            'class' => 'primary'
        ),
        array(
            'id' => 'budget-next-btn', 
            'text' => 'Next',
            'class' => 'secondary float-right preferences-next-btn',
            'redirect' => '/members/hotels/'
        )
    )
), $atts );

$user_id = get_current_user_id();
$user_regions = maybe_unserialize(get_user_meta( $user_id, 'user_regions', true));
$user_budgets = maybe_unserialize(get_user_meta( $user_id, 'user_budgets', true));
if (!$user_regions) $user_regions = array();
if (!$user_budgets) $user_budgets = array();

$regions = $this->regions;
$not_selected_regions = array();

um_fetch_user( $current_user );
$role = UM()->user()->get_role();

if ( $role == "um_premium" || $role == 'um_account_setup' || $role == 'subscriber' || $role == 'administrator' ) {
    $classes = array( 
        'first' => 'First Class', 
        'business' => 'Business Class', 
        'economy' => 'Economy Class'
    );
} else {
    $classes = array(  'economy' => 'Economy Class' );
}

$output = '';

foreach( $regions as $region_code => $region_title ) {
    if ( in_array( $region_code, $user_regions ) ) {
        $output .= '<div class="content-box" style="margin-bottom:25px">';
            $output .= '<div class="content-box-header"><h3>' . $region_title . '</h3></div>';
            foreach ( $classes as $class_key => $class_title ) {

                $region_key = $class_key.'-'.$region_code.'-range';
                $budget_key = $region_code .'_'. $class_key;
                $val = (array_key_exists($budget_key, $user_budgets) ? $user_budgets[$budget_key] : '');
                if (!$val) {
                    $val = 500;
                    update_user_meta( $user_id, $budget_key, $val);
                }

                $output .= '<div style="margin-bottom:10px; font-weight:bold" class="row range-slider-row">
                    <div class="col-sm-4 range-label '.$class_key.'-label">'.$class_title.'</div>
                    <div class="col-sm-8">
                        <div class="range-wrap">
                            <div class="range-value" data-type="currency" data-max="1000" data-id="'.$region_key.'" id="range-'.$region_key.'"></div>
                            <input
                            style="width:100%"
                            class="region-range-input"
                            id="'.$region_key.'-input"
                            type="range"
                            min="300"
                            max="1000"
                            step="10"
                            data-region-key="'.$region_key.'"
                            data-key="'.$budget_key.'"
                            value="'. $val .'"
                            />
                        </div>
                    </div>
                </div>';
            }
        $output .= '</div>';
    } else {
        $not_selected_regions[$region_code] = $region_title;
    }
}

if ( count($not_selected_regions) > 0) {
    $output .= '<div class="content-box">';
        $output .= '<div style="font-weight:bold">Want to modify where you want to fly to?</div>';
        $output .= '<div>You can adjust your regions by going back.</div>';
        // foreach($not_selected_regions as $region_title) {
        //     $output .= '<div>- ' . $region_title . '</div>';
        // }
    $output .= '</div>';
}

$output .= $this->parent->component->btns( $array );

return $output;