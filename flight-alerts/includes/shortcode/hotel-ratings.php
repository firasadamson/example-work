<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$array = shortcode_atts( array(
    'is_welcome_page' => false,
    'buttons' => array(
        array(
            'id' => 'hotel-ratings-save-btn',
            'text' => 'Update',
            'class' => 'primary'
        ),
        array(
            'id' => 'hotel-ratings-next-btn', 
            'text' => 'Next',
            'class' => 'secondary float-right preferences-next-btn',
            'redirect' => '/members/notifications/'
        )
    )
), $atts );

$user_id = get_current_user_id();
$user_hotel_ratings = maybe_unserialize(get_user_meta( $user_id, 'user_hotel_ratings', true));
if (!$user_hotel_ratings) $user_hotel_ratings = array();

$hotel_ratings = array( 5,4,3,2,1 );

$output = '<div class="content-box">';

    $output .= '<div class="list-header row">';
    $output .= '<h3 class="col col-sm-10">Select the ratings of hotels you want to stay in</h3>';
    $output .= '<span class="col col-sm-2 select-all-wrapper"><label class="control control-checkbox select-all">Select All <input type="checkbox" class="select-all-checkbox" /><div class="control_indicator"></div></label></span>';
    $output .= '</div>';

    $output .= '<ul class="list-wrapper">';
    foreach( $hotel_ratings as $hotel_rating ) {
        $output .= '<li class="list-item hotel-rating-item">';
            $output .= '<label class="control control-checkbox hotel-ratings-label" for="hotel-rating-'.$hotel_rating.'">';
            $output .= '<input class="checkbox" '.( in_array( $hotel_rating, $user_hotel_ratings ) || count($user_hotel_ratings) == 0 ? 'checked="checked"' : '') .' type="checkbox" class="calendar-checkbox" id="hotel-rating-'.$hotel_rating.'" name="hotel-rating-'.$hotel_rating.'" value="'.$hotel_rating.'" autocomplete="off"><div class="control_indicator"></div>' . $this->parent->hotel->hotel_ratings_star($hotel_rating) . ' <span class="hotel-rating-text">('.$hotel_rating.' star rating)</span>';
            $output .= '</label>';
        $output .= '</li>';
    }
    $output .= '</ul>';
$output .= '</div>';

$output .= $this->parent->component->btns( $array );

return $output;
