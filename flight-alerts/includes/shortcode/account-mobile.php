<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$account_section = 'mobile';

$output = '<div class="content-box">' . $this->get_mobile_form() . '</div>';

$output .= $this->parent->component->save_btn('account-'.$account_section.'-save-btn');

return $output;