<?php
/**
 * Crm class file.
 *
 * @package WordPress Plugin Template/Crm
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Crm class.
 */
class Flight_Alerts_Crm {

	/**
	 * The single instance of Flight_Alerts_Crm.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin crm.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available crm for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $crm = array();

	public $ac = null;
	public $user = null;

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;

		// add_action( 'init', array($this,'init_ac'));

		// hook when authenticate
		// add_action( 'um_after_email_confirmation', array($this,'user_email_verified'), 1, 1 );

		// after user registered
		add_action( 'user_register', array($this,'user_not_started_trial'), 1, 1 );
		
		// hook when order complete / start trial
		add_action( 'woocommerce_order_status_completed', array($this,'user_started_trial'), 10, 1 );

	}

	function init_ac() {
		$this->ac = new ActiveCampaign('https://craigtomkins.api-us1.com', 'a14109a7680e0d87b9e43589b9f2100c5b33a7a289ce19f655e7d647f70d98883f0de866');
	}

	function get_user() {
		$this->user = get_userdata( get_current_user_id() );
	}

	function add_contact($user) {
		
		$contact = array(
			"email" => $user->user_email,
			"first_name" => $user->first_name
		);
		
		$contact_sync = $this->ac->api("contact/sync", $contact);
		$contact_id = $contact_sync->subscriber_id;
		if ( !get_user_meta( $user->ID, 'activecampaign_contact_id', true )) {
			add_user_meta( $user->ID, 'activecampaign_contact_id', $contact_id);
		}
	}

	function user_email_verified( $user_id ) {
		
		$this->init_ac();

		$user = get_userdata( $user_id );

		$this->add_contact($user);

		$contact = array(
			"email" => $user->user_email,
			"tags"   => 'Email Verified'
		);
		
		// Could not resolve host: &api_action=contact_tag_add&api_output=json
		$res = $this->ac->api("contact/tag_add", $contact);

	}

	function user_not_started_trial( $user_id ) {
		
		$this->init_ac();

		$user = get_userdata( $user_id );

		$this->add_contact($user);

		$contact = array(
			"email" => $user->user_email,
			"tags"   => 'Trial Not Started'
		);
		
		// Could not resolve host: &api_action=contact_tag_add&api_output=json
		$res = $this->ac->api("contact/tag_add", $contact);

	}

	function user_started_trial( $order_id ) {

		$this->init_ac();

		$order = wc_get_order( $order_id );
		$user_id = $order->user_id;
		$user = get_userdata( $user_id );

		// $user_id = $this->user->ID;

		if ( !get_user_meta($user_id, 'started_trial', true) ) {
			// remove previous tag?
			// $contact = array(
			// 	"email" => $this->user->user_email,
			// 	"tags"   => 'Email Verified'
			// );
			// $this->ac->api("contact/tag_remove", $contact);
			$contact = array(
				"email" => $user->user_email,
				"tags"   => 'Trial Started'
			);
			$this->ac->api("contact/tag_add", $contact);

			update_user_meta($user_id, 'started_trial', true);

		}

	}

	function update_mobile() {
		
		$this->init_ac();

		$user_id = get_current_user_id();
		$user = get_userdata( $user_id );

		$mobile = get_user_meta($user_id, 'mobile', true);
		$code = get_user_meta($user_id, 'mobile_country_code', true);
		// $ac_id = get_user_meta( $user_id, 'activecampaign_contact_id', true);

		$contact = array(
			"email" => $user->user_email,
			"phone" => $code.$mobile,
			"first_name" =>$user->first_name
		);
		$this->ac->api("contact/sync", $contact);
	}

	/**
	 * Main Flight_Alerts_Crm Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Crm is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Crm instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
