<?php
/**
 * Hotel class file.
 *
 * @package WordPress Plugin Template/Hotel
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Hotel class.
 */
class Flight_Alerts_Hotel {

	/**
	 * The single instance of Flight_Alerts_Hotel.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin hotel.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available hotel for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $hotel = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) 
	{
		$this->parent = $parent;
		$this->base = 'wpt_';
		$this->assets_url = $this->parent->assets_url;

	}

	public function api_get_hotels( $city_code, $best_hotel_dates ) 
	{
		return include 'hotel' . DIR_SEP . 'api-get-hotels.php';
	}

	public function get_hotel_url( $city_code, $dates ) 
	{
		$check_in_date = $dates['check_in_date'];
		$check_out_date = $dates['check_out_date'];
		$check_in_dates = explode('-',$check_in_date);
		$check_out_dates = explode('-',$check_out_date);
	
		return "https://www.booking.com/searchresults.en-gb.html?ss=$city_code&is_ski_area=0&dest_type=city&checkin_year=$check_in_dates[0]&checkin_month=$check_in_dates[1]&checkin_monthday=$check_in_dates[2]&checkout_year=$check_out_dates[0]&checkout_month=$check_out_dates[1]&checkout_monthday=$check_out_dates[2]&group_adults=2&group_children=0&no_rooms=1&b_h4u_keep_filters=&from_sf=1";
	}

	public function get_hotel_filename( $city_code, $best_hotel_dates ) 
	{
		return 'hotels_' . $city_code . '_' . $best_hotel_dates['check_in_date'] . '_' . $best_hotel_dates['check_out_date'];
	}

	public function get_hotel_rating( $rating ) 
	{
		$output = '';
		for( $i = 0; $i < $rating; $i++ ) {
			$output .= '<span style="color:#febb02; margin-right:1px">&#9733;</span>';
		}
		return $output;
	}

	public function hotel_ratings_star($hotel_rating) {

		$output = '<span class="hotel-rating-star-wrapper">';
		for ( $i = 0; $i < 5; $i++ ) {
			$output .= '<img class="hotel-ratings-star" src="' . FA_PLUGIN_ASSETS_URL . '/img/icons/star' . ( $i < $hotel_rating ? '' : '-outline' ) . '.svg" />';
		}
		$output .= '</span>';
		return $output;
	}

	/**
	 * Main Flight_Alerts_Hotel Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Hotel is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Hotel instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
