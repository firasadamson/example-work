<?php
/**
 * Cron_Job class file.
 *
 * @package WordPress Plugin Template/Cron_Job
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Cron_Job class.
 */
class Flight_Alerts_Cron_Job {

	/**
	 * The single instance of Flight_Alerts_Cron_Job.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin cron_job.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available cron_job for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	
	public $cron_job = array();
	private $countries = '';
	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
		$this->base = $this->parent->_token;
		$this->assets_url = $this->parent->assets_url;

		$this->countries = array(
			'UK', 
			'US', 
			'CA', 
			'AU', 
			// 'IE', 
			// 'ES', 
			// 'DE', 
			// 'NZ'
		);

		// REQUIRED: remove old data in DB that's passed
	}

	function send_to_users() {
		// get basic and premium users
		$users = get_users( 
			array( 
				'role__in' => array( 'administrator', 'um_basic', 'um_premium' ) 
			) 
		);

		foreach( $users as $user ) {
			echo $user->ID . '<br>';
			$this->parent->user->send_to_user( $user->ID );
		}
		// $this->parent->user->send_to_user( 1 );
		
		// empty tables!
		global $wpdb;
		$prefix = $wpdb->prefix . $this->base;
		$table_names = array(
			$prefix . '_destinations',
			$prefix . '_destination_dates',
			$prefix . '_user_destination_dates'
		);
		foreach($table_names as $tbl) {
			$wpdb->query("TRUNCATE TABLE $tbl");
		}
	}

	function fetch_flights_from_api() {
		foreach( $this->countries as $country ) {
			$this->parent->flight->api_fetch_flight_data($country);
		} 
	}
	
	function insert_flights_into_db() {
		foreach( $this->countries as $country ) {
			$this->parent->flight->db_insert_flight_data($country);
		} 
	}

	/**
	 * Initialise cron_job
	 *
	 * @return void
	 */

	/**
	 * Main Flight_Alerts_Cron_Job Instance
	 *
	 * Ensures only one instance of Flight_Alerts_Cron_Job is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Flight_Alerts()
	 * @param object $parent Object instance.
	 * @return object Flight_Alerts_Cron_Job instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of Flight_Alerts_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
