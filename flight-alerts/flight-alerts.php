<?php
/**
 * Plugin Name: Flight Alerts
 * Version: 1.0.1
 * Plugin URI: http://www.firas.org/
 * Description: This is a plugin that alerts users with cheap flight deals.
 * Author: Firas
 * Author URI: http://www.firas.org/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: flight-alerts
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Firas
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once 'includes/_constants.php';

require_once 'vendor/activecampaign/includes/ActiveCampaign.class.php';


// Load plugin class files.
require_once 'includes/class-flight-alerts.php';
require_once 'includes/class-flight-alerts-ajax.php';
require_once 'includes/class-flight-alerts-api.php';
require_once 'includes/class-flight-alerts-database.php';
require_once 'includes/class-flight-alerts-settings.php';

require_once 'includes/class-flight-alerts-component.php';
require_once 'includes/class-flight-alerts-crm.php';
require_once 'includes/class-flight-alerts-cron-job.php';
require_once 'includes/class-flight-alerts-email.php';
require_once 'includes/class-flight-alerts-error.php';
require_once 'includes/class-flight-alerts-flight.php';
require_once 'includes/class-flight-alerts-helper.php';
require_once 'includes/class-flight-alerts-hotel.php';
require_once 'includes/class-flight-alerts-json.php';
require_once 'includes/class-flight-alerts-shortcode.php';
require_once 'includes/class-flight-alerts-sms.php';
require_once 'includes/class-flight-alerts-user.php';
require_once 'includes/class-flight-alerts-ultimate-member.php';
require_once 'includes/class-flight-alerts-woocommerce.php';

// Load plugin libraries.
require_once 'includes/lib/class-flight-alerts-admin-api.php';
require_once 'includes/lib/class-flight-alerts-post-type.php';
require_once 'includes/lib/class-flight-alerts-taxonomy.php';

// Init

/**
 * Returns the main instance of Flight_Alerts to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Flight_Alerts
 */
function flight_alerts() {
	$instance = Flight_Alerts::instance( __FILE__, '1.0.1' );

	if ( !isset( $instance->ajax ) ) {
		$instance->ajax = Flight_Alerts_Ajax::instance( $instance );
	}

	if ( !isset( $instance->api ) ) {
		$instance->api = Flight_Alerts_Api::instance( $instance );
	}

	if ( !isset( $instance->component ) ) {
		$instance->component = Flight_Alerts_Component::instance( $instance );
	}

	if ( !isset( $instance->crm ) ) {
		$instance->crm = Flight_Alerts_Crm::instance( $instance );
	}

	if ( !isset( $instance->cron_job ) ) {
		$instance->cron_job = Flight_Alerts_Cron_Job::instance( $instance );
	}

	if ( !isset( $instance->database ) ) {
		$instance->database = Flight_Alerts_Database::instance( $instance );
	}

	if ( !isset( $instance->email ) ) {
		$instance->email = Flight_Alerts_Email::instance( $instance );
	}

	if ( !isset( $instance->error ) ) {
		$instance->error = Flight_Alerts_Error::instance( $instance );
	}

	if ( !isset( $instance->flight ) ) {
		$instance->flight = Flight_Alerts_Flight::instance( $instance );
	}

	if ( !isset( $instance->helper ) ) {
		$instance->helper = Flight_Alerts_Helper::instance( $instance );
	}

	if ( !isset( $instance->hotel ) ) {
		$instance->hotel = Flight_Alerts_Hotel::instance( $instance );
	}

	if ( !isset( $instance->json ) ) {
		$instance->json = Flight_Alerts_Json::instance( $instance );
	}

	if ( !isset( $instance->settings ) ) {
		$instance->settings = Flight_Alerts_Settings::instance( $instance );
	}

	if ( !isset( $instance->shortcode ) ) {
		$instance->shortcode = Flight_Alerts_Shortcode::instance( $instance );
	}

	if ( !isset( $instance->sms ) ) {
		$instance->sms = Flight_Alerts_Sms::instance( $instance );
	}

	if ( !isset( $instance->user ) ) {
		$instance->user = Flight_Alerts_User::instance( $instance );
	}

	if ( !isset( $instance->ultimate_member ) ) {
		$instance->ultimate_member = Flight_Alerts_Ultimate_Member::instance( $instance );
	}

	if ( !isset( $instance->woocommerce ) ) {
		$instance->woocommerce = Flight_Alerts_Woocommerce::instance( $instance );
	}

	return $instance;
}

flight_alerts();
