// const app = require('express')();
// const http = require('http').createServer(app);

// app.get('/', (req, res) => {
//   res.send('<h1>Qinnect</h1>');
// });

// http.listen(3000, () => {
//   console.log('listening on *:3000');
// });

// 	socket.on('send-message', function(data) {
// 		console.log('send message',data);
// 		channel_name = data.key;
// 		socket.broadcast.to(channel_name).emit('message-received', data);
// 	});	

// Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
const corsOptions = {
	// origin: (origin, callback) => {
	// 	if (allowedOrigins.includes(origin) || !origin) {
	// 		callback(null, true);
	// 	} else {
	// 		callback(new Error('Origin not allowed by CORS'));
	// 	}
	// },
	origin: false,
	methods: ['GET', 'PUT', 'POST'],
	allowedHeaders: ['Content-Type', 'Authorization', 'Access-Control-Allow-Origin', 'Access-Control-Allow-Headers']

}


var app = require('express')();
var cors = require('cors');
var server = require('http').createServer(app); // onRequest
var io = require("socket.io")(server, {
  cors: {
    origin: '*',
    methods: ['GET', 'PUT', 'POST'],
    allowedHeaders: ['Content-Type', 'Authorization', 'Access-Control-Allow-Origin', 'Access-Control-Allow-Headers']

  }
});

// var express = require('express'),;
// app = express();
// http = require('http');
// server = http.createServer(app);
// io = require('socket.io').listen(server);
// var cors = require('cors');

var port = 8080;
// var port = 3000;

var channel_names = [];
var channel_name = '';
var timeout = undefined;

const allowedOrigins = [
	'capacitor://localhost',
	'ionic://localhost',
	'http://localhost',
	'http://localhost:8080',
	'http://localhost:8100',
	'https://api.qinnectapp.com/'
];

// Enable preflight requests for all routes
//app.options('*', cors(corsOptions));

// function onRequest(req,res){
// 	res.writeHead(200, {
// 	'Access-Control-Allow-Origin' : '*',
// 	//"Access-Control-Allow-Headers" : "Origin, X-Requested-With, Content-Type, Accept"
// 	});
// };

// app.get('/', function(req, res){
// 	res.send('<h1>Hello world</h1>');
// });

app.use(cors());

app.get('/', cors(corsOptions), (req, res, next) => {
	res.json({ message: 'This route is CORS-enabled for an allowed origin.' });
})

// Runs only once!
// io.use(function(socket, next) {
	
	// var handshakeData = socket.request;
	// var keys = handshakeData._query['keys'];
	// console.log("handshakeData._query['keys']",handshakeData._query['keys']);

	// console.log('KEYS!!!!',keys);

	// if (!keys) return;

	// if (keys.includes(',')) {
	// 	keys = keys.split(',');
	// 	for(let i = 0; i < keys.length; i++ )
	// 	channel_names.push( keys[i] );
	// } else {
	// 	channel_names.push( keys );
	// }
	// next();
// });

io.on('connection', function(socket) {

	console.log('user connected...')

	socket.on("join-channels", function(data) { 
		console.log('join channels...');  
		// if (!channel_names || channel_names.length == 0) return;
		for ( i = 0; i < data.keys.length; i++ ) {
			console.log('user connected to', data.keys[i]);
			socket.join(data.keys[i]);
		}
	});

	socket.on("join-channel", function(data) {   
		console.log('subscribing to',data);
		channel_name = data.channel_key;
		socket.join(channel_name);   
		io.sockets.in(channel_name).emit('channel-joined', data);
	});

	socket.on("join-server", function(data) {   
		console.log('subscribing to server',data);
		channel_name = data.server_key;
		socket.join(channel_name);   
		io.sockets.in(channel_name).emit('server-joined', data);
	});

	socket.on('send-message', function(data) {
		console.log('send message',data);
		channel_name = data.channel_key;
		io.sockets.in(channel_name).emit('message-received', data);
	});	

	// socket.on('typing', function(data) {
	// 	channel_name = data.key;
	// 	console.log('typing..', channel_name)
	// 	if ( data.typing == true ) {
	// 		socket.broadcast.to(channel_name).emit('display', data);
	// 		clearTimeout(timeout);
	// 		timeout = setTimeout( function() {
	// 			data.typing = false;
	// 			socket.broadcast.to(channel_name).emit('display', data);
	// 		}, 1500);
	// 	} else {
	// 		socket.broadcast.to(channel_name).emit('display', data);
	// 	}
	// })

	socket.on('refresh-channels-list', function(data) {
		console.log('refresh channels list',data);
		channel_name = data.server_key;
		io.sockets.in(channel_name).emit('refresh-channels-list-done', data);
	});	


});

server.listen(port, function(){
	console.log('Connected to Qinnect socket server...');
	console.log(`listening on *:${port}`);
});
