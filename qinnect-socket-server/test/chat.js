var app = require('express')();
var cors = require('cors');
var server = require('http').createServer(app); // onRequest
var io = require("socket.io")(server);

var port = 3000;

var channel_name = '';
var timeout = undefined;

app.use(cors());

io.use(function(socket, next) {
	var handshakeData = socket.request;
	channel_name = handshakeData._query['key'];
	console.log('channel_name 1', channel_name);
	next();
});

io.on('connection', function(socket) {
	
	console.log('User connected.');
	socket.join(channel_name);
	
	// socket.emit('user-connected', {message: 'user has connected to the server!'});
	socket.on('send-message', function(data) {
		// send to everyone in the channel including the sender. This is required.
		io.to(channel_name).emit('message-received', data);
	});	

	socket.on('join-channel', function(data) {
		channel_name = data['key'];
		console.log('channel_name 2', channel_name);
		socket.join(channel_name);
	})

	socket.on('typing', function(data) {
		if ( data.typing == true ) {
			socket.to(channel_name).emit('display', data);
			clearTimeout(timeout);
			timeout = setTimeout( function() {
				data.typing = false;
				socket.to(channel_name).emit('display', data);
			}, 1500);
		} else {
			socket.to(channel_name).emit('display', data);
		}
	})
});

server.listen(port, function(){
	console.log('Connected to server!');
	console.log(`listening on *:${port}`);
});
